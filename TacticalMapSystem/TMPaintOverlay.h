//
//  TMPaintOverlay.h
//  TacticalMapSystem
//
//  Created by Chen Zhejun on 2014/01/07.
//  Copyright (c) 2014年 chen zhejun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import <pthread.h>

@interface TMPaintOverlay : NSObject<MKOverlay>
@property (nonatomic, readonly)NSMutableArray *paths;
- (id)initWithCenterCoordinate:(CLLocationCoordinate2D)coord;
- (void)addPathWithLocation:(CLLocation *)location;
- (MKMapRect)addLineWithLocation:(CLLocation *)location;
- (void)lockForReading;
- (void)unlockForReading;
@end
