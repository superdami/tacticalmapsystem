//
//  TMUserManager.h
//  TacticalMapSystem
//
//  Created by Chen Zhejun on 7/17/14.
//  Copyright (c) 2014 chen zhejun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface TMUserManager : NSObject
@property(nonatomic, readonly)NSString *userId;
@property(nonatomic, readonly)NSString *userName;
@property(nonatomic, readonly)NSString *callsign;
@property(nonatomic, readonly)NSString *token;
@property(nonatomic, readonly)NSString *avatar;
@property(nonatomic, assign)CLLocationCoordinate2D lastCoordinate;
@property(nonatomic, readonly)BOOL isLogin;
@property(nonatomic, readonly)NSString *uuid;

+ (TMUserManager *)sharedInstance;
- (void)authWithTokenCompletion:(void(^)())completeBlock;
- (void)setLoginData:(NSDictionary *)data;
- (void)setUserData:(NSDictionary *)data;
- (void)exitMapViewController;
- (void)loginWithFB;
@end
