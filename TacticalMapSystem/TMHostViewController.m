//
//  TMHostViewController.m
//  TacticalMapSystem
//
//  Created by chen zhejun on 2015/03/15.
//  Copyright (c) 2015年 chen zhejun. All rights reserved.
//

#import "TMHostViewController.h"
#import "TMGameManager.h"
#import "MapViewController.h"

@implementation TMHostViewController {
    UITextField *_titleField;
    UITextField *_nameField;
    UITextField *_passwordField;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGSize btnSize = CGSizeMake(250.0, 40.0);
    CGFloat x = (self.view.frame.size.width - 250.0) / 2.0;
    
    if (![_joinRoomId length]) {
        _titleField = [[UITextField alloc] initWithFrame:CGRectMake(x, 100.0, btnSize.width, btnSize.height)];
        _titleField.placeholder = @"room description";
        [self.view addSubview:_titleField];
        
        _nameField = [[UITextField alloc] initWithFrame:CGRectMake(x, 150.0, btnSize.width, btnSize.height)];
        _nameField.placeholder = @"room name";
        [self.view addSubview:_nameField];
    }
    
    _passwordField = [[UITextField alloc] initWithFrame:CGRectMake(x, 200.0, btnSize.width, btnSize.height)];
    _passwordField.placeholder = @"key";
    [self.view addSubview:_passwordField];

    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(x, 250, btnSize.width, btnSize.height);
    [button setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
    [button setTitle:@"Go" forState:UIControlStateNormal];
    
    ALActionBlock block = nil;
    if ([_joinRoomId length]) {
        block = ^(id weakSender) {
            if (!_passwordField.text) {
                return;
            }
            [[TMAPIClient sharedInstance] joinRoomId:_joinRoomId password:_passwordField.text completion:^(NSDictionary *result) {
                MapViewController *mapVC = [[MapViewController alloc] init];
                [self.navigationController pushViewController:mapVC animated:YES];
            } failure:nil];
        };
    } else {
        block = ^(id weakSender) {
            if (!_titleField.text || !_nameField.text || !_passwordField.text) {
                return;
            }
            CLLocationCoordinate2D coordinate = [TMUserManager sharedInstance].lastCoordinate;
            [[TMAPIClient sharedInstance] createRoomWith:coordinate name:_nameField.text title:_titleField.text password:_passwordField.text completion:^(NSDictionary *result) {
                MapViewController *mapVC = [[MapViewController alloc] init];
                [self.navigationController pushViewController:mapVC animated:YES];
            } failure:nil];
        };
    }
    
    [button handleControlEvents:UIControlEventTouchUpInside withBlock:block];
    [self.view addSubview:button];
}
@end
