//
//  TMNavigationController.m
//  TacticalMapSystem
//
//  Created by chen zhejun on 2015/03/28.
//  Copyright (c) 2015年 chen zhejun. All rights reserved.
//

#import "TMNavigationController.h"
#import "MapViewController.h"

@implementation TMNavigationController

- (BOOL)shouldAutorotate {
    return _enableAutorotate;
}

- (NSUInteger)supportedInterfaceOrientations {
    if ([self.topViewController isKindOfClass:[MapViewController class]]) {
        return UIInterfaceOrientationMaskAll;        
    }
    return UIInterfaceOrientationMaskPortrait;
}
@end
