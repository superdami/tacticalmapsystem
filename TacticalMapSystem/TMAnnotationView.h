//
//  TMAnnotationView.h
//  TacticalMapSystem
//
//  Created by chen zhejun on 9/28/12.
//  Copyright (c) 2012 chen zhejun. All rights reserved.
//

#import <MapKit/MapKit.h>

@protocol TMAnnotationViewDelegate <NSObject>
@required
- (void)moveAnnotationBegin:(id<MKAnnotation>)annotation;
- (void)movingAnnotation:(id<MKAnnotation>)annotation;
- (void)movedAnnotation:(id<MKAnnotation>)annotation;
@end

@interface TMAnnotationView : MKAnnotationView
@property (nonatomic, weak) id<TMAnnotationViewDelegate> delegate;
@property (nonatomic, readonly)UIImageView *imageView;
@property (nonatomic, weak) MKMapView *mapView;
@property (nonatomic, assign) BOOL showTag;

- (void)setRotationAngle:(CGFloat)angle;
- (void)setTagText:(NSString *)text;
- (void)showSelectedBound;
- (void)hideSelectedBound;
- (void)refresh;
@end