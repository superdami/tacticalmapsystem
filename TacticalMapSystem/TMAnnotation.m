//
//  TMAnnotation.m
//  TacticalMapSystem
//
//  Created by chen zhejun on 9/21/12.
//  Copyright (c) 2012 chen zhejun. All rights reserved.
//

#import "TMAnnotation.h"

@implementation TMAnnotation

- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate {
    self = [super init];
    if (self) {
        self.coordinate = coordinate;
    }
    return self;
}

- (NSDictionary *)dataObject {
    return @{@"desc" : self.desc ? self.desc : @"",
             @"type" : @(self.type),
             @"coordinate" : @{@"lat" : @(self.coordinate.latitude), @"lon" : @(self.coordinate.longitude)},
             @"uid" : self.uid,
             @"heading" : @(self.heading),
             @"status": @(_status),
             @"image" : self.image ? self.image : @""};
}
@end
