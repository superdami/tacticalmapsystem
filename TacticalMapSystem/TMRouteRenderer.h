//
//  TMRouteRenderer.h
//  TacticalMapSystem
//
//  Created by Chen Zhejun on 2013/12/28.
//  Copyright (c) 2013年 chen zhejun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@class TMRouteOverlay;
@interface TMRouteRenderer : MKOverlayRenderer
@property (nonatomic, assign)BOOL lineDash;
@property (nonatomic, assign)CGFloat lineWidth;
- (void)setPathColorWithRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue alpha:(CGFloat)alpha;
- (void)updateDrawRect:(MKMapRect)updateRect zoomScale:(MKZoomScale)zoomScale;
@end

@interface TMRouteRenderer(TMRouteRenderer_protected)
- (CGPathRef)newPathForAnnotations:(NSArray *)annotations
                          clipRect:(MKMapRect)mapRect
                         zoomScale:(MKZoomScale)zoomScale;
@end
