//
//  TMHelper.m
//  TacticalMapSystem
//
//  Created by Chen Zhejun on 2014/01/08.
//  Copyright (c) 2014年 chen zhejun. All rights reserved.
//

#import "TMHelper.h"
#import <CommonCrypto/CommonDigest.h>

CGFloat DegreesToRadians(CGFloat degrees) {return degrees * M_PI / 180;}
@implementation TMHelper
#define MINIMUM_DELTA_METERS 2.0

+ (MKMapRect)makeRectWithPoint:(MKMapPoint)point1 and:(MKMapPoint)point2 {
    CLLocationDistance metersApart = MKMetersBetweenMapPoints(point1, point2);
    MKMapRect mapRect = MKMapRectNull;
    if (metersApart > MINIMUM_DELTA_METERS)
    {
        double minX = MIN(point1.x, point2.x);
        double minY = MIN(point1.y, point2.y);
        double maxX = MAX(point1.x, point2.x);
        double maxY = MAX(point1.y, point2.y);
        
        mapRect = MKMapRectMake(minX, minY, maxX - minX, maxY - minY);
    }
    return mapRect;
}

+ (CLLocationDirection)directionFromCoordinate:(CLLocationCoordinate2D)coord1 to:(CLLocationCoordinate2D)coord2 {
    
    CLLocationDegrees deltaLong = coord2.longitude - coord1.longitude;
    CLLocationDegrees yComponent = sin(deltaLong) * cos(coord2.latitude);
    CLLocationDegrees xComponent = (cos(coord1.latitude) * sin(coord2.latitude)) - (sin(coord1.latitude) * cos(coord2.latitude) * cos(deltaLong));
    
    CLLocationDegrees radians = atan2(yComponent, xComponent);
    CLLocationDegrees degrees = RAD_TO_DEG(radians) + 360;
    
    return fmod(degrees, 360);
}

+ (BOOL)linePoint:(MKMapPoint)p0 and:(MKMapPoint)p1 intersectsRect:(MKMapRect)r
{
    double minX = MIN(p0.x, p1.x);
    double minY = MIN(p0.y, p1.y);
    double maxX = MAX(p0.x, p1.x);
    double maxY = MAX(p0.y, p1.y);
    
    MKMapRect r2 = MKMapRectMake(minX, minY, maxX - minX, maxY - minY);
    return MKMapRectIntersectsRect(r, r2);
}

+ (CGFloat)distanceBetween:(CLLocationCoordinate2D)coordinate1 and:(CLLocationCoordinate2D)coordinate2 {
    MKMapPoint mapP1 = MKMapPointForCoordinate(coordinate1);
    MKMapPoint mapP2 = MKMapPointForCoordinate(coordinate2);
    return MKMetersBetweenMapPoints(mapP1, mapP2);
}

+ (MKMapRect)MKMapRectForCoordinateRegion:(MKCoordinateRegion)region
{
    MKMapPoint a = MKMapPointForCoordinate(CLLocationCoordinate2DMake(
                                                                      region.center.latitude + region.span.latitudeDelta / 2,
                                                                      region.center.longitude - region.span.longitudeDelta / 2));
    MKMapPoint b = MKMapPointForCoordinate(CLLocationCoordinate2DMake(
                                                                      region.center.latitude - region.span.latitudeDelta / 2,
                                                                      region.center.longitude + region.span.longitudeDelta / 2));
    return MKMapRectMake(MIN(a.x,b.x), MIN(a.y,b.y), ABS(a.x-b.x), ABS(a.y-b.y));
}

+ (MKCoordinateRegion) regionForAnnotations:(NSArray *)annotationArray
{
    double minLat=90.0f, maxLat=-90.0f;
    double minLon=180.0f, maxLon=-180.0f;
    
    for (id<MKAnnotation> mka in annotationArray) {
        if ( mka.coordinate.latitude  < minLat ) minLat = mka.coordinate.latitude;
        if ( mka.coordinate.latitude  > maxLat ) maxLat = mka.coordinate.latitude;
        if ( mka.coordinate.longitude < minLon ) minLon = mka.coordinate.longitude;
        if ( mka.coordinate.longitude > maxLon ) maxLon = mka.coordinate.longitude;
    }
    
    CLLocationCoordinate2D center = CLLocationCoordinate2DMake((minLat+maxLat)/2.0, (minLon+maxLon)/2.0);
    MKCoordinateSpan span = MKCoordinateSpanMake(maxLat-minLat, maxLon-minLon);
    MKCoordinateRegion region = MKCoordinateRegionMake (center, span);
    
    return region;
}

@end
