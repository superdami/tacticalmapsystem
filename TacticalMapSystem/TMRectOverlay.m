//
//  TMRectOverlay.m
//  TacticalMapSystem
//
//  Created by Chen Zhejun on 2014/09/02.
//  Copyright (c) 2014年 chen zhejun. All rights reserved.
//

#import "TMRectOverlay.h"
#import "TMHelper.h"
#import "TMAnnotation.h"

@implementation TMRectOverlay {
    CLLocationCoordinate2D _centerCoord;
}

- (id)initWithRegion:(MKCoordinateRegion) region mapView:(MKMapView *)mapView {
    self = [super init];
    if (self) {
        _waypointAnnotations = [NSMutableArray new];
        MKMapRect rect = [TMHelper MKMapRectForCoordinateRegion:region];
        
        TMAnnotation *leftDown = [[TMAnnotation alloc] initWithCoordinate:MKCoordinateForMapPoint(MKMapPointMake(MKMapRectGetMinX(rect), MKMapRectGetMaxY(rect)))];
        TMAnnotation *leftUp = [[TMAnnotation alloc] initWithCoordinate:MKCoordinateForMapPoint(MKMapPointMake(MKMapRectGetMinX(rect), MKMapRectGetMinY(rect)))];
        TMAnnotation *rightUp = [[TMAnnotation alloc] initWithCoordinate:MKCoordinateForMapPoint(MKMapPointMake(MKMapRectGetMaxX(rect), MKMapRectGetMinY(rect)))];
        TMAnnotation *rightDown = [[TMAnnotation alloc] initWithCoordinate:MKCoordinateForMapPoint(MKMapPointMake(MKMapRectGetMaxX(rect), MKMapRectGetMaxY(rect)))];
        _centerAnnotation = [[TMAnnotation alloc] initWithCoordinate:region.center];
        leftUp.type = ControlType;
        leftDown.type = ControlType;
        rightUp.type = ControlType;
        rightDown.type = ControlType;
        _centerAnnotation.type = ControlType;
        [self.mapView addAnnotation:_centerAnnotation];
        
        [self addWaypointAnnptation:leftDown];
        [self addWaypointAnnptation:leftUp];
        [self addWaypointAnnptation:rightUp];
        [self addWaypointAnnptation:rightDown];
        [self setRegion:region];

        self.mapView = mapView;
    }
    return self;
}

- (MKMapRect)boundingMapRect {
    MKCoordinateRegion region = [TMHelper regionForAnnotations:_waypointAnnotations];
    return [TMHelper MKMapRectForCoordinateRegion:region];
}

- (void)setMapView:(MKMapView *)mapView {
    if (self.mapView) {
        [self.mapView removeAnnotation:_centerAnnotation];
    }
    
    [super setMapView:mapView];
    [self.mapView addAnnotation:_centerAnnotation];
}

- (BOOL)closePath {
    return YES;
}

- (void)moveAnnotationBegin:(id<MKAnnotation>)annotation {
    if (annotation == _centerAnnotation) {
        _centerCoord = _centerAnnotation.coordinate;
    }
}

- (void)movingAnnotation:(NSObject<MKAnnotation> *)annotation {
    if ([self.waypointAnnotations containsObject:annotation]) {
        NSInteger index = [self.waypointAnnotations indexOfObject:annotation];
        NSDictionary *points = [self associatedPointsWithIndex:index];
        NSObject<MKAnnotation> *prePoint = [points objectForKey:@"prepoint"];
        NSObject<MKAnnotation> *nextPoint = [points objectForKey:@"nextpoint"];
        if (index % 2) {
            prePoint.coordinate = CLLocationCoordinate2DMake(prePoint.coordinate.latitude, annotation.coordinate.longitude);
            nextPoint.coordinate = CLLocationCoordinate2DMake(annotation.coordinate.latitude, nextPoint.coordinate.longitude);
        }
        else {
            prePoint.coordinate = CLLocationCoordinate2DMake(annotation.coordinate.latitude, prePoint.coordinate.longitude);
            nextPoint.coordinate = CLLocationCoordinate2DMake(nextPoint.coordinate.latitude, annotation.coordinate.longitude);
        }
        _centerAnnotation.coordinate = CLLocationCoordinate2DMake((prePoint.coordinate.latitude + nextPoint.coordinate.latitude) / 2.0, (prePoint.coordinate.longitude + nextPoint.coordinate.longitude) / 2.0);
    }
    else if(annotation == _centerAnnotation){
        CLLocationDegrees lat = _centerAnnotation.coordinate.latitude - _centerCoord.latitude;
        CLLocationDegrees lon = _centerAnnotation.coordinate.longitude - _centerCoord.longitude;
        
        for (NSObject<MKAnnotation> *annotation in self.waypointAnnotations) {
            annotation.coordinate = CLLocationCoordinate2DMake(annotation.coordinate.latitude + lat, annotation.coordinate.longitude + lon);
        }
    }
    _centerCoord = _centerAnnotation.coordinate;
}

- (void)movedAnnotation:(id<MKAnnotation>)annotation {
    [self movingAnnotation:annotation];
}

//- (void)resetConersCoordinate {
//    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(_centerAnnotation.coordinate, 1000, 1000);
//    NSObject<MKAnnotation> *leftDown = [self.waypointAnnotations objectAtIndex:0];
//    NSObject<MKAnnotation> *leftUp = [self.waypointAnnotations objectAtIndex:1];
//    NSObject<MKAnnotation> *rightUp = [self.waypointAnnotations objectAtIndex:2];
//    NSObject<MKAnnotation> *rightDown = [self.waypointAnnotations objectAtIndex:3];
//    
//    MKMapPoint point1 = MKMapPointForCoordinate(CLLocationCoordinate2DMake(region.center.latitude + region.span.latitudeDelta / 2.0, region.center.longitude - region.span.longitudeDelta / 2.0));
//    MKMapPoint point2 = MKMapPointForCoordinate(CLLocationCoordinate2DMake(region.center.latitude - region.span.latitudeDelta / 2.0, region.center.longitude + region.span.longitudeDelta / 2.0));
//    
//    leftUp.coordinate = MKCoordinateForMapPoint(MKMapPointMake(MIN(point1.x, point2.x), MIN(point1.y, point2.y)));
//    leftDown.coordinate = MKCoordinateForMapPoint(MKMapPointMake(MIN(point1.x, point2.x), MAX(point1.y, point2.y)));
//    rightUp.coordinate = MKCoordinateForMapPoint(MKMapPointMake(MAX(point1.x, point2.x), MIN(point1.y, point2.y)));
//    rightDown.coordinate = MKCoordinateForMapPoint(MKMapPointMake(MAX(point1.x, point2.x), MAX(point1.y, point2.y)));
//}

- (void)setRegion:(MKCoordinateRegion)region {
    MKMapRect rect = [TMHelper MKMapRectForCoordinateRegion:region];

    NSObject<MKAnnotation> *leftDown = [self.waypointAnnotations objectAtIndex:0];
    NSObject<MKAnnotation> *leftUp = [self.waypointAnnotations objectAtIndex:1];
    NSObject<MKAnnotation> *rightUp = [self.waypointAnnotations objectAtIndex:2];
    NSObject<MKAnnotation> *rightDown = [self.waypointAnnotations objectAtIndex:3];
    
    leftDown.coordinate = MKCoordinateForMapPoint(MKMapPointMake(MKMapRectGetMinX(rect), MKMapRectGetMaxY(rect)));
    leftUp.coordinate = MKCoordinateForMapPoint(MKMapPointMake(MKMapRectGetMinX(rect), MKMapRectGetMinY(rect)));
    rightUp.coordinate = MKCoordinateForMapPoint(MKMapPointMake(MKMapRectGetMaxX(rect), MKMapRectGetMinY(rect)));
    rightDown.coordinate = MKCoordinateForMapPoint(MKMapPointMake(MKMapRectGetMaxX(rect), MKMapRectGetMaxY(rect)));
    _centerAnnotation.coordinate = region.center;
}
@end
