//
//  TMRoom.h
//  TacticalMapSystem
//
//  Created by chen zhejun on 2015/03/14.
//  Copyright (c) 2015年 chen zhejun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMRoom : NSObject
@property (nonatomic, readonly)NSString *title;
@property (nonatomic, readonly)NSString *name;
@property (nonatomic, readonly)NSString *berfing;
@property (nonatomic, readonly)CLLocationCoordinate2D coordinate;
@property (nonatomic, readonly)NSTimeInterval createdAt;
@property (nonatomic, readonly)NSTimeInterval startedAt;
@property (nonatomic, readonly)NSDictionary *host;
@property (nonatomic, readonly)NSString *uid;
@property (nonatomic, strong)NSDictionary *originalData;
@end
