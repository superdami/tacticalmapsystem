//
//  TMSideMenuView.m
//  TacticalMapSystem
//
//  Created by chen zhejun on 2015/05/09.
//  Copyright (c) 2015年 chen zhejun. All rights reserved.
//

#define kGrayBlueColor [UIColor colorWithRed:0.0 / 255.0 green:22.0 / 255.0 blue:104.0 / 255.0 alpha:1.0]
#import "TMSideMenuView.h"
@interface TMSideMenuView() <UITableViewDelegate, UITableViewDataSource>
@end

@implementation TMSideMenuView {
    UITableView *_tableView;
    NSMutableArray *_cellArray;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 100.0, self.bounds.size.width, self.bounds.size.height - 100.0)];
        _tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [self addSubview:_tableView];
        
        [self initCells];
        [_tableView reloadData];
    }
    return self;
}

- (void)initCells {
    _cellArray = [NSMutableArray new];
    
    CGRect cellRect = CGRectMake(0.0, 0.0, self.bounds.size.width, 60.0);
    CGRect switcherRect = CGRectMake(cellRect.size.width - 100.0, (cellRect.size.height - 40.0) / 2.0, 80.0, 40.0);
    UITableViewCell *cell = [[UITableViewCell alloc] initWithFrame:cellRect];
    cell.textLabel.text = @"Show Tags";
    UIButton *switcher = [[UIButton alloc] initWithFrame:switcherRect];
    [switcher setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [switcher setTitle:@"OFF" forState:UIControlStateNormal];
    [switcher setTitle:@"ON" forState:UIControlStateSelected];
    switcher.tag = 1;
    [cell addSubview:switcher];
    [switcher addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [_cellArray addObject:cell];
    switcher.selected = [[[NSUserDefaults standardUserDefaults] objectForKey:kShowAnnotationTag] boolValue];
    
    cell = [[UITableViewCell alloc] initWithFrame:cellRect];
    cell.textLabel.text = @"Unit";
    switcher = [[UIButton alloc] initWithFrame:switcherRect];
    [switcher setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [switcher setTitle:@"Imperial" forState:UIControlStateNormal];
    [switcher setTitle:@"Metric" forState:UIControlStateSelected];
    [switcher addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    switcher.tag = 2;
    [cell addSubview:switcher];
    [_cellArray addObject:cell];
    switcher.selected = [[[NSUserDefaults standardUserDefaults] objectForKey:kDistanceUnitMetric] boolValue];
    
    cell = [[UITableViewCell alloc] initWithFrame:cellRect];
    cell.textLabel.text = @"Direction";
    switcher = [[UIButton alloc] initWithFrame:switcherRect];
    [switcher setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [switcher setTitle:@"Degree" forState:UIControlStateNormal];
    [switcher setTitle:@"Clock" forState:UIControlStateSelected];
    [switcher addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    switcher.tag = 3;
   [cell addSubview:switcher];
    [_cellArray addObject:cell];
    switcher.selected = [[[NSUserDefaults standardUserDefaults] objectForKey:kDirectionFormatDegree] boolValue];

    cell = [[UITableViewCell alloc] initWithFrame:cellRect];
    cell.textLabel.text = @"Disconnect";
    cell.backgroundColor = [UIColor redColor];
    [_cellArray addObject:cell];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_cellArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return _cellArray[indexPath.row];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 3) {
        if (_delegate && [_delegate respondsToSelector:@selector(disconnect)]) {
            [_delegate disconnect];
        }
    }
}

- (void)buttonPressed:(UIButton *)sender {
    sender.selected = !sender.selected;
    NSString *key;
    if (sender.tag == 1) {
        key = kShowAnnotationTag;
        if (_delegate && [_delegate respondsToSelector:@selector(switchShowAnnotationTag:)]) {
            [_delegate switchShowAnnotationTag:sender.selected];
        }
    } else if (sender.tag == 2) {
        if (_delegate && [_delegate respondsToSelector:@selector(switchUsingUnitMetric:)]) {
            [_delegate switchUsingUnitMetric:sender.selected];
        }
        key = kDistanceUnitMetric;
    } else {
        if (_delegate && [_delegate respondsToSelector:@selector(switchUsingDegree:)]) {
            [_delegate switchUsingDegree:sender.selected];
        }
        key = kDirectionFormatDegree;
    }
    [[NSUserDefaults standardUserDefaults] setObject:@(sender.selected) forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
@end
