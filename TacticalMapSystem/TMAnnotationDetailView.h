//
//  TMAnnotationDetailView.h
//  TacticalMapSystem
//
//  Created by chen zhejun on 2015/05/12.
//  Copyright (c) 2015年 chen zhejun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMAnnotationDetailView : UIView
@property (nonatomic, strong)TMAnnotation *annotation;
- (void)refreshLabels;
@end
