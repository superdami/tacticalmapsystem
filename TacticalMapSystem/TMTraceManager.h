//
//  TMTraceManager.h
//  TacticalMapSystem
//
//  Created by Chen Zhejun on 2014/01/09.
//  Copyright (c) 2014年 chen zhejun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@class TMPaintOverlay;
@class TMPaintRenderer;
@protocol TMTraceManagerDelegate <NSObject>
- (MKZoomScale)mapCurrentZoomScale;
- (void)addOverlayToMapView:(NSObject<MKOverlay> *)overlay;
- (void)addAnnotationToMapView:(NSObject<MKAnnotation> *)annotation;
- (MKAnnotationView *)viewForAnnotation:(id<MKAnnotation>)annotation;
@end

@interface TMTraceManager : NSObject
@property (nonatomic, readonly) BOOL isReplayTrace;
@property (nonatomic, readonly) TMPaintOverlay *trackingOverlay;
@property (nonatomic, readonly) TMPaintRenderer *trackingRenderer;
@property (nonatomic, weak) id<TMTraceManagerDelegate> delegate;
@property (nonatomic, assign) CGFloat replaySpeedRate;
- (void)setTracingAnnotation:(NSObject <MKAnnotation> *)annotation;
- (BOOL)loadTraceData;
- (void)replayRoutePath;
@end
