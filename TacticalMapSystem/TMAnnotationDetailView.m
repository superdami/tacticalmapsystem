//
//  TMAnnotationDetailView.m
//  TacticalMapSystem
//
//  Created by chen zhejun on 2015/05/12.
//  Copyright (c) 2015年 chen zhejun. All rights reserved.
//

#import "TMSideMenuView.h"
#import "TMHelper.h"
#import "TMAnnotationManager.h"
#import "TMAnnotationDetailView.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface TMAnnotationDetailView() <UITextFieldDelegate>
@end

@implementation TMAnnotationDetailView {
    UIImageView *_imageView;
    UILabel *_distanceLabel;
    UILabel *_directionLabel;
    UITextField *_tagTextfield;
    UILabel *_timeLabel;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        UIToolbar *bg = [[UIToolbar alloc] initWithFrame:self.bounds];
        bg.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self addSubview:bg];
        
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, -20.0, 60.0, 60.0)];
        _imageView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
        [self addSubview:_imageView];

        _tagTextfield = [[UITextField alloc] initWithFrame:CGRectMake(100.0, 0.0,
                                                                      self.frame.size.width - 100.0, self.frame.size.height / 2.0)];
        _tagTextfield.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _tagTextfield.textColor = [UIColor darkGrayColor];
        _tagTextfield.delegate = self;
        [self addSubview:_tagTextfield];

        CGSize labelSize = CGSizeMake(self.frame.size.width / 3.0, self.frame.size.height - CGRectGetMaxY(_imageView.frame));
        _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0, CGRectGetMaxY(_imageView.frame),
                                                               labelSize.width, labelSize.height)];
        _timeLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _timeLabel.textColor = [UIColor darkGrayColor];
        _timeLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_timeLabel];

        _directionLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_timeLabel.frame), CGRectGetMaxY(_imageView.frame),
                                                                    labelSize.width, labelSize.height)];
        _directionLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
        _directionLabel.textColor = [UIColor darkTextColor];
        _directionLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_directionLabel];

        _distanceLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_directionLabel.frame), CGRectGetMaxY(_imageView.frame),
                                                                   labelSize.width, labelSize.height)];
        _distanceLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
        _distanceLabel.textColor = [UIColor darkTextColor];
        _distanceLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_distanceLabel];
        
        [[TMAnnotationManager sharedManager].userAnnotation addObserver:self forKeyPath:@"coordinate" options:NSKeyValueObservingOptionPrior | NSKeyValueObservingOptionNew context:NULL];
    }
    return self;
}

- (void)setAnnotation:(TMAnnotation *)annotation {
    if (_annotation && _annotation != [TMAnnotationManager sharedManager].userAnnotation) {
        [_annotation removeObserver:self forKeyPath:@"coordinate"];
        _annotation = nil;
    }
    _imageView.image = nil;
    
    if (!annotation) {
        return;
    }
    
    _annotation = annotation;
    if ([_annotation.image length]) {
        NSString *path = [[NSURL URLWithString:_annotation.image relativeToURL:[NSURL URLWithString:kBaseURL]] absoluteString];
        NSURL *url = [NSURL URLWithString:path];
        [_imageView sd_setImageWithURL:url];
    } else {
        _imageView.image = [TMAnnotationManager imageWithAnnotationType:_annotation.type];
    }
    
    if (_annotation != [TMAnnotationManager sharedManager].userAnnotation) {
        [_annotation addObserver:self forKeyPath:@"coordinate" options:NSKeyValueObservingOptionPrior | NSKeyValueObservingOptionNew context:NULL];
    }
    [self refreshLabels];
}

- (void)dealloc {
    if (_annotation && _annotation != [TMAnnotationManager sharedManager].userAnnotation) {
        @try{
            [_annotation removeObserver:self forKeyPath:@"coordinate"];
        }@catch(id anException){

        }
    }
    [[TMAnnotationManager sharedManager].userAnnotation removeObserver:self forKeyPath:@"coordinate"];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([change objectForKey:@"new"]) {
        [self refreshLabels];
    }
}

- (void)refreshLabels {
    CLLocationCoordinate2D userCoord = [TMAnnotationManager sharedManager].userAnnotation.coordinate;
    CLLocationCoordinate2D targetCoord = _annotation.coordinate;
    
    CGFloat distance = [TMHelper distanceBetween:userCoord and:targetCoord];
    CLLocationDirection direction = [TMHelper directionFromCoordinate:userCoord to:targetCoord];
    BOOL usingDegree = [[[NSUserDefaults standardUserDefaults] objectForKey:kDirectionFormatDegree] boolValue];
    BOOL usingMetric = [[[NSUserDefaults standardUserDefaults] objectForKey:kDistanceUnitMetric] boolValue];
    if (usingDegree) {
        _directionLabel.text = [NSString stringWithFormat:@"%d dge", (int)direction];
    } else {
        int clock = round(direction / 30.0);
        clock = clock ? clock : 12;
        _directionLabel.text = [NSString stringWithFormat:@"%d clock", clock];
    }
    
    if (usingMetric) {
        if (distance > 1000) {
            _distanceLabel.text = [NSString stringWithFormat:@"%.1f km", distance / 1000.0];
        } else {
            _distanceLabel.text = [NSString stringWithFormat:@"%.0f m", distance];
        }
    } else {
        distance = distance / 0.9144;
        if (distance > 1760) {
            _distanceLabel.text = [NSString stringWithFormat:@"%.1f mile", distance / 1760.0];
        } else {
            _distanceLabel.text = [NSString stringWithFormat:@"%.1f yard", distance];            
        }
    }
    
    if (_annotation == [TMAnnotationManager sharedManager].userAnnotation) {
        _tagTextfield.text = [NSString stringWithFormat:@"%@(Me)", _annotation.desc];
    } else {
        _tagTextfield.placeholder = [TMAnnotationManager descriptionWithAnnotationType:_annotation.type];
        _tagTextfield.text = _annotation.desc;
    }
    NSUInteger seconds = [[NSDate date] timeIntervalSince1970] - _annotation.updatedAt / 1000;
    if (seconds < 60) {
        _timeLabel.text = [NSString stringWithFormat:@"%lu sec ago", seconds];
    } else {
        _timeLabel.text = [NSString stringWithFormat:@"%lu min ago", seconds / 60];
    }
}

- (BOOL)becomeFirstResponder {
    _tagTextfield.text = [_annotation.desc length] ? _annotation.desc : nil;
    return [_tagTextfield becomeFirstResponder];
}

- (BOOL)resignFirstResponder {
    _annotation.desc = _tagTextfield.text ? _tagTextfield.text : @"";
    _tagTextfield.text = [_annotation.desc length] ? _annotation.desc : [TMAnnotationManager descriptionWithAnnotationType:_annotation.type];
    
    [_tagTextfield endEditing:YES];
    return [_tagTextfield resignFirstResponder];
}

- (BOOL)isFirstResponder {
    return [_tagTextfield isFirstResponder];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
}

//- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
//    
//}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
}
@end
