//
//  TMAnnotationView.m
//  TacticalMapSystem
//
//  Created by chen zhejun on 9/28/12.
//  Copyright (c) 2012 chen zhejun. All rights reserved.
//

#import "TMAnnotationView.h"
#import "TMAnnotationManager.h"

@interface TMAnnotationView()
@end

@implementation TMAnnotationView {
    UIImageView *_selectedBoundView;
    UILabel *_tagLabel;

    /**
     A flag to indicate whether this annotation is in the middle of being dragged
     */
    BOOL isMoving;
    
    /**
     The horizontal distance in pixels from the center of the annotation to the point
     at which the user touched and held the annotation. This annotation has a large
     'handle' so that it can be dragged without the center being obscured by the user's
     finger.
     */
    float touchXOffset;
    
    /**
     The vertical distance in pixels from the center of the annotation to the point
     at which the user touched and held the annotation. This annotation has a large
     'handle' so that it can be dragged without the center being obscured by the user's
     finger.
     */
    float touchYOffset;
}

- (id)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    self.canShowCallout = NO;
    self.frame = CGRectMake(0, 0, 44, 44);
    
    UIImage *image = [UIImage imageNamed:@"map_obj_selected_bound"];
    _selectedBoundView = [[UIImageView alloc] initWithImage:image];
    _selectedBoundView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin |
                                            UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    _selectedBoundView.center = self.center;
    [self insertSubview:_selectedBoundView atIndex:0];
    _selectedBoundView.hidden = YES;
    [self clearTouch];

    _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 35.0, 35.0)];
    _imageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin |
    UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    CGPoint pos = CGPointMake(ceilf((self.bounds.size.width - _imageView.bounds.size.width) / 2.0),
                              ceilf((self.bounds.size.height - _imageView.bounds.size.height) / 2.0));
    _imageView.frame = CGRectMake(pos.x, pos.y, _imageView.bounds.size.width, _imageView.bounds.size.height);
    [self addSubview:_imageView];

    CGRect f = CGRectMake(ceilf((self.bounds.size.width - 50.0) / 2.0), 35.0, 50.0, 15.0);
    _tagLabel = [[UILabel alloc] initWithFrame:f];
    _tagLabel.backgroundColor = [UIColor clearColor];
    _tagLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16.0];
    _tagLabel.textColor = [UIColor grayColor];
    _tagLabel.textAlignment = NSTextAlignmentCenter;
    _tagLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin |
    UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    _tagLabel.hidden = YES;
    [self addSubview:_tagLabel];
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)refresh {
    self.draggable = NO;
    
    _imageView.image = [[TMAnnotationManager sharedManager] imageWithAnnotation:self.annotation];
    if ([self.annotation isKindOfClass:[TMAnnotation class]]) {
        TMAnnotation *tmAnnot = self.annotation;
        [self setTagText:tmAnnot.desc];
        [self setRotationAngle:DEG_TO_RAD(tmAnnot.heading)];
        self.draggable = tmAnnot.type > TMAnnotationTypeUser ? YES : NO;
    } else {
        [self setRotationAngle:DEG_TO_RAD(0)];
    }
}

- (void)setAnnotation:(id<MKAnnotation>)annotation {
    [super setAnnotation:annotation];
    [self refresh];
}

- (void)setDragState:(MKAnnotationViewDragState)newDragState animated:(BOOL)animated
{
    if (newDragState == MKAnnotationViewDragStateStarting)
    {
        self.dragState = MKAnnotationViewDragStateDragging;
    }
    else if (newDragState == MKAnnotationViewDragStateEnding)
    {
        self.dragState = MKAnnotationViewDragStateNone;
    }
    else if (newDragState == MKAnnotationViewDragStateCanceling)
    {
        self.dragState = MKAnnotationViewDragStateNone;
    }
}

- (void)setOffsetsFromTouch:(UITouch *)touch
{
    CGPoint point = [touch locationInView:self];
    touchXOffset = point.x - self.bounds.size.width / 2.0;
    touchYOffset = point.y - self.bounds.size.height / 2.0;
}

/**
 Set the coordinate of the MKPointAnnotation by adding the touchXOffset and
 touchYOffset to the touch and then using the MKMapView to translate the pixel
 location to a map coordinate.
 */
- (void)updateCoordinateFromTouch:(UITouch *)touch
{
    CGPoint point = [touch locationInView:nil]; // nil means "use the application window"
    point.x -= touchXOffset;
    point.y -= touchYOffset;    
    NSLog(@"set %@", NSStringFromCGPoint(point));
    CLLocationCoordinate2D newCoordinate = [self.mapView convertPoint:point
                                            toCoordinateFromView:nil]; // nil means "use the application window"
//    self.annotation.coordinate = newCoordinate;
}

/**
 Reset the touch tracking member variables to an initial state.
 */
- (void)clearTouch
{
    touchXOffset = 0.0;
    touchYOffset = 0.0;
}

-(void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
{
    [super touchesBegan:touches withEvent:event];
    [self setOffsetsFromTouch:[[event allTouches] anyObject]];
    if (_delegate && [_delegate respondsToSelector:@selector(moveAnnotationBegin:)]) {
        [_delegate moveAnnotationBegin:self.annotation];
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesMoved:touches withEvent:event];
    
    if (self.dragState == MKAnnotationViewDragStateDragging) {
        [self updateCoordinateFromTouch:[[event allTouches] anyObject]];
        if (_delegate && [_delegate respondsToSelector:@selector(movingAnnotation:)]) {
            [_delegate movingAnnotation:self.annotation];
        }
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    if (_delegate && [_delegate respondsToSelector:@selector(movedAnnotation:)]) {
        [_delegate movedAnnotation:self.annotation];
    }
    [self clearTouch];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesCancelled:touches withEvent:event];
    [self clearTouch];
}

- (void)setTagText:(NSString *)text {
    _tagLabel.text = text;
}

- (void)setShowTag:(BOOL)showTag {
    _showTag = showTag;
    _tagLabel.hidden = !_showTag;
}

- (void)setRotationAngle:(CGFloat)angle {
    _imageView.transform = CGAffineTransformMakeRotation(angle);
}

- (void)showSelectedBound {
    _selectedBoundView.hidden = NO;
    [UIView animateWithDuration:0.4
                          delay:0.0
                        options:UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse
                     animations:^{
                         _selectedBoundView.transform = CGAffineTransformMakeScale(1.5, 1.5);
                     }
                     completion:^(BOOL finished){
                     }];
}

- (void)hideSelectedBound {
    [UIView animateWithDuration:0.1
                     animations:^{
                         _selectedBoundView.transform = CGAffineTransformMakeScale(2.0, 2.0);
                         _selectedBoundView.alpha = 0.0;
                     }
                     completion:^(BOOL finished){
                         _selectedBoundView.hidden = YES;
                         _selectedBoundView.alpha = 1.0;
                         _selectedBoundView.transform = CGAffineTransformIdentity;
                     }];
}

@end
