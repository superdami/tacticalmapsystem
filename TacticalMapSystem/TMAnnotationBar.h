//
//  TMAnnotationBar.h
//  TacticalMapSystem
//
//  Created by chen zhejun on 2015/05/12.
//  Copyright (c) 2015年 chen zhejun. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol TMAnnotationBarDelegate <NSObject>
- (void)unfocusSelectedAnnotation:(TMAnnotation *)annotation;
- (void)removeSelectedAnnotation:(TMAnnotation *)annotation;
- (void)editTagWithSelectedAnnotation:(TMAnnotation *)annotation;
- (void)centerSelectedAnnotation:(TMAnnotation *)annotation;
@end

@interface TMAnnotationBar : UIView
@property (nonatomic, weak) id<TMAnnotationBarDelegate> delegate;
@property (nonatomic, strong)TMAnnotation *selectedAnnotation;
- (void)showAnimated:(BOOL)animated;
- (void)hideAnimated:(BOOL)animated;
@end
