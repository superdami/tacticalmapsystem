//
//  Constants.h
//  TacticalMapSystem
//
//  Created by Chen Zhejun on 7/17/14.
//  Copyright (c) 2014 chen zhejun. All rights reserved.
//

#if (TARGET_IPHONE_SIMULATOR)
#   define kBaseHostName  @"127.0.0.1"
#   define kBaseHostSchema @"http://"
#   define kBaseHostPort @"3000"
#else
#ifdef DEBUG
#   define kBaseHostName  @"10.0.1.4"
#   define kBaseHostSchema @"http://"
#   define kBaseHostPort @"3000"
#else
#   define kBaseHostName  @"127.0.0.1"
#   define kBaseHostSchema @"https://"
#   define kBaseHostPort @"3000"
#endif
#endif

#define kAPIBasicURL                                @""
#define kTokenKey                                   @"x-access-token"
#define kCeilfHalf(x)     ceilf(x / 2.0)
#define RAD_TO_DEG(r) ((r) * (180 / M_PI))
#define DEG_TO_RAD(d) ((d) / 180 * M_PI)

static NSString *const kBaseURL = kBaseHostSchema kBaseHostName @":" kBaseHostPort;
static int const kAPIServerErrorCode = 500;

#define kShowAnnotationTag @"ShowAnnotationTag"
#define kDistanceUnitMetric @"DistanceUnitMetric"
#define kDirectionFormatDegree @"DirectionFormatDegree"