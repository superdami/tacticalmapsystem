//
//  TMWaypointView.h
//  TacticalMapSystem
//
//  Created by Chen Zhejun on 2014/02/11.
//  Copyright (c) 2014年 chen zhejun. All rights reserved.
//

#import "TMAnnotationView.h"

@interface TMWaypointView : TMAnnotationView
- (void)setLabelNumber:(NSInteger)number;
@end