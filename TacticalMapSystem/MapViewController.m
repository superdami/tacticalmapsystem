//
//  MapViewController.m
//  TacticalMapSystem
//
//  Created by chen zhejun on 9/20/12.
//  Copyright (c) 2012 chen zhejun. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "KLCPopup.h"

#import "MapViewController.h"
#import "TMAnnotationView.h"
#import "TMWaypointView.h"
#import "PopMenuView.h"

#import "TMRouteRenderer.h"
#import "TMRouteOverlay.h"
#import "TMHelper.h"
#import "TMAnnotationManager.h"
#import "TMGameManager.h"

#import "TMSideMenuView.h"
#import "TMPopupView.h"
#import "TMAnnotationBar.h"
#import "TMAnnotationDetailView.h"

#import "TMCompassAnnotation.h"
#import "TMCompassAnnotationView.h"
#import "CountDownView.h"
#import "TimeCounterView.h"

@interface MapViewController ()<MKMapViewDelegate, CLLocationManagerDelegate, UIGestureRecognizerDelegate, TMPopupViewDelegate, TMAnnotationBarDelegate, TMGameManagerDelegate, TMSlideMenuViewDelegate>
@property (nonatomic, assign)BOOL hasSelection;
@property (nonatomic)UIView *baseView;
@property (nonatomic)TMSideMenuView *leftMenuView;
@property (nonatomic)TMAnnotationDetailView *detailView;
@property (nonatomic)MKMapView *mapView;
@property (nonatomic)UIToolbar *bottomToolBar;
@property (nonatomic)TMPopupView *bottomPopupView;
@property (nonatomic)TMAnnotationBar *annotationBar;
@end

@implementation MapViewController {
    BOOL _trackingUserMode;
    
    CLLocationManager *_locationManager;
    PopMenuView *_popMenuView;
    
    UITapGestureRecognizer *_tapGesture;
    UILongPressGestureRecognizer *_longPressGesture;
    
    BOOL _locationUpdated;
    
    UINavigationBar *_topBar;
    
    TMCompassAnnotation *_compaseAnnotation;
    TMCompassAnnotationView *_compaseAnnotationView;
    BOOL _toolBarHidden;
    
    UIButton *_closeKeyboardCover;
    CountDownView *_countDownView;
    TimeCounterView *_startedTimer;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _trackingUserMode = NO;
    
    _baseView = [[UIView alloc] initWithFrame:self.view.bounds];
    _baseView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    _baseView.backgroundColor = [UIColor grayColor];
    _baseView.layer.shadowColor = [UIColor blackColor].CGColor;
    _baseView.layer.shadowOffset = CGSizeMake(-4.0, 0.0);
    _baseView.layer.shadowRadius = 4.0;
    _baseView.layer.shadowOpacity = 0.5;
    
    _baseView.clipsToBounds = NO;
    [self.view addSubview:_baseView];
    
    _mapView = [[MKMapView alloc] initWithFrame:self.view.bounds];
    _mapView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    _mapView.mapType = MKMapTypeSatellite;
    _mapView.pitchEnabled = NO;
    _mapView.showsUserLocation = NO;
    _mapView.showsPointsOfInterest = NO;
    _mapView.delegate = self;
    [_mapView setZoomEnabled:YES];
    [_baseView addSubview:_mapView];
    
    _locationManager = [[CLLocationManager alloc] init];
    [_locationManager requestAlwaysAuthorization];
    _locationManager.delegate = self;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    _locationManager.headingFilter = 1.0;
    _locationManager.distanceFilter = kCLDistanceFilterNone;
    
    _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    _tapGesture.numberOfTapsRequired = 1;
    _tapGesture.numberOfTouchesRequired = 1;
    _tapGesture.delegate = self;
    [_mapView addGestureRecognizer:_tapGesture];
    
    _longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    _longPressGesture.minimumPressDuration = 0.3;
    _longPressGesture.numberOfTouchesRequired = 1;
    _longPressGesture.delegate = self;
    [_mapView addGestureRecognizer:_longPressGesture];
    
    [_tapGesture requireGestureRecognizerToFail:_longPressGesture];
    _popMenuView = [[PopMenuView alloc] initWithFrame:CGRectMake(0.0, 0.0, 211.0, 211.0)];
    _popMenuView.hidden = YES;
    [_baseView addSubview:_popMenuView];
    [self resetPopMenuStatus];
    self.navigationController.navigationBarHidden = YES;
    
    _compaseAnnotation = [[TMCompassAnnotation alloc] init];
    _compaseAnnotationView = [[TMCompassAnnotationView alloc] initWithAnnotation:_compaseAnnotation reuseIdentifier:nil];
    _compaseAnnotationView.draggable = NO;
    _compaseAnnotationView.enabled = NO;
    
    _leftMenuView = [[TMSideMenuView alloc] initWithFrame:CGRectMake(0.0, 0.0, 240.0, self.view.frame.size.height)];
    _leftMenuView.delegate = self;
    _leftMenuView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin;
    [self.view insertSubview:_leftMenuView atIndex:0];

    [self initTopBar];
    [self initBottomBars];
    _toolBarHidden = NO;
    
    _closeKeyboardCover = [UIButton buttonWithType:UIButtonTypeCustom];
    _closeKeyboardCover.frame = self.view.bounds;
    _closeKeyboardCover.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    _closeKeyboardCover.hidden = YES;
    _closeKeyboardCover.backgroundColor = [UIColor clearColor];
    
    __weak typeof(self) weakMe = self;
    [_closeKeyboardCover handleControlEvents:UIControlEventTouchUpInside withBlock:^(id weakSender) {
        if (weakMe.detailView.isFirstResponder) {
            [weakMe.detailView resignFirstResponder];
            
            MKAnnotationView* anView = [weakMe.mapView viewForAnnotation:weakMe.detailView.annotation];
            if ([anView isKindOfClass:[TMAnnotationView class]]){
                [(TMAnnotationView *)anView setTagText:weakMe.detailView.annotation.desc];
            }
            [[TMAnnotationManager sharedManager] updateAnnotation:weakMe.detailView.annotation];
        }
    }];
    
    [[UIApplication sharedApplication].keyWindow addSubview:_closeKeyboardCover];
    
    _countDownView = [[CountDownView alloc] initWithFrame:self.view.frame];
    _countDownView.hidden = YES;
    [_baseView addSubview:_countDownView];
    
    [TMAnnotationManager sharedManager].mapView = _mapView;
    [[TMAnnotationManager sharedManager] loadAnnotations];
    
    [[TMGameManager sharedManager] addDelegate:self];
    [[TMGameManager sharedManager] resetGameStatus];
}

- (void)initTopBar {
    _topBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, 64.0)];
    _topBar.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
    [_baseView addSubview:_topBar];
    
    __weak typeof(self) weakMe = self;
    UIBarButtonItem *leftMenu = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose block:^(id weakSender) {
        [UIView animateWithDuration:0.2 animations:^{
            CGRect f = weakMe.baseView.frame;
            if (f.origin.x) {
                f.origin.x = 0.0;
            } else {
                f.origin.x = weakMe.leftMenuView.frame.size.width;
            }
            weakMe.baseView.frame = f;
        }];
    }];
    
    _startedTimer = [[TimeCounterView alloc] initWithFrame:CGRectMake(0.0, 0.0, 200.0, 44.0)];
    UINavigationItem *topItems = [[UINavigationItem alloc] init];
    [topItems setTitleView:_startedTimer];
    [topItems setLeftBarButtonItem:leftMenu];
    [_topBar setItems:@[topItems]];
}

- (void)initBottomBars {
    UIButton *userLocation = [UIButton buttonWithType:UIButtonTypeContactAdd];
    userLocation.frame = CGRectMake(10.0, 0.0, 44.0, 44.0);
    userLocation.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
    __weak typeof(self) weakMe = self;
    [userLocation handleControlEvents:UIControlEventTouchUpInside withBlock:^(id weakSender) {
        if (weakMe.mapView.userTrackingMode == MKUserTrackingModeFollowWithHeading) {
            weakMe.mapView.userTrackingMode = MKUserTrackingModeNone;
        } else {
            weakMe.mapView.userTrackingMode = MKUserTrackingModeFollowWithHeading;
        }
    }];
    
    _bottomPopupView = [[TMPopupView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, 200.0)];
    _bottomPopupView.delegate = self;
    _bottomPopupView.mapType = _mapView.mapType;
    _bottomPopupView.userStatus = 0;
    
    UIButton *mapMenu = [UIButton buttonWithType:UIButtonTypeInfoLight];
    mapMenu.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    mapMenu.frame = CGRectMake(self.view.frame.size.width - 54.0, 0.0, 44.0, 44.0);
    [mapMenu handleControlEvents:UIControlEventTouchUpInside withBlock:^(id weakSender) {
        KLCPopupLayout layout = KLCPopupLayoutMake(KLCPopupHorizontalLayoutCenter,
                                                   KLCPopupVerticalLayoutBottom);
        
        KLCPopup* popup = [KLCPopup popupWithContentView:weakMe.bottomPopupView
                                                showType:KLCPopupShowTypeSlideInFromBottom
                                             dismissType:KLCPopupDismissTypeSlideOutToBottom
                                                maskType:KLCPopupMaskTypeDimmed
                                dismissOnBackgroundTouch:YES
                                   dismissOnContentTouch:NO];
        
        [popup showWithLayout:layout];
    }];
    
    _bottomToolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0, self.view.frame.size.height - 44.0, self.view.frame.size.width, 44.0)];
    _bottomToolBar.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    [_bottomToolBar addSubview:userLocation];
    [_bottomToolBar addSubview:mapMenu];
    [_baseView addSubview:_bottomToolBar];
    
    _annotationBar = [[TMAnnotationBar alloc] initWithFrame:CGRectZero];
    _annotationBar.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
    _annotationBar.delegate = self;
    _annotationBar.hidden = YES;
    [_baseView addSubview:_annotationBar];

    CGFloat width = MIN([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    _detailView = [[TMAnnotationDetailView alloc] initWithFrame:CGRectMake((self.view.frame.size.width - width) / 2.0, self.view.frame.size.height, width, 60.0)];
    _detailView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
    _detailView.alpha = 0.0;
    [_baseView addSubview:_detailView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    ((TMNavigationController *)self.navigationController).enableAutorotate = YES;

    _locationUpdated = NO;
    [_locationManager startUpdatingLocation];
    [_locationManager startUpdatingHeading];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:@"UIKeyboardWillShowNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:@"UIKeyboardWillHideNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChangeFrame:) name:@"UIKeyboardWillChangeFrameNotification" object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)dealloc {
    [_locationManager stopUpdatingHeading];
    [_locationManager stopUpdatingLocation];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [_locationManager setHeadingOrientation:[UIDevice currentDevice].orientation];
}

- (void)resetUserLocationVisibleAnimated:(BOOL)animated {
    MKCoordinateRegion newRegion;
    newRegion.center.latitude = [TMAnnotationManager sharedManager].userAnnotation.coordinate.latitude;
    newRegion.center.longitude = [TMAnnotationManager sharedManager].userAnnotation.coordinate.longitude;
    newRegion.span.latitudeDelta = 0.001;
    newRegion.span.longitudeDelta = 0.001;
    [_mapView setRegion:newRegion animated:animated];
}

- (void)resetPopMenuStatus {
    __weak typeof(_mapView) weakMapView = _mapView;
    if ([_mapView.selectedAnnotations count]) {
        TMAnnotation *annotation = (TMAnnotation *)[_mapView.selectedAnnotations objectAtIndex:0];
        if (annotation == [TMAnnotationManager sharedManager].userAnnotation) {
            [_popMenuView setMenuWithTypes:nil annotationType:nil handleBlock:nil];
        }
        else if ([annotation isKindOfClass:[TMAnnotation class]]) {
            TMAnnotation *theAnnotation = (TMAnnotation *)annotation;
            if (theAnnotation.type == TMAnnotationTypeHostile || theAnnotation.type == TMAnnotationTypeHostiles) {
                [_popMenuView setMenuWithTypes:@[@(TMCircleViewTypeTop)] annotationType:@[@(TMAnnotationTypeActionMove)] handleBlock:^(TMAnnotationType typeSelected, CGPoint markPoint) {
                    [[TMAnnotationManager sharedManager] moveAnnotation:annotation withPoint:markPoint];
                }];
            }
        }
    }
    else {
        [_popMenuView setMenuWithTypes:@[@(TMCircleViewTypeLeft), @(TMCircleViewTypeTop), @(TMCircleViewTypeRight)] annotationType:@[@(TMAnnotationTypeLandmark), @(TMAnnotationTypeHostile), @(TMAnnotationTypeHostiles)] handleBlock:^(TMAnnotationType selectedType, CGPoint markPoint) {
            __strong typeof(weakMapView) strongMapView = weakMapView;
            CLLocationCoordinate2D coordinate = [strongMapView convertPoint:markPoint toCoordinateFromView:strongMapView];
            if (selectedType == TMAnnotationTypeHostile) {
                TMAnnotation *targetAnnot = [[TMAnnotation alloc] init];
                targetAnnot.type = TMAnnotationTypeHostile;
                targetAnnot.coordinate = coordinate;
                [[TMAnnotationManager sharedManager] markAnnotation:targetAnnot];
            }
            else if (selectedType == TMAnnotationTypeHostiles) {
                TMAnnotation *targetAnnot = [[TMAnnotation alloc] init];
                targetAnnot.type = TMAnnotationTypeHostiles;
                targetAnnot.coordinate = coordinate;
                [[TMAnnotationManager sharedManager] markAnnotation:targetAnnot];
            } else {
                
            }
        }];
    }
}

- (void)forceExit {
    [[TMAnnotationManager sharedManager] close];
    [[TMGameManager sharedManager] close];
    [[TMGameManager sharedManager] removeDelegate:self];

    if(![TMAnnotationManager sharedManager].offlineMode) [[TMAPIClient sharedInstance] leaveRoom];
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)handleLongPress:(UILongPressGestureRecognizer *)gesture {
    if ([gesture state] == UIGestureRecognizerStateBegan) {
        [_popMenuView showAnimatedPosition:[gesture locationInView:self.view]];
    }
    if ([gesture state] == UIGestureRecognizerStateChanged) {
        [_popMenuView handleGestureChange:gesture];
    }
    if ([gesture state] == UIGestureRecognizerStateEnded || [gesture state] == UIGestureRecognizerStateCancelled) {
        [_popMenuView handleGestureEnd:gesture];
        [_popMenuView hideAnimated];
    }
}

- (void)setToolBarHidden:(BOOL)hidden animated:(BOOL)animated {
    [UIView animateWithDuration:animated ? 0.2 : 0.0 animations:^{
        CGFloat topY;
        CGFloat bottomY;
        if (hidden) {
            topY = - _topBar.frame.size.height;
            bottomY = self.view.frame.size.height;
        } else {
            topY = 0.0;
            bottomY = self.view.frame.size.height - 44.0;
        }
        _topBar.frame = CGRectMake(0.0, topY, _topBar.frame.size.width, _topBar.frame.size.height);
        _bottomToolBar.frame = CGRectMake(0.0, bottomY, _bottomToolBar.frame.size.width, _bottomToolBar.frame.size.height);
    }];
}

- (void)setAnnotationControlHidden:(BOOL)hidden animated:(BOOL)animated {
    [UIView animateWithDuration:animated ? 0.2 : 0.0 animations:^{
        CGRect f = _detailView.frame;
        if (hidden) {
            f.origin.y = self.view.frame.size.height;
        } else {
            f.origin.y = self.view.frame.size.height - _detailView.frame.size.height;
        }
        _detailView.frame = f;
        _detailView.alpha = hidden ? 0.0 : 1.0;
    }];
}

- (void)handleTapGesture:(UITapGestureRecognizer *)gesture {
    if ([gesture state] == UIGestureRecognizerStateEnded) {
        _toolBarHidden = !_toolBarHidden;
        [self setToolBarHidden:_toolBarHidden animated:YES];
    }
}

- (void)keyboardWillShow:(NSNotification *)noti {
    if ([_detailView isFirstResponder]) {
        CGFloat t = [[noti.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
        _closeKeyboardCover.hidden = NO;
        [UIView animateWithDuration:t animations:^{
            _annotationBar.alpha = 0.0;
        }];
    }
}

- (void)keyboardWillHide:(NSNotification *)noti {
    if ([_detailView isFirstResponder]) {
        _closeKeyboardCover.hidden = YES;
        CGFloat t = [[noti.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
        [UIView animateWithDuration:t animations:^{
            _annotationBar.alpha = 1.0;
        }];
    }
}

- (void)keyboardWillChangeFrame:(NSNotification *)noti {
    if ([_detailView isFirstResponder]) {
        CGFloat y = [[noti.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].origin.y;
        CGFloat t = [[noti.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
        CGRect f = _detailView.frame;
        f.origin.y = y - _detailView.frame.size.height;
        
        [UIView animateWithDuration:t animations:^{
            _detailView.frame = f;
        }];
    }
}

#pragma mark - MKMapView delegate
- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    NSLog(@"anno selected");
    if ([view.annotation isKindOfClass:[TMAnnotation class]]) {
        _annotationBar.selectedAnnotation = view.annotation;
        _detailView.annotation = view.annotation;
    } else if (view.annotation == _mapView.userLocation) {
        _annotationBar.selectedAnnotation = [TMAnnotationManager sharedManager].userAnnotation;
        _detailView.annotation = [TMAnnotationManager sharedManager].userAnnotation;
    }
    
    CGRect f = _annotationBar.frame;
    f.origin.x = self.view.frame.size.width - _annotationBar.frame.size.width;
    f.origin.y = (self.view.frame.size.height - _annotationBar.frame.size.height) / 2.0;
    _annotationBar.frame = f;
    
    [(TMAnnotationView *)view showSelectedBound];
    [self resetPopMenuStatus];
    
    if (_hasSelection) {
        [_annotationBar showAnimated:NO];
    } else {
        [self setAnnotationControlHidden:NO animated:YES];
        [_annotationBar showAnimated:YES];
        [self setToolBarHidden:YES animated:YES];
    }
    _hasSelection = YES;
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
    [(TMAnnotationView *)view hideSelectedBound];
    [self resetPopMenuStatus];

    __weak typeof(self) weakMe = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (![_mapView.selectedAnnotations count]) {
            [weakMe setAnnotationControlHidden:YES animated:YES];
            [weakMe setToolBarHidden:_toolBarHidden animated:YES];
            [_annotationBar hideAnimated:YES];
            weakMe.hasSelection = NO;
        }
    });
}

- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views {
    for (MKAnnotationView *view in views) {
        if ([view isKindOfClass:[TMCompassAnnotationView class]]) {
            continue;
        }
        
        MKMapPoint point =  MKMapPointForCoordinate(view.annotation.coordinate);
        if (!MKMapRectContainsPoint(_mapView.visibleMapRect, point)) {
            continue;
        }
        CGRect endFrame = view.frame;
        view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y - self.view.frame.size.height, view.frame.size.width, view.frame.size.height);
        [UIView animateWithDuration:0.5 delay:0.04*[views indexOfObject:view] options:UIViewAnimationCurveLinear animations:^{
            view.frame = endFrame;
        }completion:^(BOOL finished){
            if (finished) {
                [UIView animateWithDuration:0.05 animations:^{
                    view.transform = CGAffineTransformMakeScale(1.0, 0.8);
                    
                }completion:^(BOOL finished){
                    [UIView animateWithDuration:0.1 animations:^{
                        view.transform = CGAffineTransformIdentity;
                    }];
                }];
            }
        }];
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    if ([annotation isKindOfClass:[TMCompassAnnotation class]]) {
        return _compaseAnnotationView;
    }
    
    NSString *annotationIdentifier = @"PersonAnnotationIdentifier";
    TMAnnotationView *annotView = (TMAnnotationView*)[_mapView dequeueReusableAnnotationViewWithIdentifier:annotationIdentifier];
    if (!annotView) {
        annotView = [[TMAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationIdentifier];
    }
    annotView.showTag = [[[NSUserDefaults standardUserDefaults] objectForKey:kShowAnnotationTag] boolValue];
    annotView.annotation = annotation;
    
    return annotView;
}

- (void)mapView:(MKMapView *)mapView didChangeUserTrackingMode:(MKUserTrackingMode)mode animated:(BOOL)animated {
    if (mode == MKUserTrackingModeNone) {
        _trackingUserMode = NO;
        [_mapView removeAnnotation:_compaseAnnotation];
        [self resetUserLocationVisibleAnimated:NO];
    }
    else if (mode == MKUserTrackingModeFollow) {
        _mapView.userTrackingMode = MKUserTrackingModeFollowWithHeading;
    }
    else if (mode == MKUserTrackingModeFollowWithHeading) {
        _trackingUserMode = YES;
        [_mapView addAnnotation:_compaseAnnotation];
        TMAnnotationView *view = (TMAnnotationView *)[_mapView viewForAnnotation:[TMAnnotationManager sharedManager].userAnnotation];
        view.imageView.transform = CGAffineTransformIdentity;
    }
}

- (BOOL)mapViewRegionDidChangeFromUserInteraction
{
    UIView *view = _mapView.subviews[0];
    for(UIGestureRecognizer *recognizer in view.gestureRecognizers) {
        if((recognizer.state == UIGestureRecognizerStateBegan || recognizer.state == UIGestureRecognizerStateEnded) &&
           [recognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
            return YES;
        }
    }
    
    return NO;
}

- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated {
    if ([self mapViewRegionDidChangeFromUserInteraction]) {
        if (!_toolBarHidden) {
            _toolBarHidden = YES;
            [self setToolBarHidden:YES animated:YES];
        }
    }
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    
}
#pragma mark - CLLocationManager Delegate
- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading {
    CGFloat headingAngle = DEG_TO_RAD(newHeading.trueHeading);
    if (!_trackingUserMode) {
        TMAnnotationView *userView = (TMAnnotationView *)[_mapView viewForAnnotation:[TMAnnotationManager sharedManager].userAnnotation];
        [TMAnnotationManager sharedManager].userAnnotation.heading = newHeading.trueHeading;
        [UIView animateWithDuration:0.05 animations:^{
            [userView setRotationAngle:headingAngle];
        }];
    } else {
        _compaseAnnotationView.heading = -headingAngle;
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *location = [locations lastObject];
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
    [TMAnnotationManager sharedManager].userAnnotation.coordinate = coordinate;
    if (_trackingUserMode) {
        _compaseAnnotation.coordinate = coordinate;
    }
    
    if (!_locationUpdated) {
        [self resetUserLocationVisibleAnimated:YES];
        _locationUpdated = YES;
    }
    [[TMAnnotationManager sharedManager] updateUserStatusForce:NO];
}

#pragma mark - UIGesture methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRequireFailureOfGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    if (gestureRecognizer == _tapGesture) {
        if ([otherGestureRecognizer isKindOfClass:[UITapGestureRecognizer class]]) {
            UITapGestureRecognizer *otherTap = (UITapGestureRecognizer *)otherGestureRecognizer;
            if(otherTap.numberOfTapsRequired == 2) {
                return YES;
            }
        }
    }
    return NO;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if (gestureRecognizer == _tapGesture) {
        if ([_mapView.selectedAnnotations count]) {
            return NO;
        } else {
            CGPoint p = [touch locationInView:_mapView];
            MKMapRect visibleMapRect = _mapView.visibleMapRect;
            NSSet *visibleAnnotations = [_mapView annotationsInMapRect:visibleMapRect];
            for ( NSObject<MKAnnotation> *annotation in visibleAnnotations ){
                UIView *av = [_mapView viewForAnnotation:annotation];
                if( CGRectContainsPoint(av.frame, p) ){
                    return NO;
                }
            }
        }
    }
    return YES;
}

#pragma mark - TMPopupView Delegate
- (void)mapTypeSelected:(MKMapType)type {
    _mapView.mapType = type;
}

- (void)userStatusSelected:(TMAnnotationStatus)status {
    if ([TMAnnotationManager sharedManager].userAnnotation.status == status) {
        return;
    }
    [TMAnnotationManager sharedManager].userAnnotation.status = status;
    [[TMAnnotationManager sharedManager] updateUserStatusForce:YES];
}

- (void)lockRotateStatusChanged:(BOOL)locked {
    ((TMNavigationController *)self.navigationController).enableAutorotate = !locked;
}

- (void)gameStatusChanged:(BOOL)started {
    [[TMGameManager sharedManager] setGameStarted:started];
}

#pragma mark - TMPopupView Delegate
- (void)unfocusSelectedAnnotation:(TMAnnotation *)annotation {
    [_mapView deselectAnnotation:annotation animated:YES];
}

- (void)removeSelectedAnnotation:(TMAnnotation *)annotation {
    [[TMAnnotationManager sharedManager] dropAnnotation:annotation];
}

- (void)editTagWithSelectedAnnotation:(TMAnnotation *)annotation {
    [_mapView resignFirstResponder];
    [_detailView becomeFirstResponder];
}

- (void)centerSelectedAnnotation:(TMAnnotation *)annotation {
    MKCoordinateRegion newRegion;
    newRegion.center.latitude = annotation.coordinate.latitude;
    newRegion.center.longitude = annotation.coordinate.longitude;
    newRegion.span = _mapView.region.span;
    [_mapView setRegion:newRegion animated:NO];
}

#pragma mark - TMGameManager Delegate
- (void)gameStatusDidChange:(BOOL)started timeTravel:(NSTimeInterval)time {
    _bottomPopupView.gameStarted = started;
    if (started) {
        _countDownView.hidden = time > 0 ? YES : NO;
        if (time > 0) {
            [_startedTimer updateTimeSeconds:time];
            NSLog(@"count down: %d", (int)time);
        } else {
            [_countDownView updateCountDown:time];
            NSLog(@"game has began: %d", (int)time);
        }
    } else {
        [_startedTimer updateTimeSeconds:0];
        _countDownView.hidden = YES;
        NSLog(@"game stop");
    }
}

- (void)gameTimeTravelUpdate:(NSTimeInterval)time {
    _countDownView.hidden = time > 0 ? YES : NO;
    if (time > 0) {
        [_startedTimer updateTimeSeconds:time];
        NSLog(@"count down: %d", (int)time);
    } else {
        [_startedTimer updateTimeSeconds:0];
        [_countDownView updateCountDown:time];
        NSLog(@"game has began: %d", (int)time);
    }    
}

- (void)switchShowAnnotationTag:(BOOL)show {
    MKMapRect visibleMapRect = _mapView.visibleMapRect;
    NSSet *visibleAnnotations = [_mapView annotationsInMapRect:visibleMapRect];
    for ( NSObject<MKAnnotation> *annotation in visibleAnnotations ){
        MKAnnotationView* anView = [_mapView viewForAnnotation:annotation];
        if ([anView isKindOfClass:[TMAnnotationView class]]){
            [(TMAnnotationView *)anView setShowTag:show];
        }
    }
}

- (void)switchUsingUnitMetric:(BOOL)isMetric {
    
}

- (void)switchUsingDegree:(BOOL)isDegree {
    
}

- (void)disconnect {
    [self forceExit];
}

@end
