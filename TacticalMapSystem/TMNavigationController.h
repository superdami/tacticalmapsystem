//
//  TMNavigationController.h
//  TacticalMapSystem
//
//  Created by chen zhejun on 2015/03/28.
//  Copyright (c) 2015年 chen zhejun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMNavigationController : UINavigationController
@property (nonatomic, assign)BOOL enableAutorotate;
@end
