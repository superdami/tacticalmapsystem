//
//  TMTraceManager.m
//  TacticalMapSystem
//
//  Created by Chen Zhejun on 2014/01/09.
//  Copyright (c) 2014年 chen zhejun. All rights reserved.
//

#import "TMTraceManager.h"
#import "TMPaintOverlay.h"
#import "TMPaintRenderer.h"
#import "VideoPlayerController.h"

#define CheckTimeInterval 0.2
#define CheckTimeIntervalHeading 0.1
@implementation TMTraceManager
{
    NSArray *_locationArray;
    NSArray *_headingArray;
    NSUInteger _currentLocationNumber;
    NSUInteger _currentLocationPlayStep;
    
    NSUInteger _currentHeadingNumber;
    NSUInteger _currentHeadingPlayStep;

    NSObject <MKAnnotation> *_tracingAnnotation;
    NSTimer *_playtimeCheckTimer;
}

- (BOOL)loadTraceData {
    NSURL *url = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    NSURL *traceRecordFile = [url URLByAppendingPathComponent:@"userTraceOutput"];
    _isReplayTrace = NO;

    if ([[NSFileManager defaultManager] fileExistsAtPath:[traceRecordFile path]]) {
        NSError *error;
        NSData *data = [NSData dataWithContentsOfURL:traceRecordFile options:NSDataReadingMappedIfSafe error:&error];
        NSDictionary *loadedDic = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        NSLog(@"load heading %@", [loadedDic objectForKey:@"headingRecord"]);
        NSLog(@"load location %@", [loadedDic objectForKey:@"locationRecord"]);
        
        _locationArray = [loadedDic objectForKey:@"locationRecord"];
        _headingArray = [loadedDic objectForKey:@"headingRecord"];
        
        if (!_trackingOverlay) {
            CLLocation *location = [_locationArray objectAtIndex:0];
            _trackingOverlay = [[TMPaintOverlay alloc] initWithCenterCoordinate:location.coordinate];
            _trackingRenderer = [[TMPaintRenderer alloc] initWithOverlay:_trackingOverlay];
            [_delegate addOverlayToMapView:_trackingOverlay];
            _trackingRenderer.lineDash = NO;
            [_trackingRenderer setPathColorWithRed:0.0 green:255.0 blue:255.0 alpha:1.0];
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerPlaybackStateDidChange:) name:MPMoviePlayerPlaybackStateDidChangeNotification object:nil];
        }
        return YES;
    }
    return NO;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackStateDidChangeNotification object:nil];
}

- (void)replayRoutePath {
    _currentLocationNumber = 0;
    _currentLocationPlayStep = 0;
    
    _currentHeadingNumber = 0;
    _currentHeadingPlayStep = 0;

    if ([_locationArray count]) {
        NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:CheckTimeInterval target:self selector:@selector(playStepLocation:) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
        _isReplayTrace = YES;
    }
    
    if ([_headingArray count]) {
        NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:CheckTimeIntervalHeading target:self selector:@selector(playStepHeading:) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
        _isReplayTrace = YES;
    }
}

- (void)playStepLocation:(NSTimer *)timer {
    CLLocation *firstLocation = [_locationArray objectAtIndex:0];
    
    CLLocation *nextLocation = [_locationArray objectAtIndex:_currentLocationNumber];
    NSTimeInterval timeInterval = [nextLocation.timestamp timeIntervalSinceDate:firstLocation.timestamp];
    
    MKMapRect lastRect = MKMapRectNull;
    CLLocation *currentLocation;
    while ((timeInterval < _currentLocationPlayStep * CheckTimeInterval) && (_currentLocationNumber < [_locationArray count])) {
        nextLocation = [_locationArray objectAtIndex:_currentLocationNumber];
        if (_currentLocationNumber > 0) {
            currentLocation = [_locationArray objectAtIndex:_currentLocationNumber - 1];
            MKMapRect updateRect = [_trackingOverlay addLineWithLocation:currentLocation];
            lastRect = MKMapRectUnion(updateRect, lastRect);
        }
        else {
            currentLocation = [_locationArray objectAtIndex:0];
            [_trackingOverlay addPathWithLocation:currentLocation];
        }
        timeInterval = [nextLocation.timestamp timeIntervalSinceDate:firstLocation.timestamp];
        
        _currentLocationNumber++;
    }
    
    if (!MKMapRectEqualToRect(MKMapRectNull, lastRect)) {
        MKZoomScale zoomScale = [_delegate mapCurrentZoomScale];
        [_trackingRenderer updateDrawRect:lastRect zoomScale:zoomScale];
    }
    
    if (currentLocation) {
        _tracingAnnotation.coordinate = currentLocation.coordinate;
    }
    
    if (_currentLocationNumber >= [_locationArray count]) {
        [timer invalidate];
        if(_currentHeadingNumber >= [_headingArray count]) {
            _isReplayTrace = NO;
        }
    }
    _currentLocationPlayStep++;
}

- (void)playStepHeading:(NSTimer *)timer {
    CLHeading *firstHeading = [_headingArray objectAtIndex:0];
    
    CLHeading *nextHeading = [_headingArray objectAtIndex:_currentHeadingNumber];
    NSTimeInterval timeInterval = [nextHeading.timestamp timeIntervalSinceDate:firstHeading.timestamp];
    
    CLHeading *currentHeading;
    while ((timeInterval < _currentHeadingPlayStep * CheckTimeIntervalHeading) && (_currentHeadingNumber < [_headingArray count])) {
        nextHeading = [_headingArray objectAtIndex:_currentHeadingNumber];
        if (_currentHeadingNumber > 0) {
            currentHeading = [_headingArray objectAtIndex:_currentHeadingNumber - 1];
        }
        else {
            currentHeading = [_locationArray objectAtIndex:0];
        }
        timeInterval = [nextHeading.timestamp timeIntervalSinceDate:firstHeading.timestamp];
        _currentHeadingNumber++;
    }
    
    if (currentHeading) {
        MKAnnotationView *view = [_delegate viewForAnnotation:_tracingAnnotation];
        CGFloat angle = currentHeading.trueHeading / 360.0 * M_PI;
        view.transform = CGAffineTransformMakeRotation(angle);
    }
    if (_currentHeadingNumber >= [_headingArray count]) {
        [timer invalidate];
        if (_currentLocationNumber >= [_locationArray count]) {
            _isReplayTrace = NO;
        }
    }
    _currentHeadingPlayStep++;
}

- (void)setCurrentPlayingTime:(NSTimeInterval)time {
    
    if (_currentLocationNumber < [_locationArray count]) {
        CLLocation *firstLocation = [_locationArray objectAtIndex:0];
        CLLocation *nextLocation = [_locationArray objectAtIndex:_currentLocationNumber];
        NSTimeInterval timeInterval = [nextLocation.timestamp timeIntervalSinceDate:firstLocation.timestamp];
        
        MKMapRect lastRect = MKMapRectNull;
        CLLocation *currentLocation;
        while (timeInterval < time && _currentLocationNumber < [_locationArray count]) {
            nextLocation = [_locationArray objectAtIndex:_currentLocationNumber];
            if (_currentLocationNumber > 0) {
                currentLocation = [_locationArray objectAtIndex:_currentLocationNumber - 1];
                MKMapRect updateRect = [_trackingOverlay addLineWithLocation:currentLocation];
                lastRect = MKMapRectUnion(updateRect, lastRect);
            }
            else {
                currentLocation = [_locationArray objectAtIndex:0];
                [_trackingOverlay addPathWithLocation:currentLocation];
            }
            timeInterval = [nextLocation.timestamp timeIntervalSinceDate:firstLocation.timestamp];
            _currentLocationNumber++;
        }
        
        if (!MKMapRectEqualToRect(MKMapRectNull, lastRect)) {
            MKZoomScale zoomScale = [_delegate mapCurrentZoomScale];
            [_trackingRenderer updateDrawRect:lastRect zoomScale:zoomScale];
        }
        
        if (currentLocation) {
            _tracingAnnotation.coordinate = currentLocation.coordinate;
        }
    }
    else {
        if(_currentHeadingNumber >= [_headingArray count]) {
            _isReplayTrace = NO;
        }
    }
    
    if (_currentHeadingNumber < [_headingArray count]) {
        CLHeading *firstHeading = [_headingArray objectAtIndex:0];
        CLHeading *nextHeading = [_headingArray objectAtIndex:_currentHeadingNumber];
        NSTimeInterval timeInterval = [nextHeading.timestamp timeIntervalSinceDate:firstHeading.timestamp];
        CLHeading *currentHeading;
        while (timeInterval < time && _currentHeadingNumber < [_headingArray count]) {
            nextHeading = [_headingArray objectAtIndex:_currentHeadingNumber];
            if (_currentHeadingNumber > 0) {
                currentHeading = [_headingArray objectAtIndex:_currentHeadingNumber - 1];
            }
            else {
                currentHeading = [_locationArray objectAtIndex:0];
            }
            timeInterval = [nextHeading.timestamp timeIntervalSinceDate:firstHeading.timestamp];
            _currentHeadingNumber++;
        }
        
        if (currentHeading) {
            MKAnnotationView *view = [_delegate viewForAnnotation:_tracingAnnotation];
            CGFloat angle = currentHeading.trueHeading / 360.0 * M_PI;
            view.transform = CGAffineTransformMakeRotation(angle);
        }
    }
    else {
        if (_currentLocationNumber >= [_locationArray count]) {
            _isReplayTrace = NO;
        }   
    }
}

- (void)setTracingAnnotation:(NSObject<MKAnnotation> *)annotation {
    _tracingAnnotation = annotation;
}

- (void)playtimeCheck:(NSTimer *)timer {
    MPMoviePlayerController *playerController = timer.userInfo;
    if (playerController.playbackState == MPMoviePlaybackStatePlaying) {
        [self setCurrentPlayingTime:playerController.currentPlaybackTime];
    }
}

- (void)playerPlaybackStateDidChange:(NSNotification *)notification {
    MPMoviePlayerController *playerController = notification.object;
    if (playerController.playbackState == MPMoviePlaybackStateSeekingForward ||
        playerController.playbackState == MPMoviePlaybackStateSeekingBackward ) {
        _currentLocationNumber = 0;
        _currentHeadingNumber = 0;
        if ([_playtimeCheckTimer isValid]) {
            [_playtimeCheckTimer invalidate];
        }
    }
    if (playerController.playbackState == MPMoviePlaybackStateInterrupted ||
        playerController.playbackState == MPMoviePlaybackStateStopped) {
        _currentLocationNumber = 0;
        _currentHeadingNumber = 0;
        if ([_playtimeCheckTimer isValid]) {
            [_playtimeCheckTimer invalidate];
        }
    }
    if (playerController.playbackState == MPMoviePlaybackStatePlaying) {
        if (![_playtimeCheckTimer isValid]) {
            _playtimeCheckTimer = [NSTimer scheduledTimerWithTimeInterval:CheckTimeIntervalHeading target:self selector:@selector(playtimeCheck:) userInfo:playerController repeats:YES];
            [[NSRunLoop mainRunLoop] addTimer:_playtimeCheckTimer forMode:NSRunLoopCommonModes];
        }
    }
    if (playerController.playbackState == MPMoviePlaybackStatePaused) {
        if ([_playtimeCheckTimer isValid]) {
            [_playtimeCheckTimer invalidate];
        }
    }
}
@end
