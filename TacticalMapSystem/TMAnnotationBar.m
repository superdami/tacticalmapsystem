//
//  TMAnnotationBar.m
//  TacticalMapSystem
//
//  Created by chen zhejun on 2015/05/12.
//  Copyright (c) 2015年 chen zhejun. All rights reserved.
//

#import "TMAnnotationBar.h"

#define kButtonSize 38.0
@implementation TMAnnotationBar {
    UIToolbar *_centerBtn, *_unfocusBtn, *_removeBtn, *_tagButton;
    NSArray *_avaiableBtns;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initButtons];
    }
    return self;
}

- (void)initButtons {
    CGRect f = CGRectMake(0.0, 0.0, kButtonSize, kButtonSize);
    UIButton *center = [UIButton buttonWithType:UIButtonTypeCustom];
    center.frame = f;
    [center setTitle:@"center" forState:UIControlStateNormal];
    [center setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
    [center handleControlEvents:UIControlEventTouchUpInside withBlock:^(id weakSender) {
        if (_delegate && [_delegate respondsToSelector:@selector(centerSelectedAnnotation:)]) {
            if (self.selectedAnnotation) [_delegate centerSelectedAnnotation:self.selectedAnnotation];
        }
    }];
    
    UIButton *tag = [UIButton buttonWithType:UIButtonTypeCustom];
    tag.frame = f;
    [tag setTitle:@"tag" forState:UIControlStateNormal];
    [tag setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
    [tag handleControlEvents:UIControlEventTouchUpInside withBlock:^(id weakSender) {
        if (_delegate && [_delegate respondsToSelector:@selector(editTagWithSelectedAnnotation:)]) {
            if (self.selectedAnnotation) [_delegate editTagWithSelectedAnnotation:self.selectedAnnotation];
        }
    }];
    
    UIButton *remove = [UIButton buttonWithType:UIButtonTypeCustom];
    remove.frame = f;
    [remove setTitle:@"remove" forState:UIControlStateNormal];
    [remove setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
    [remove handleControlEvents:UIControlEventTouchUpInside withBlock:^(id weakSender) {
        if (_delegate && [_delegate respondsToSelector:@selector(removeSelectedAnnotation:)]) {
            if (self.selectedAnnotation) [_delegate removeSelectedAnnotation:self.selectedAnnotation];
        }
    }];
    
    UIButton *unfocus = [UIButton buttonWithType:UIButtonTypeCustom];
    unfocus.frame = f;
    [unfocus setTitle:@"close" forState:UIControlStateNormal];
    [unfocus setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
    [unfocus handleControlEvents:UIControlEventTouchUpInside withBlock:^(id weakSender) {
        if (_delegate && [_delegate respondsToSelector:@selector(unfocusSelectedAnnotation:)]) {
            if (self.selectedAnnotation) [_delegate unfocusSelectedAnnotation:self.selectedAnnotation];
        }
    }];
    
    NSArray *btns = @[center, tag, remove, unfocus];
    for (int i = 0; i < [btns count]; i++) {
        UIView *btn = btns[i];
        UIToolbar *bg = [[UIToolbar alloc] initWithFrame:btn.bounds];
        bg.clipsToBounds = YES;
        bg.barStyle = UIBarStyleDefault;
        bg.layer.cornerRadius = bg.frame.size.width / 2.0;
        [bg addSubview:btn];
        [self addSubview:bg];

        if (i == 0) {
            _centerBtn = bg;
        } else if (i == 1) {
            _tagButton = bg;
        } else if (i == 2) {
            _removeBtn = bg;
        } else {
            _unfocusBtn = bg;
        }
    }
}

- (void)setSelectedAnnotation:(TMAnnotation *)selectedAnnotation {
    _avaiableBtns = nil;
    _centerBtn.transform = CGAffineTransformIdentity;
    _tagButton.transform = CGAffineTransformIdentity;
    _removeBtn.transform = CGAffineTransformIdentity;
    _unfocusBtn.transform = CGAffineTransformIdentity;

    _centerBtn.hidden = YES;
    _tagButton.hidden = YES;
    _removeBtn.hidden = YES;
    _unfocusBtn.hidden = YES;
    
    _selectedAnnotation = selectedAnnotation;
    if (_selectedAnnotation.type == TMAnnotationTypeUser) {
        _avaiableBtns = @[_centerBtn, _unfocusBtn];
    } else {
        _avaiableBtns = @[_centerBtn, _tagButton, _removeBtn, _unfocusBtn];
    }
    
    CGFloat h = [_avaiableBtns count] * kButtonSize + ([_avaiableBtns count] - 1) * 20.0;
    CGRect rect = self.frame;
    rect.size.height = h;
    rect.size.width = 44.0;
    self.frame = rect;
    
    for (int i = 0; i < [_avaiableBtns count]; i++) {
        UIView *v = _avaiableBtns[i];
        v.autoresizingMask = UIViewAutoresizingNone;
        CGRect f = v.frame;
        f.origin.x = (self.frame.size.width - f.size.width) / 2.0;
        f.origin.y = i * (kButtonSize + 20.0);
        v.frame = f;
        v.hidden = NO;
    }
}

- (void)showAnimated:(BOOL)animated {
    for (UIView *v in _avaiableBtns) {
        v.alpha = 0.0;
        v.transform = CGAffineTransformMakeScale(0.5, 0.5);
    }
    self.hidden = NO;
    
    for (int i = 0; i < [_avaiableBtns count]; i++) {
        UIView *v = _avaiableBtns[i];
        [UIView animateWithDuration:animated ? 0.1 : 0.0 delay:animated ? 0.05 * i : 0.0 options:UIViewAnimationCurveEaseIn animations:^{
            v.transform = CGAffineTransformMakeScale(1.1, 1.1);
            v.alpha = 1.0;
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:animated ? 0.05 : 0.0 animations:^{
                v.transform = CGAffineTransformIdentity;
            }];
        }];
    }
}

- (void)hideAnimated:(BOOL)animated {
    __block NSInteger blockCount = 0;
    for (int i = 0; i < [_avaiableBtns count]; i++) {
        UIView *v = _avaiableBtns[i];
        [UIView animateWithDuration:animated ? 0.1 : 0.0 delay:animated ? 0.05 * i : 0.0 options:UIViewAnimationCurveEaseIn animations:^{
            v.transform = CGAffineTransformMakeScale(0.5, 0.5);
            v.alpha = 0.0;
        } completion:^(BOOL finished) {
            if (++blockCount >= [_avaiableBtns count]) {
                self.hidden = YES;
            }
        }];
    }
}
@end
