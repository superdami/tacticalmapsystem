//
//  TMPaintOverlay.m
//  TacticalMapSystem
//
//  Created by Chen Zhejun on 2014/01/07.
//  Copyright (c) 2014年 chen zhejun. All rights reserved.
//

#import "TMPaintOverlay.h"
#import "TMHelper.h"
#define WORLD_SCALE         4.0

@implementation TMPaintOverlay
{
    MKMapRect _boundingMapRect;
    pthread_rwlock_t _rwLock;
}

- (id)initWithCenterCoordinate:(CLLocationCoordinate2D)coord
{
	self = [super init];
    if (self)
	{
        MKMapPoint origin = MKMapPointForCoordinate(coord);
        origin.x -= MKMapSizeWorld.width / (WORLD_SCALE * 2);
        origin.y -= MKMapSizeWorld.height / (WORLD_SCALE * 2);
        MKMapSize size = MKMapSizeWorld;
        size.width /= WORLD_SCALE;
        size.height /= WORLD_SCALE;
        _boundingMapRect = (MKMapRect) { origin, size };
        MKMapRect worldRect = MKMapRectMake(0, 0, MKMapSizeWorld.width, MKMapSizeWorld.height);
        _boundingMapRect = MKMapRectIntersection(_boundingMapRect, worldRect);
        
        _paths = [NSMutableArray new];
    }
    return self;
}

- (void)dealloc
{
    pthread_rwlock_destroy(&_rwLock);
}

- (MKMapRect)boundingMapRect
{
    return _boundingMapRect;
}

- (CLLocationCoordinate2D)coordinate
{
    if ([_paths count]) {
        NSMutableArray *locations = [_paths objectAtIndex:0];
        CLLocation *firstLocation = [locations objectAtIndex:0];
        return firstLocation.coordinate;
    }
    return CLLocationCoordinate2DMake(0, 0);
}

- (void)lockForReading
{
    pthread_rwlock_rdlock(&_rwLock);
}

- (void)unlockForReading
{
    pthread_rwlock_unlock(&_rwLock);
}

- (void)addPathWithLocation:(CLLocation *)location {
    [self lockForReading];
    NSMutableArray *coordsArray = [NSMutableArray new];
    [coordsArray addObject:location];
    [_paths addObject:coordsArray];
    [self unlockForReading];
}

- (MKMapRect)addLineWithLocation:(CLLocation *)location {
    if ([_paths count]) {
        NSMutableArray *locations = [_paths lastObject];
        int count = [locations count];
        if (count) {
            [self lockForReading];
            [locations addObject:location];
            
            CLLocationCoordinate2D coord1 = [[locations objectAtIndex:count - 1] coordinate];
            CLLocationCoordinate2D coord2 = [[locations objectAtIndex:count] coordinate];
            MKMapPoint point1 = MKMapPointForCoordinate(coord1);
            MKMapPoint point2 = MKMapPointForCoordinate(coord2);
            MKMapRect updateRect = [TMHelper makeRectWithPoint:point1 and:point2];
            [self unlockForReading];
            return updateRect;
        }
    }
    return MKMapRectNull;
}
@end
