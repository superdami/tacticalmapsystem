//
//  TMAnnotation.h
//  TacticalMapSystem
//
//  Created by chen zhejun on 9/21/12.
//  Copyright (c) 2012 chen zhejun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

typedef enum {
    TMAnnotationStatusIdle = 0,
    TMAnnotationStatusNormal = 1,
    TMAnnotationStatusHit = 2,
} TMAnnotationStatus;

typedef enum {
    TMAnnotationTypeNone = 0,
    TMAnnotationTypeUser = 1,    
	TMAnnotationTypeHostile = 2,
    TMAnnotationTypeHostiles,
    TMAnnotationTypeLandmark,
    ControlType,
    
    TMAnnotationTypeActionMove,
} TMAnnotationType;

@interface TMAnnotation : NSObject <MKAnnotation>
@property (nonatomic, assign) CLLocationDirection heading;
@property (nonatomic, strong) NSString *desc;
@property (nonatomic, assign) TMAnnotationType type;
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, assign) NSTimeInterval createdAt;
@property (nonatomic, assign) NSTimeInterval updatedAt;
@property (nonatomic, strong) NSString *uid;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, assign) TMAnnotationStatus status;

- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate;
- (NSDictionary *)dataObject;
@end