//
//  TimeCounterView.m
//  TacticalMapSystem
//
//  Created by chen zhejun on 2015/05/24.
//  Copyright (c) 2015年 chen zhejun. All rights reserved.
//

#import "TimeCounterView.h"

@implementation TimeCounterView {
    UILabel *_label;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.userInteractionEnabled = NO;
        _label = [[UILabel alloc] initWithFrame:self.bounds];
        _label.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _label.backgroundColor = [UIColor clearColor];
        _label.font = [UIFont systemFontOfSize:16.0];
        _label.textAlignment = NSTextAlignmentCenter;
        _label.text = @"--:--";
        [self addSubview:_label];
    }
    return self;
}

- (void)updateTimeSeconds:(NSInteger)number {
    NSInteger min = number / 60;
    NSInteger sec = number % 60;
    NSString *minString = min < 10 ? [NSString stringWithFormat:@"0%ld", min] : [NSString stringWithFormat:@"%ld", min];
    NSString *secString = sec < 10 ? [NSString stringWithFormat:@"0%ld", sec] : [NSString stringWithFormat:@"%ld", sec];
    
    _label.text = [NSString stringWithFormat:@"%@:%@", minString, secString];
}
@end
