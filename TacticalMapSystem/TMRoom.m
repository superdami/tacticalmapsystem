//
//  TMRoom.m
//  TacticalMapSystem
//
//  Created by chen zhejun on 2015/03/14.
//  Copyright (c) 2015年 chen zhejun. All rights reserved.
//

#import "TMRoom.h"

@implementation TMRoom

- (void)setOriginalData:(NSDictionary *)originalData {
    _originalData = originalData;
    _name = [_originalData objectForKey:@"name"];
    _title = [_originalData objectForKey:@"title"];
    _host = [_originalData objectForKey:@"host"];
    _uid = [_originalData objectForKey:@"uid"];
    _createdAt = [[_originalData objectForKey:@"createdAt"] longValue] / 1000;
    _startedAt = [[_originalData objectForKey:@"startedAt"] longValue] / 1000;
    _berfing = [_originalData objectForKey:@"berfing"];
    
    NSDictionary *coord = [_originalData objectForKey:@"coordinate"];
    if ([coord isKindOfClass:[NSDictionary class]]) {
        _coordinate = CLLocationCoordinate2DMake([[coord objectForKey:@"lat"] doubleValue], [[coord objectForKey:@"lon"] doubleValue]);        
    }
}
@end
