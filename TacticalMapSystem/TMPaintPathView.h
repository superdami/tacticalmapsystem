//
//  TMPaintPathView.h
//  TacticalMapSystem
//
//  Created by Chen Zhejun on 2014/01/07.
//  Copyright (c) 2014年 chen zhejun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "TMPaintOverlay.h"
#import "TMPaintRenderer.h"

@class TMPaintPathView;
@protocol TMPaintPathViewDelegate <NSObject>
@required;
- (void)addOverlayToMapView:(NSObject<MKOverlay> *)overlay;
- (MKZoomScale)mapCurrentZoomScale;
- (CLLocationCoordinate2D)convertPoint:(CGPoint)point toCoordinateFromView:(UIView *)view;
@end

@interface TMPaintPathView : UIView
@property (nonatomic, readonly)TMPaintOverlay *paintOverlay;
@property (nonatomic, readonly)TMPaintRenderer *paintRenderer;
@property (nonatomic, weak)id<TMPaintPathViewDelegate> delegate;
@end
