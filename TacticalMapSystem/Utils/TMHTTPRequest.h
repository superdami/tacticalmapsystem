#import <SVHTTPRequest/SVHTTPRequest.h>
#import "SVHTTPRequest.h"
typedef void (^TMAPIClientSuccessBlock)(id result);
typedef void (^TMAPIClientFailureBlock)(NSError *error);

@interface TMHTTPRequest : SVHTTPRequest

@property(nonatomic, strong) SVHTTPClient *client;

+ (TMHTTPRequest *)sharedInstance;

+ (SVHTTPRequest *)GET:(NSArray *)path parameters:(NSDictionary *)parameters requiredClass:(Class)classInstance completion:(TMAPIClientSuccessBlock)completion failure:(TMAPIClientFailureBlock)failure;

+ (SVHTTPRequest*)GET:(NSArray *)path parameters:(NSDictionary *)parameters requiredClass:(Class)classInstance saveToPath:(NSString *)savePath progress:(void (^)(float))progressBlock completion:(TMAPIClientSuccessBlock)completion failure:(TMAPIClientFailureBlock)failure;

+ (SVHTTPRequest *)POST:(NSArray *)path parameters:(NSDictionary *)parameters requiredClass:(Class)classInstance completion:(TMAPIClientSuccessBlock)completion failure:(TMAPIClientFailureBlock)failure;

+ (SVHTTPRequest*)POST:(NSArray *)path parameters:(NSDictionary *)parameters requiredClass:(Class)classInstance progress:(void (^)(float))progressBlock completion:(TMAPIClientSuccessBlock)completion failure:(TMAPIClientFailureBlock)failure;

+ (SVHTTPRequest *)POST:(NSString *)URLStr parameters:(NSDictionary *)parameters innerClient:(BOOL)innerClient progress:(void (^)(float))progressBlock completion:(SVHTTPRequestCompletionHandler)block;

+ (SVHTTPRequest *)GET:(NSString *)URLStr parameters:(NSDictionary *)parameters innerClient:(BOOL)innerClient saveToPath:(NSString *)savePath progress:(void (^)(float))progressBlock completion:(SVHTTPRequestCompletionHandler)block;

+ (id)handleResponse:(id)response urlResponse:(NSHTTPURLResponse *)urlResponse requiredClass:(Class)class error:(NSError *)error failure:(TMAPIClientFailureBlock)failure;

+ (NSString *)apiPathForArray:(NSArray *)components;

+ (void)loginWithToken:(NSString *)token;
+ (void)logout;
@end