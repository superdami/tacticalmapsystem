//
//  TMAnnotationManager.m
//  TacticalMapSystem
//
//  Created by Chen Zhejun on 2014/10/12.
//  Copyright (c) 2014年 chen zhejun. All rights reserved.
//
#import "NSString+MD5.h"
#import "TMAnnotationManager.h"
@interface TMAnnotationManager()
@end

@implementation TMAnnotationManager {
    NSTimeInterval _lastUpdateTime;
}

+ (TMAnnotationManager *)sharedManager {
    static TMAnnotationManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[TMAnnotationManager alloc] init];
    });
    
    return manager;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)close {
    [self dropAnnotation:_userAnnotation];
    [_annotations removeAllObjects];
    _annotations = nil;
    _userAnnotation = nil;
    _mapView = nil;
}

- (void)loadAnnotations {
    _annotations = [NSMutableDictionary new];

    TMAnnotation *userAnnotation = [[TMAnnotation alloc] init];
    userAnnotation.type = TMAnnotationTypeUser;
    userAnnotation.status = TMAnnotationStatusIdle;
    if ([TMUserManager sharedInstance].isLogin) {
        userAnnotation.desc = [TMUserManager sharedInstance].callsign;
        userAnnotation.uid = [TMUserManager sharedInstance].userId;
        userAnnotation.image = [TMUserManager sharedInstance].avatar;
    } else {
        userAnnotation.desc = @"Rockie";
        userAnnotation.uid = [self generateUniqueKey];
    }
    _userAnnotation = userAnnotation;
    [_mapView addAnnotation:userAnnotation];

    if (_offlineMode) return;
    
    [TMAPIClient getAnnotationsCompletion:^(NSArray *result) {
        for (NSDictionary *annotationData in result) {
            if (![userAnnotation.uid isEqualToString:annotationData[@"uid"]]) {
                [self fetchAnnotationWith:annotationData force:YES];
            }
        }
    } failure:nil];
}

- (void)removeAllMarkAnnotations {
    NSMutableArray *removeKeys = [NSMutableArray new];
    NSMutableArray *removeArray = [NSMutableArray new];
    for (TMAnnotation *annotation in [_annotations allValues]) {
        if (annotation.type > TMAnnotationTypeUser) {
            [removeKeys addObject:annotation.uid];
            [removeArray addObject:annotation];
        }
    }
    
    [_annotations removeObjectsForKeys:removeKeys];
    [_mapView removeAnnotations:removeArray];
}

- (void)dealloc {
    [self close];
}

- (void)markAnnotation:(TMAnnotation *)annotation {
    NSString *uid = [self generateUniqueKey];
    annotation.uid = uid;
    [_annotations setObject:annotation forKey:uid];
    [_mapView addAnnotation:annotation];
    
    if (_offlineMode) return;
    [[TMAPIClient sharedInstance] createAnnotation:annotation];
}

- (void)moveAnnotation:(TMAnnotation *)annotation withPoint:(CGPoint)point {
    CLLocationCoordinate2D coordinate = [_mapView convertPoint:point toCoordinateFromView:_mapView];
    MKAnnotationView *annotationView = [_mapView viewForAnnotation:annotation];
    CGRect f = annotationView.frame;
    annotation.coordinate = coordinate;
    annotationView.frame = f;
    [UIView animateWithDuration:0.3 animations:^{
        annotationView.frame = CGRectMake(point.x, point.y, annotationView.frame.size.width, annotationView.frame.size.height);
    }];
    
    if (_offlineMode) return;
    [[TMAPIClient sharedInstance] updateAnnotation:annotation];
}

- (void)dropAnnotation:(TMAnnotation *)annotation {
    [_annotations removeObjectForKey:annotation.uid];
    [_mapView removeAnnotation:annotation];
    
    if (_offlineMode) return;
    [[TMAPIClient sharedInstance] removeAnnotation:annotation];
}

- (void)dropAllAnnotations {
    [self removeAllMarkAnnotations];
    
    if (_offlineMode) return;
    [[TMAPIClient sharedInstance] removeAllMarkAnnotations];
}

- (void)updateUserStatusForce:(BOOL)force {
    if (_offlineMode) return;
    
    if (force) {
        [[TMAPIClient sharedInstance] updateUserAnnotation:_userAnnotation];
    } else {
        NSTimeInterval now = [[NSDate date] timeIntervalSince1970];
        if (now - _lastUpdateTime >= 10) {
            [[TMAPIClient sharedInstance] updateUserAnnotation:_userAnnotation];
            _lastUpdateTime = now;
        }
    }
}

- (void)updateAnnotation:(TMAnnotation *)annotation {
    if (_offlineMode) return;
    [[TMAPIClient sharedInstance] updateUserAnnotation:annotation];
}

- (void)fetchAnnotationWith:(NSDictionary *)data force:(BOOL)force {
    NSString *uid = data[@"uid"];
    NSDictionary *coordData = data[@"coordinate"];
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([[coordData objectForKey:@"lat"] doubleValue], [[coordData objectForKey:@"lon"] doubleValue]);
    TMAnnotation *annotation = [_annotations objectForKey:uid];
    if (annotation) {
        if ([data[@"updatedAt"] doubleValue] > annotation.updatedAt) {
            annotation.coordinate = coordinate;
            annotation.updatedAt = [data[@"updatedAt"] doubleValue];
            annotation.heading = [data[@"heading"] integerValue];
            annotation.desc = data[@"desc"];
            annotation.status = [data[@"status"] intValue];
            annotation.image = data[@"image"];
            
            MKAnnotationView *view = [_mapView viewForAnnotation:annotation];
            if ([view isKindOfClass:[TMAnnotationView class]]) {
                [(TMAnnotationView *)view refresh];
            }
        } else {
            NSLog(@"received data is outdate");
        }
    } else if (force) {
        annotation = [[TMAnnotation alloc] initWithCoordinate:coordinate];
        annotation.heading = [data[@"heading"] integerValue];
        annotation.desc = data[@"desc"];
        annotation.type = [data[@"type"] intValue];
        annotation.updatedAt = [data[@"updatedAt"] doubleValue];
        annotation.createdAt = [data[@"createdAt"] doubleValue];
        annotation.uid = uid;
        annotation.status = [data[@"status"] intValue];
        annotation.image = data[@"image"];
        
        [_annotations setObject:annotation forKey:uid];
        if (_mapView) {
            [_mapView addAnnotation:annotation];
        }
    }
}

- (void)removeAnnotationsWith:(NSDictionary *)data {
    NSString *uid = [data objectForKey:@"uid"];
    TMAnnotation *annotation = [_annotations objectForKey:uid];
    if (annotation) {
        [_annotations removeObjectForKey:uid];
        [_mapView removeAnnotation:annotation];
    }
}

- (NSString *)generateUniqueKey {
    NSTimeInterval timestamp = [[NSDate date] timeIntervalSince1970];
    NSString *string = [[TMUserManager sharedInstance].uuid stringByAppendingString:[NSString stringWithFormat:@"%f", timestamp]];
    return [string md5];
}

- (UIImage *)imageWithAnnotation:(id<MKAnnotation>)annotation {
    if ([annotation isKindOfClass:[TMAnnotation class]]) {
        TMAnnotation *anno = (TMAnnotation *)annotation;
        if (anno.type == TMAnnotationTypeUser) {
            if (anno == _userAnnotation) {
                return [UIImage imageNamed:@"map_user_annot"];
            } else if (anno.status == TMAnnotationStatusIdle) {
                return nil;
            } else if (anno.status == TMAnnotationStatusHit) {
                return [UIImage imageNamed:@"map_men_hitted_annot"];
            } else {
                return [UIImage imageNamed:@"map_mate_annot"];
            }
        } else {
            return [TMAnnotationManager imageWithAnnotationType:anno.type];
        }
    } else if (annotation == _mapView.userLocation) {
        return [UIImage imageNamed:@"map_user_annot"];
    }
    return nil;
}

+ (UIImage *)imageWithAnnotationType:(TMAnnotationType)type {
    if (type == TMAnnotationTypeHostile) {
        return [UIImage imageNamed:@"map_single_target_annot"];
    } else if (type == TMAnnotationTypeHostiles) {
        return [UIImage imageNamed:@"map_muti_target_annot"];
    } else if (type == TMAnnotationTypeLandmark) {
        return [UIImage imageNamed:@""];
    } else if (type == TMAnnotationTypeActionMove) {
        return [UIImage imageNamed:@"pop_menu_movehere"];
    }
    return nil;
}

+ (NSString *)descriptionWithAnnotationType:(TMAnnotationType)type {
    if (type == TMAnnotationTypeUser) {
        return @"User";
    } else if (type == TMAnnotationTypeHostile) {
        return @"Singal Hostile";
    } else if (type == TMAnnotationTypeHostiles) {
        return @"Multi Hostile";
    } else if (type == TMAnnotationTypeLandmark) {
        return @"Landmark";
    } else if (type == TMAnnotationTypeActionMove) {
        return @"Move Here";
    }
    return @"";
}

@end
