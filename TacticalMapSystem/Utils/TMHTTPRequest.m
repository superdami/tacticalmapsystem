#import "NSString+MD5.h"

#import "TMHTTPRequest.h"

@interface SVHTTPRequest (PrivateMethods)
- (SVHTTPRequest*)initWithAddress:(NSString*)urlString
                           method:(SVHTTPRequestMethod)method
                       parameters:(NSObject*)parameters
                       saveToPath:(NSString*)savePath
                         progress:(void (^)(float))progressBlock
                       completion:(SVHTTPRequestCompletionHandler)completionBlock;

@end

@interface SVHTTPClient (PrivateMethods)
@property (nonatomic, strong) NSMutableDictionary *HTTPHeaderFields;
@property (nonatomic, strong) NSOperationQueue *operationQueue;
@end

static TMHTTPRequest *__sharedInstance = nil;

@implementation TMHTTPRequest

- (instancetype)initWithInnerClient {
    self = [super init];
    if (self) {
        _client = [SVHTTPClient sharedClientWithIdentifier:@"TMAPIClient"];
        _client.userAgent = [[NSUserDefaults standardUserDefaults] valueForKey:@"UserAgent"];
    }
    return self;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _client = [SVHTTPClient sharedClient];
        _client.userAgent = [[NSUserDefaults standardUserDefaults] valueForKey:@"UserAgent"];
    }
    return self;
}

+ (TMHTTPRequest *)sharedInstance {
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        __sharedInstance = [[TMHTTPRequest alloc] initWithInnerClient];
    });
    return __sharedInstance;
}

+ (NSString *)apiPathForArray:(NSArray *)components {
    NSMutableString *path = [NSMutableString stringWithString:kAPIBasicURL];
    for (NSUInteger i = 0; i < components.count; i++) {
        NSString *component = components[i];
        if (component.length == 0) continue;    // Skip empty string
        
        if ([path rangeOfString:@"/"].location != (path.length - 1) &&
            [component rangeOfString:@"/"].location != 0) {
            [path appendString:@"/"];
        }
        [path appendString:[component stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    }
    return [kBaseURL stringByAppendingString:path];
}

+ (void)loginWithToken:(NSString *)token {
    [[TMHTTPRequest sharedInstance].client setValue:token forHTTPHeaderField:kTokenKey];
}

+ (void)logout {
    [[TMHTTPRequest sharedInstance].client setValue:@"" forHTTPHeaderField:kTokenKey];
}

+ (SVHTTPRequest *)GET:(NSArray *)path parameters:(NSDictionary *)parameters requiredClass:(Class)classInstance completion:(TMAPIClientSuccessBlock)completion failure:(TMAPIClientFailureBlock)failure {
    return [self GET:path parameters:parameters requiredClass:classInstance saveToPath:nil progress:nil completion:completion failure:failure];
}

+ (SVHTTPRequest*)GET:(NSArray *)path parameters:(NSDictionary *)parameters requiredClass:(Class)classInstance saveToPath:(NSString *)savePath progress:(void (^)(float))progressBlock completion:(TMAPIClientSuccessBlock)completion failure:(TMAPIClientFailureBlock)failure {
    NSString *pathStr = [TMHTTPRequest apiPathForArray:path];
    return [self GET:pathStr parameters:parameters innerClient:YES saveToPath:savePath progress:progressBlock completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error){
        id result = [self handleResponse:response urlResponse:urlResponse requiredClass:classInstance error:error failure:failure];
        if (result && completion) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(result);
            });
        }
    }];
}

+ (SVHTTPRequest *)POST:(NSArray *)path parameters:(NSDictionary *)parameters requiredClass:(Class)classInstance completion:(TMAPIClientSuccessBlock)completion failure:(TMAPIClientFailureBlock)failure {
    return [self POST:path parameters:parameters requiredClass:classInstance progress:nil completion:completion failure:failure];
}

+ (SVHTTPRequest*)POST:(NSArray *)path parameters:(NSDictionary *)parameters requiredClass:(Class)classInstance progress:(void (^)(float))progressBlock completion:(TMAPIClientSuccessBlock)completion failure:(TMAPIClientFailureBlock)failure; {
    NSString *pathStr = [TMHTTPRequest apiPathForArray:path];
    return [self POST:pathStr parameters:parameters innerClient:YES progress:progressBlock completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error){
        id result = [self handleResponse:response urlResponse:urlResponse requiredClass:classInstance error:error failure:failure];
        if (completion && result) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(result);
            });
        }
    }];
}

+ (SVHTTPRequest *)POST:(NSString *)URLStr parameters:(NSDictionary *)parameters innerClient:(BOOL)innerClient progress:(void (^)(float))progressBlock completion:(SVHTTPRequestCompletionHandler)block {
    SVHTTPClient *client = innerClient ? [self sharedInstance].client : ([[TMHTTPRequest alloc] init].client);
    LOG_MESSAGE(@"POST: %@", URLStr);
    return [client POST:URLStr parameters:parameters progress:progressBlock completion:block];
}

+ (SVHTTPRequest *)GET:(NSString *)URLStr parameters:(NSDictionary *)parameters innerClient:(BOOL)innerClient saveToPath:(NSString *)savePath progress:(void (^)(float))progressBlock completion:(SVHTTPRequestCompletionHandler)block {
    SVHTTPClient *client = innerClient ? [self sharedInstance].client : ([[TMHTTPRequest alloc] init].client);
    LOG_MESSAGE(@"GET: %@", URLStr);
    return [client GET:URLStr parameters:parameters saveToPath:savePath progress:progressBlock completion:block];
}

+ (id)processResponse:(NSHTTPURLResponse *)urlResponse response:(id)response requiredClass:(Class)class failure:(TMAPIClientFailureBlock)failure {
    NSError *error = nil;
    void (^failureBlock)(NSError *err) = ^(NSError *err) {
        if (failure) {
            dispatch_async(dispatch_get_main_queue(), ^{
                failure(err);
            });
        }
    };
    if (urlResponse.statusCode == 204) {
        return @"";
    }
    else if ([response isKindOfClass:[NSData class]]) {
        id result = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];
        if (error) {
            failureBlock(error);
            return nil;
        }
        return [TMHTTPRequest processResponse:urlResponse response:result requiredClass:class failure:failure];
    } else if ([response isKindOfClass:class]) {
        if (urlResponse.statusCode != 200) {
            NSMutableDictionary *userInfo = [@{NSLocalizedDescriptionKey : @"Bad Server Response"} mutableCopy];
            if ([response isKindOfClass:[NSDictionary class]] && response[@"message"]) {
                userInfo[NSLocalizedDescriptionKey] = response[@"message"];
                userInfo[@"server-message"] = @(YES);
            }
            NSError *error = [NSError errorWithDomain:kBaseHostName code:urlResponse.statusCode userInfo:userInfo];
            failureBlock(error);
            return nil;
        }
        return response;
    } else {
        // API Server Error
        error = [NSError errorWithDomain:@"TMCient" code:kAPIServerErrorCode userInfo:nil];
        failureBlock(error);
        return nil;
    }
}

+ (id)handleResponse:(id)response urlResponse:(NSHTTPURLResponse *)urlResponse requiredClass:(Class)class error:(NSError *)error failure:(TMAPIClientFailureBlock)failure {
    if (error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (failure) {
                failure(error);
            }
        });
        return nil;
    }
    id result = [self processResponse:urlResponse response:response requiredClass:class failure:failure];
    return result;
}
@end