//
//  TMGameManager.m
//  TacticalMapSystem
//
//  Created by chen zhejun on 2015/05/24.
//  Copyright (c) 2015年 chen zhejun. All rights reserved.
//

#import "TMGameManager.h"

@implementation TMGameManager {
    NSTimer *_gameTimer;
    NSMutableArray *_delegateArray;
}

+ (TMGameManager *)sharedManager {
    static TMGameManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[TMGameManager alloc] init];
    });
    
    return manager;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)dealloc {
    if ([_gameTimer isValid]) {
        [_gameTimer invalidate];
    }
    [_delegateArray removeAllObjects];
}

- (void)resetGameStatus {
    if (_room.startedAt > 0) {
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:_room.startedAt];
        NSTimeInterval now = [[NSDate date] timeIntervalSince1970];
        _gameTimeTreval = now - [date timeIntervalSince1970];
        if (![_gameTimer isValid]) {
            _gameTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timeTravelUpdate:) userInfo:nil repeats:YES];
        }
        for (id<TMGameManagerDelegate> delegate in _delegateArray) {
            if ([delegate respondsToSelector:@selector(gameStatusDidChange:timeTravel:)]) {
                [delegate gameStatusDidChange:YES timeTravel:_gameTimeTreval];
            }
        }
    } else {
        if ([_gameTimer isValid]) {
            [_gameTimer invalidate];
        }
        
        _gameTimeTreval = 0;
        for (id<TMGameManagerDelegate> delegate in _delegateArray) {
            if ([delegate respondsToSelector:@selector(gameStatusDidChange:timeTravel:)]) {
                [delegate gameStatusDidChange:NO timeTravel:_gameTimeTreval];
            }
        }
    }
}

- (void)close {
    if ([_gameTimer isValid]) {
        [_gameTimer invalidate];
    }
    _gameTimeTreval = 0;
}

- (void)timeTravelUpdate:(NSTimer *)sender {
    ++_gameTimeTreval;
    for (id<TMGameManagerDelegate> delegate in _delegateArray) {
        if ([delegate respondsToSelector:@selector(gameTimeTravelUpdate:)]) {
            [delegate gameTimeTravelUpdate:_gameTimeTreval];
        }
    }
}

- (void)addDelegate:(id<TMGameManagerDelegate>) delegate {
    if (!_delegateArray) {
        _delegateArray = [NSMutableArray new];
    }
    
    if (![_delegateArray containsObject:delegate]) {
        [_delegateArray addObject:delegate];
    }
}

- (void)removeDelegate:(id<TMGameManagerDelegate>) delegate {
    if ([_delegateArray containsObject:delegate]) {
        [_delegateArray removeObject:delegate];
    }
}

- (BOOL)gameStarted {
    return _room.startedAt ? YES : NO;
}

- (void)setGameStarted:(BOOL)started {
    if (started) {
        [[TMAPIClient sharedInstance] sendToStartGame];
    } else {
        [[TMAPIClient sharedInstance] sendToStopGame];
    }
}
@end
