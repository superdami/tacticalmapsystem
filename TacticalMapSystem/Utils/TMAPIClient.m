#import "TMAPIClient.h"

@interface SVHTTPRequest (PrivateMethods)
- (SVHTTPRequest*)initWithAddress:(NSString*)urlString
                           method:(SVHTTPRequestMethod)method
                       parameters:(NSObject*)parameters
                       saveToPath:(NSString*)savePath
                         progress:(void (^)(float))progressBlock
                       completion:(SVHTTPRequestCompletionHandler)completionBlock;
@end

@implementation TMAPIClient

+ (void)signup:(NSDictionary *)params completion:(TMAPIClientSuccessBlock)completion failure:(TMAPIClientFailureBlock)failure {
    [TMHTTPRequest POST:@[@"users", @"signup"] parameters:params requiredClass:[NSDictionary class] completion:^(id result) {
        if (completion) {
            completion(result);
        }
    } failure:failure];
}

+ (void)login:(NSDictionary *)params completion:(TMAPIClientSuccessBlock)completion failure:(TMAPIClientFailureBlock)failure {
    [TMHTTPRequest GET:@[@"users", @"login"] parameters:params requiredClass:[NSDictionary class] completion:^(id result) {
        if (completion) {
            completion(result);
        }
    } failure:failure];
}

+ (void)authToken:(NSString *)token completion:(TMAPIClientSuccessBlock)completion failure:(TMAPIClientFailureBlock)failure {
    [TMHTTPRequest GET:@[@"users", @"token-auth"] parameters:@{@"access_token": token} requiredClass:[NSDictionary class] completion:^(id result) {
        if (completion) {
            completion(result);
        }
    } failure:failure];
}

+ (void)facebookAuthToken:(NSString *)token completion:(TMAPIClientSuccessBlock)completion failure:(TMAPIClientFailureBlock)failure {
    [TMHTTPRequest POST:@[@"api", @"facebook", @"auth"] parameters:@{@"access_token": token} requiredClass:[NSDictionary class] completion:^(id result) {
        if (completion) {
            completion(result);
        }
    } failure:failure];
}

+ (void)checkRoomCompletion:(TMAPIClientSuccessBlock)completion failure:(TMAPIClientFailureBlock)failure {
    [TMHTTPRequest GET:@[@"users", @"check-room"] parameters:nil requiredClass:[NSDictionary class] completion:^(id result) {
        if (completion) {
            completion(result);
        }
    } failure:failure];
}

+ (void)requestRoomList:(NSDictionary *)params completion:(TMAPIClientSuccessBlock)completion failure:(TMAPIClientFailureBlock)failure {
    [TMHTTPRequest GET:@[@"api", @"rooms", @"list"] parameters:params requiredClass:[NSArray class] completion:^(id result) {
        if (completion) {
            completion(result);
        }
    } failure:failure];
}

+ (void)requestCreateRoom:(NSDictionary *)params completion:(TMAPIClientSuccessBlock)completion failure:(TMAPIClientFailureBlock)failure {
    [TMHTTPRequest POST:@[@"create-room"] parameters:params requiredClass:[NSDictionary class] completion:^(id result) {
        if (completion) {
            completion(result);
        }
    } failure:failure];
}

+ (void)requestJoinRoom:(NSDictionary *)params completion:(TMAPIClientSuccessBlock)completion failure:(TMAPIClientFailureBlock)failure {
    [TMHTTPRequest GET:@[@"join-room"] parameters:params requiredClass:[NSDictionary class] completion:^(id result) {
        if (completion) {
            completion(result);
        }
    } failure:failure];
}

+ (void)requestLeaveRoomCompletion:(TMAPIClientSuccessBlock)completion {
    NSString *pathStr = [TMHTTPRequest apiPathForArray:@[@"in-room", @"leave-room"]];
    [TMHTTPRequest GET:pathStr parameters:nil innerClient:YES saveToPath:nil progress:nil completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        if (completion) {
            completion(nil);
        }
    }];
}

+ (void)getAnnotationsCompletion:(TMAPIClientSuccessBlock)completion failure:(TMAPIClientFailureBlock)failure {
    [TMHTTPRequest GET:@[@"in-room", @"annotations"] parameters:nil requiredClass:[NSArray class] completion:completion failure:failure];
}

+ (void)uploadAvatarImageData:(NSData *)imageData Completion:(TMAPIClientSuccessBlock)completion failure:(TMAPIClientFailureBlock)failure {
    [TMHTTPRequest POST:@[@"users", @"upload", @"avatar"] parameters:@{@"file": imageData} requiredClass:[NSDictionary class] completion:^(id result) {
        if (completion) {
            completion(result);
        }
    } failure:failure];
}
@end