//
//  TMGameManager.h
//  TacticalMapSystem
//
//  Created by chen zhejun on 2015/05/24.
//  Copyright (c) 2015年 chen zhejun. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol TMGameManagerDelegate <NSObject>
@optional
- (void)gameStatusDidChange:(BOOL)started timeTravel:(NSTimeInterval)time;
- (void)gameTimeTravelUpdate:(NSTimeInterval)time;
@end

@interface TMGameManager : NSObject
@property (nonatomic, strong) TMRoom *room;
@property (nonatomic, assign) NSTimeInterval gameTimeTreval;
@property (nonatomic, readonly) BOOL gameStarted;

+ (TMGameManager *)sharedManager;
- (void)resetGameStatus;
- (void)addDelegate:(id<TMGameManagerDelegate>) delegate;
- (void)removeDelegate:(id<TMGameManagerDelegate>) delegate;
- (void)setGameStarted:(BOOL)started;
- (void)close;
@end
