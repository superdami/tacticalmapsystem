#import "TMHTTPRequest.h"

@class SocketIO;
@interface TMAPIClient : NSObject
@property (nonatomic, strong)SocketIO *socketIO;
@property (nonatomic, strong)void (^connectedBlock)(NSDictionary *info);

+ (void)signup:(NSDictionary *)params completion:(TMAPIClientSuccessBlock)completion failure:(TMAPIClientFailureBlock)failure;

+ (void)login:(NSDictionary *)params completion:(TMAPIClientSuccessBlock)completion failure:(TMAPIClientFailureBlock)failure;

+ (void)authToken:(NSString *)token completion:(TMAPIClientSuccessBlock)completion failure:(TMAPIClientFailureBlock)failure;

+ (void)facebookAuthToken:(NSString *)token completion:(TMAPIClientSuccessBlock)completion failure:(TMAPIClientFailureBlock)failure;

+ (void)checkRoomCompletion:(TMAPIClientSuccessBlock)completion failure:(TMAPIClientFailureBlock)failure;

+ (void)requestRoomList:(NSDictionary *)params completion:(TMAPIClientSuccessBlock)completion failure:(TMAPIClientFailureBlock)failure;

+ (void)requestLeaveRoomCompletion:(TMAPIClientSuccessBlock)completion;

+ (void)getAnnotationsCompletion:(TMAPIClientSuccessBlock)completion failure:(TMAPIClientFailureBlock)failure;

+ (void)uploadAvatarImageData:(NSData *)imageData Completion:(TMAPIClientSuccessBlock)completion failure:(TMAPIClientFailureBlock)failure;
@end
