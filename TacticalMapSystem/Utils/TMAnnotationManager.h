//
//  TMAnnotationManager.h
//  TacticalMapSystem
//
//  Created by Chen Zhejun on 2014/10/12.
//  Copyright (c) 2014年 chen zhejun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "TMAnnotationView.h"
#import "TMRouteOverlay.h"
#import "TMAnnotationBar.h"

@interface TMAnnotationManager : NSObject {
}
@property (nonatomic, readonly) NSMutableDictionary *annotations;
@property (nonatomic, weak) MKMapView *mapView;
@property (nonatomic, weak) TMAnnotation *userAnnotation;
@property (nonatomic, assign) BOOL offlineMode;

+ (TMAnnotationManager *)sharedManager;
- (void)loadAnnotations;
- (void)close;

- (void)markAnnotation:(TMAnnotation *)annotation;
- (void)moveAnnotation:(TMAnnotation *)annotation withPoint:(CGPoint)point;
- (void)dropAnnotation:(TMAnnotation *)annotation;
- (void)dropAllAnnotations;
- (void)updateUserStatusForce:(BOOL)force;
- (void)updateAnnotation:(TMAnnotation *)annotation;
- (void)fetchAnnotationWith:(NSDictionary *)data force:(BOOL)force;
- (void)removeAnnotationsWith:(NSDictionary *)data;
- (NSString *)generateUniqueKey;

- (UIImage *)imageWithAnnotation:(id<MKAnnotation>)annotation;
+ (UIImage *)imageWithAnnotationType:(TMAnnotationType)type;
+ (NSString *)descriptionWithAnnotationType:(TMAnnotationType)type;
@end
