//
//  TMCompassAnnotationView.h
//  TacticalMapSystem
//
//  Created by chen zhejun on 2015/05/10.
//  Copyright (c) 2015年 chen zhejun. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface TMCompassAnnotationView : MKAnnotationView
@property (nonatomic, assign)CGFloat heading;
@end
