//
//  TMCompassAnnotation.h
//  TacticalMapSystem
//
//  Created by chen zhejun on 2015/05/10.
//  Copyright (c) 2015年 chen zhejun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMCompassAnnotation : NSObject<MKAnnotation>
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;

@end
