//
//  TMAPIClient+SocketIO.h
//  TacticalMapSystem
//
//  Created by chen zhejun on 2015/03/24.
//  Copyright (c) 2015年 chen zhejun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMAPIClient.h"
#import "SocketIO.h"
#import "SocketIOPacket.h"
#import "SocketIOJSONSerialization.h"

@interface TMAPIClient (SocketIO) <SocketIODelegate>

+ (TMAPIClient *)sharedInstance;

- (void)createRoomWith:(CLLocationCoordinate2D)coordinate name:(NSString *)roomName title:(NSString *)title password:(NSString *)password completion:(void (^)(NSDictionary *))completion failure:(void (^)())failureBlock;

- (void)joinRoomId:(NSString *)roomId password:(NSString *)password completion:(void (^)(NSDictionary *))completion failure:(void (^)())failureBlock;

- (void)leaveRoom;

- (void)updateUserAnnotation:(TMAnnotation *)annotation;

- (void)createAnnotation:(TMAnnotation *)annotation;

- (void)updateAnnotation:(TMAnnotation *)annotation;

- (void)removeAnnotation:(TMAnnotation *)annotation;

- (void)removeAllMarkAnnotations;

- (void)socketJoinRoomId:(NSString *)roomId completion:(void (^)(NSDictionary *))completion;

- (void)sendToStartGame;

- (void)sendToStopGame;
@end
