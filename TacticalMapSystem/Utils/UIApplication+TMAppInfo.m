//
//  UIApplication(TMAppInfo)
//
//  Created by Chen Zhejun on 2014/10/12.
//  Copyright (c) 2014年 chen zhejun. All rights reserved.
//

#import "UIApplication+TMAppInfo.h"

@implementation UIApplication (TMAppInfo)

+ (NSString *)appVersion {
    static NSString *appVersion = nil;
    if (!appVersion) {
        appVersion = [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"];
    }
    return appVersion;
}

+ (NSString *)appBuildVersion {
    static NSString *appBuildVersion = nil;
    if (!appBuildVersion) {
        appBuildVersion = [[NSBundle mainBundle] infoDictionary][(NSString *) kCFBundleVersionKey];
    }
    return appBuildVersion;
}

+ (NSString *)appName {
    static NSString *appName = nil;
    if (!appName) {
        appName = [[NSBundle mainBundle] localizedInfoDictionary][@"CFBundleDisplayName"];
        if (!appName) {
            appName = [[NSBundle mainBundle] infoDictionary][@"CFBundleDisplayName"];
        }
    }
    return appName;
}

+ (NSString *)appBundleId {
    static NSString *appBundleId = nil;
    if (!appBundleId) {
        appBundleId = [[NSBundle mainBundle] infoDictionary][(NSString *)kCFBundleIdentifierKey];
    }
    return appBundleId;
}

+ (NSString *)appBundleName {
    static NSString *appBundleName = nil;
    if (!appBundleName) {
        appBundleName = [[NSBundle mainBundle] infoDictionary][(NSString *)kCFBundleNameKey];
    }
    return appBundleName;
}
@end