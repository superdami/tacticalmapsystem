//
//  UIImage+Resize.h
//  TacticalMapSystem
//
//  Created by chen zhejun on 2015/05/28.
//  Copyright (c) 2015年 chen zhejun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIImage (Resize)
+ (UIImage*)imageWithImage:(UIImage*)sourceImage scaled:(CGFloat)scale;
@end
