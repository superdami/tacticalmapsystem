//
//  UIApplication(TMAppInfo)
//
//  Created by Chen Zhejun on 2014/10/12.
//  Copyright (c) 2014年 chen zhejun. All rights reserved.
//

@interface UIApplication (TMAppInfo)

+ (NSString *)appVersion;
+ (NSString *)appBuildVersion;
+ (NSString *)appName;
+ (NSString *)appBundleId;
+ (NSString *)appBundleName;
@end