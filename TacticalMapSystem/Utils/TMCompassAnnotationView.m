//
//  TMCompassAnnotationView.m
//  TacticalMapSystem
//
//  Created by chen zhejun on 2015/05/10.
//  Copyright (c) 2015年 chen zhejun. All rights reserved.
//

#import "TMCompassAnnotationView.h"

@implementation TMCompassAnnotationView {
    UIImageView *_compaseImage;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self) {
        self.canShowCallout = NO;
        self.userInteractionEnabled = NO;
        self.draggable = NO;
        self.frame = CGRectMake(0.0, 0.0, 270, 270);
        CGPoint center = CGPointMake(ceilf(self.bounds.size.width / 2.0), ceilf(self.bounds.size.height / 2.0));
        
        CGFloat redius = self.bounds.size.width / 2.0;
        CGFloat startAngle = -M_PI + M_PI_4;
        CGFloat endAngle = startAngle + M_PI_2;
        
        UIBezierPath *path = [[UIBezierPath alloc] init];
        [path addArcWithCenter:center radius:redius startAngle:startAngle endAngle:endAngle clockwise:YES];
        [path addLineToPoint:center];
        [path closePath];
        
        CAShapeLayer *pieShape = [CAShapeLayer layer];
        pieShape.path = [path CGPath];
        self.layer.mask = pieShape;
        
        _compaseImage = [[UIImageView alloc] initWithFrame:self.bounds];
        _compaseImage.image = [UIImage imageNamed:@"compase"];
        [self addSubview:_compaseImage];
    }
    return self;
}

- (void)setHeading:(CGFloat)heading {
    _heading = heading;
    
    dispatch_async( dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.4 animations:^{
            _compaseImage.transform = CGAffineTransformMakeRotation(_heading);
        }];
    });
}
@end
