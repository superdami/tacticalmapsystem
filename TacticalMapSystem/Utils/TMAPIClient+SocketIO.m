//
//  TMAPIClient+SocketIO.m
//  TacticalMapSystem
//
//  Created by chen zhejun on 2015/03/24.
//  Copyright (c) 2015年 chen zhejun. All rights reserved.
//

#import "TMAPIClient+SocketIO.h"
#import "TMAnnotationManager.h"
#import "TMGameManager.h"

@interface TMAPIClient (PrivateMethods)
+ (void)requestCreateRoom:(NSDictionary *)params completion:(TMAPIClientSuccessBlock)completion failure:(TMAPIClientFailureBlock)failure;

+ (void)requestJoinRoom:(NSDictionary *)params completion:(TMAPIClientSuccessBlock)completion failure:(TMAPIClientFailureBlock)failure;
@end

@implementation TMAPIClient (SocketIO)

+ (TMAPIClient *)sharedInstance {
    static TMAPIClient *client;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        client = [[TMAPIClient alloc] init];
    });
    
    return client;
}

- (void)createRoomWith:(CLLocationCoordinate2D)coordinate name:(NSString *)roomName title:(NSString *)title password:(NSString *)password completion:(void (^)(NSDictionary *))completion failure:(void (^)())failureBlock {
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:@{@"lat": @(coordinate.latitude), @"lon": @(coordinate.longitude)}
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    NSString *data = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSDictionary *params = @{@"roomName": roomName, @"roomTitle":title, @"roomPassword": password,
                             @"coordinate": data};
    
    [TMAPIClient requestCreateRoom:params completion:^(id result) {
        TMRoom *room = [[TMRoom alloc] init];
        room.originalData = result;        
        [self socketJoinRoomId:room.uid completion:completion];
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock();
        }
    }];
}

- (void)joinRoomId:(NSString *)roomId password:(NSString *)password completion:(void (^)(NSDictionary *))completion failure:(void (^)())failureBlock {
    [TMAPIClient requestJoinRoom:@{@"uid": roomId, @"roomPassword": password} completion:^(id result) {
        [self socketJoinRoomId:roomId completion:completion];
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock();
        }
    }];
}

- (void)leaveRoom {
    [self.socketIO disconnect];
    [TMAPIClient requestLeaveRoomCompletion:nil];
}

- (void)socketJoinRoomId:(NSString *)roomId completion:(void (^)(NSDictionary *))completion {
    self.connectedBlock = completion;
    self.socketIO = [[SocketIO alloc] initWithDelegate:self];
    NSString *token = [TMUserManager sharedInstance].token;
    [self.socketIO connectToHost:kBaseHostName onPort:[kBaseHostPort integerValue] withParams:@{kTokenKey: token}];
}

- (void)createAnnotation:(TMAnnotation *)annotation {
    NSString *dataString = [SocketIOJSONSerialization JSONStringFromObject:annotation.dataObject error:nil];
    __weak typeof(self) weakMe = self;
    [self.socketIO sendEvent:@"annotation:create" withData:dataString andAcknowledge:^(id argsData) {
        [weakMe handleResponse:argsData withAnnotation:annotation];
    }];
}

- (void)updateAnnotation:(TMAnnotation *)annotation {
    NSString *dataString = [SocketIOJSONSerialization JSONStringFromObject:annotation.dataObject error:nil];
    __weak typeof(self) weakMe = self;
    [self.socketIO sendEvent:@"annotation:update" withData:dataString andAcknowledge:^(id argsData) {
        [weakMe handleResponse:argsData withAnnotation:annotation];
    }];
}

- (void)updateUserAnnotation:(TMAnnotation *)annotation {
    NSString *dataString = [SocketIOJSONSerialization JSONStringFromObject:annotation.dataObject error:nil];
    __weak typeof(self) weakMe = self;
    [self.socketIO sendEvent:@"activity:update" withData:dataString andAcknowledge:^(id argsData) {
        [weakMe handleResponse:argsData withAnnotation:annotation];
    }];
}

- (void)handleResponse:(id)argsData withAnnotation:(TMAnnotation *)annotation {
    if ([argsData isKindOfClass:[NSDictionary class]]) {
        annotation.createdAt = [argsData[@"createdAt"] doubleValue];
        annotation.updatedAt = [argsData[@"updatedAt"] doubleValue];
    }
}

- (void)removeAnnotation:(TMAnnotation *)annotation {
    if ([annotation.dataObject isKindOfClass:[NSDictionary class]]) {
        NSString *dataString = [SocketIOJSONSerialization JSONStringFromObject:annotation.dataObject error:nil];
        [self.socketIO sendEvent:@"annotation:remove" withData:dataString];        
    }
}

- (void)removeAllMarkAnnotations {
    [self.socketIO sendEvent:@"annotation:removeAll" withData:nil];
}


- (void)sendToStartGame {
    [self.socketIO sendEvent:@"room:start" withData:nil];
}

- (void)sendToStopGame {
    [self.socketIO sendEvent:@"room:stop" withData:nil];
}

#pragma mark - SocketIO Delegate
- (void)socketIODidConnect:(SocketIO *)socket {
    
}

- (void)socketIODidDisconnect:(SocketIO *)socket disconnectedWithError:(NSError *)error {
    LOG_MESSAGE(@"socket did disconnect error %@", error);
    [[TMUserManager sharedInstance] exitMapViewController];
}

- (void)socketIO:(SocketIO *)socket didReceiveMessage:(SocketIOPacket *)packet {
    LOG_MESSAGE(@"socket did receive msg %@", packet.name);
}

- (void)socketIO:(SocketIO *)socket didReceiveJSON:(SocketIOPacket *)packet {
    LOG_MESSAGE(@"socket did receive json %@", packet.name);
}

- (void)socketIO:(SocketIO *)socket didReceiveEvent:(SocketIOPacket *)packet {
    LOG_MESSAGE(@"socket did receive event %@", packet.name);
    NSDictionary *result;
    NSObject *obj = [packet.args lastObject];
    if ([obj isKindOfClass:[NSDictionary class]]) {
        result = [packet.args lastObject];
    } else if ([obj isKindOfClass:[NSString class]]) {
        NSData *data = [[packet.args lastObject] dataUsingEncoding:NSUTF8StringEncoding];
        result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    }
    if ([packet.name isEqualToString:@"activity:update"]) {
        [[TMAnnotationManager sharedManager] fetchAnnotationWith:result force:YES];
    } else if ([packet.name containsString:@"annotation:"]) {
        if ([packet.name containsString:@"create"]) {
            [[TMAnnotationManager sharedManager] fetchAnnotationWith:result force:YES];
        } else if ([packet.name containsString:@"update"]) {
            [[TMAnnotationManager sharedManager] fetchAnnotationWith:result force:NO];
        } else if ([packet.name containsString:@"remove"]) {
            [[TMAnnotationManager sharedManager] removeAnnotationsWith:result];
        }
    } else if ([packet.name containsString:@"room/"]) {
        if ([packet.name containsString:@"authed"]) {
            TMRoom *room = [[TMRoom alloc] init];
            room.originalData = result;
            [TMGameManager sharedManager].room = room;

            if (self.connectedBlock) {
                self.connectedBlock(result);
                self.connectedBlock = nil;
            }
        }
        else if ([packet.name containsString:@"update"]) {
            TMRoom *room = [[TMRoom alloc] init];
            room.originalData = result;
            if ([packet.name containsString:@"/:startedAt"]) {
                [TMGameManager sharedManager].room = room;
                [[TMGameManager sharedManager] resetGameStatus];
            }
        }
        else if ([packet.name containsString:@"close"]) {
            AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
            [appDelegate.rootViewController.navigationController popToRootViewControllerAnimated:YES];
            if (self.socketIO.isConnected || self.socketIO.isConnecting) {
                [self.socketIO disconnect];
            }
        }
    }
}

- (void)socketIO:(SocketIO *)socket didSendMessage:(SocketIOPacket *)packet {
    
}
@end
