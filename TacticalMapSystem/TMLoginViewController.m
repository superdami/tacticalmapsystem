//
//  TMLoginViewController.m
//  TacticalMapSystem
//
//  Created by chen zhejun on 2015/03/15.
//  Copyright (c) 2015年 chen zhejun. All rights reserved.
//

#import "TMLoginViewController.h"

@implementation TMLoginViewController {
    UITextField *_emailField;
    UITextField *_nameField;
    UITextField *_passwordField;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGSize btnSize = CGSizeMake(250.0, 40.0);
    CGFloat x = (self.view.frame.size.width - 250.0) / 2.0;
    _emailField = [[UITextField alloc] initWithFrame:CGRectMake(x, 80.0, btnSize.width, btnSize.height)];
    _emailField.placeholder = @"email";
    [self.view addSubview:_emailField];
    
    _nameField = [[UITextField alloc] initWithFrame:CGRectMake(x, 130.0, btnSize.width, btnSize.height)];
    _nameField.placeholder = @"user name";
    [self.view addSubview:_nameField];
    
    _passwordField = [[UITextField alloc] initWithFrame:CGRectMake(x, 180.0, btnSize.width, btnSize.height)];
    _passwordField.placeholder = @"password";
    [self.view addSubview:_passwordField];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(x, 230, btnSize.width, btnSize.height);
    [button setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
    [button setTitle:@"Login" forState:UIControlStateNormal];
    [button handleControlEvents:UIControlEventTouchUpInside withBlock:^(id weakSender) {
        if (_nameField.text && _passwordField.text) {
            [TMAPIClient login:@{@"email": _emailField.text, @"password": _passwordField.text} completion:^(id result) {
                [[TMUserManager sharedInstance] setLoginData:result];
                [self dismissViewControllerAnimated:YES completion:nil];
            } failure:^(NSError *error) {
                NSLog(@"%@", error);
            }];
        }
    }];
    [self.view addSubview:button];
    
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(x, 280, btnSize.width, btnSize.height);
    [button setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
    [button setTitle:@"Sign up" forState:UIControlStateNormal];
    [button handleControlEvents:UIControlEventTouchUpInside withBlock:^(id weakSender) {
        if (_nameField.text && _passwordField.text) {
            [TMAPIClient signup:@{@"email": _emailField.text, @"name": _nameField.text, @"password": _passwordField.text} completion:^(id result) {
                [[TMUserManager sharedInstance] setLoginData:result];
                [self dismissViewControllerAnimated:YES completion:nil];
            } failure:^(NSError *error) {
                NSLog(@"%@", error);
            }];
        }
    }];
    [self.view addSubview:button];

    button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(x, 330, btnSize.width, btnSize.height);
    [button setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
    [button setTitle:@"Facebook login" forState:UIControlStateNormal];
    [button handleControlEvents:UIControlEventTouchUpInside withBlock:^(id weakSender) {
        [[TMUserManager sharedInstance] loginWithFB];
    }];
    [self.view addSubview:button];

    button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(x, 380, btnSize.width, btnSize.height);
    [button setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
    [button setTitle:@"Close" forState:UIControlStateNormal];
    [button handleControlEvents:UIControlEventTouchUpInside withBlock:^(id weakSender) {
        if (_nameField.text && _passwordField.text) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }];
    [self.view addSubview:button];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
}
@end
