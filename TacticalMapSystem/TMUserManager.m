//
//  TMUserManager.m
//  TacticalMapSystem
//
//  Created by Chen Zhejun on 7/17/14.
//  Copyright (c) 2014 chen zhejun. All rights reserved.
//

#import "SocketIO.h"
#import "SocketIOPacket.h"
#import "SocketIOJSONSerialization.h"
#import "MapViewController.h"
#import "FXKeychain.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface TMUserManager() <SocketIODelegate>
@property (nonatomic, strong)FBSDKLoginManager *fbLoginManager;
@end

@implementation TMUserManager {
    SocketIO *_socketId;
    CLGeocoder *_geocoder;
    void (^connectedBlock)(NSDictionary *info);
    NSString *_userInRoom;
    NSString *_token;
    
    FXKeychain *_keychain;
}

+ (TMUserManager *)sharedInstance {
    static TMUserManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[TMUserManager alloc] init];
    });
    
    return manager;
}

- (id)init {
    self = [super init];
    if (self) {
        _uuid = [[NSUUID UUID] UUIDString];
        
        _keychain = [[FXKeychain alloc] initWithService:[UIApplication appBundleId] accessGroup:nil accessibility:FXKeychainAccessibleAfterFirstUnlock];
    }
    return self;
}

- (void)setToken:(NSString *)token {
    _token = token;
    _keychain[@"token"] = token;
    
    //iOS issue - save keychain value may cause error code -34018
}

- (NSString *)token {
    if ([_token length]) {
        return _token;
    }
    
    NSString *token = _keychain[@"token"];
    if ([token length]) {
        _token = token;
        return token;
    }
    return nil;
}

- (void)authWithTokenCompletion:(void(^)())completeBlock {
    [TMAPIClient authToken:self.token completion:^(id result) {
        [self setLoginData:result];
        if (completeBlock) {
            completeBlock();
        }
    } failure:^(NSError *error) {
        [self logout];
        if (completeBlock) {
            completeBlock();
        }
    }];
}

- (void)setLoginData:(NSDictionary *)data {
    _token = [data objectForKey:@"token"];
    
    NSDictionary *user = [data objectForKey:@"user"];
    [self setUserData:user];
    
    _isLogin = YES;
    [self setToken:_token];
    [TMHTTPRequest loginWithToken:_token];
}

- (void)setUserData:(NSDictionary *)data {
    _userId = [data objectForKey:@"uid"];
    _userName = [data objectForKey:@"name"];
    _callsign = [data objectForKey:@"callsign"];
    _avatar = [data objectForKey:@"avatar"];
}

- (void)logout {
    _token = nil;
    _userName = nil;
    _isLogin = NO;
    
    [self setToken:nil];
    [TMHTTPRequest logout];
    [_socketId disconnect];
}

- (void)exitMapViewController {
    AppDelegate *appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    if([appdelegate.rootViewController.navigationController.topViewController isKindOfClass:[MapViewController class]]) {
        [((MapViewController *)appdelegate.rootViewController.navigationController.topViewController) forceExit];        
    }
}

- (FBSDKLoginManager *)fbLoginManager {
    if (!_fbLoginManager) {
        _fbLoginManager = [[FBSDKLoginManager alloc] init];
    }
    return _fbLoginManager;
}

- (void)loginWithFB {
    [self.fbLoginManager logInWithReadPermissions:@[@"public_profile", @"email",] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            
        } else if (result.isCancelled) {
            
        } else {
            [TMAPIClient facebookAuthToken:result.token.tokenString completion:^(NSDictionary *result) {
                [self setLoginData:result];
            } failure:nil];
        }
    }];
}
@end
