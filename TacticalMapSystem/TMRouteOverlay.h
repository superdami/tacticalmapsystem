//
//  TMRouteOverlay.h
//  TacticalMapSystem
//
//  Created by Chen Zhejun on 2013/12/27.
//  Copyright (c) 2013年 chen zhejun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@class TMRouteOverlay;
@class TMRouteRenderer;
@protocol TMRouteOverlayDelegate <NSObject>
@required;
- (void)overlayUpdateRect:(TMRouteOverlay *)overLay;
@end

@interface TMRouteOverlay : NSObject <MKOverlay> {
    NSMutableArray *_waypointAnnotations;
}
@property (weak)id<TMRouteOverlayDelegate> delegate;
@property (readonly) NSMutableArray *waypointAnnotations;
@property (readonly) MKMapRect lastUpdateRect;
@property (nonatomic, weak)MKMapView *mapView;
@property (nonatomic, assign)BOOL closePath;

- (id)initWithAnnotation:(NSObject<MKAnnotation> *)annotation delegate:(id<TMRouteOverlayDelegate>) delegate;
- (void)lockForReading;
- (void)unlockForReading;
- (void)addWaypointAnnptation:(NSObject <MKAnnotation> *)annotation;
- (void)insertAnnotation:(NSObject<MKAnnotation> *)newAnnotation atIndex:(NSInteger)index;
- (void)removeAnnotation:(NSObject <MKAnnotation> *)annotation;

- (void)beginUpdateAnnotation:(NSObject <MKAnnotation> *)annotation;
- (void)endUpdateAnnotation:(NSObject <MKAnnotation> *)annotation;
- (NSDictionary *)associatedPointsWithIndex:(NSUInteger)index;
@end
