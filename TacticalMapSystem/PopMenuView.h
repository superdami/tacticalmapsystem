//
//  PopMenuView.h
//  TacticalMapSystem
//
//  Created by chen zhejun on 10/1/12.
//  Copyright (c) 2012 chen zhejun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMCircleView.h"

@interface PopMenuView : UIView

- (void)showAnimatedPosition:(CGPoint)point;
- (void)hideAnimated;
- (void)handleGestureChange:(UILongPressGestureRecognizer *)gesture;
- (void)handleGestureEnd:(UILongPressGestureRecognizer *)gesture;
- (void)setMenuWithTypes:(NSArray *)types annotationType:(NSArray *)annotationTypes handleBlock:(void (^)(TMAnnotationType selectedType, CGPoint markPoint))block;

@end
