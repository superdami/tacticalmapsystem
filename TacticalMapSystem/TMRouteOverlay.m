//
//  TMRouteOverlay.m
//  TacticalMapSystem
//
//  Created by Chen Zhejun on 2013/12/27.
//  Copyright (c) 2013年 chen zhejun. All rights reserved.
//

#import "TMRouteOverlay.h"
#import "TMRouteRenderer.h"
#import "TMHelper.h"
#import <pthread.h>

#define WORLD_SCALE         4.0
@implementation TMRouteOverlay
{
    MKMapRect _boundingMapRect;
    pthread_rwlock_t _rwLock;
    MKMapRect _oldRect;
}

- (id)initWithAnnotation:(NSObject<MKAnnotation> *)annotation delegate:(id<TMRouteOverlayDelegate>) delegate
{
    self = [super init];
    if (self) {
        _delegate = delegate;

        MKMapPoint origin = MKMapPointForCoordinate(annotation.coordinate);
        origin.x -= MKMapSizeWorld.width / (WORLD_SCALE * 2);
        origin.y -= MKMapSizeWorld.height / (WORLD_SCALE * 2);
        MKMapSize size = MKMapSizeWorld;
        size.width /= WORLD_SCALE;
        size.height /= WORLD_SCALE;
        _boundingMapRect = (MKMapRect) { origin, size };
        MKMapRect worldRect = MKMapRectMake(0, 0, MKMapSizeWorld.width, MKMapSizeWorld.height);
        _boundingMapRect = MKMapRectIntersection(_boundingMapRect, worldRect);
        
        _waypointAnnotations = [NSMutableArray new];
        [self addWaypointAnnptation:annotation];
    }
    return self;
}

- (void)setMapView:(MKMapView *)mapView {
    if (_mapView) {
        [_mapView removeAnnotations:_waypointAnnotations];
        [_mapView removeOverlay:self];
    }
    
    _mapView = mapView;
    if (_mapView) {
        [_mapView addOverlay:self];
        [_mapView addAnnotations:_waypointAnnotations];
    }
}

- (void)dealloc
{
    pthread_rwlock_destroy(&_rwLock);
    [self setMapView:nil];
    for (NSObject<MKAnnotation> *annotation in _waypointAnnotations) {
        [annotation removeObserver:self forKeyPath:@"coordinate"];
    }
}

- (MKMapRect)boundingMapRect
{
    return _boundingMapRect;
}

- (CLLocationCoordinate2D)coordinate
{
    if ([_waypointAnnotations count]) {
        NSObject<MKAnnotation> *annotation = [_waypointAnnotations objectAtIndex:0];
        return annotation.coordinate;
    }
    return CLLocationCoordinate2DMake(0, 0);
}

- (void)lockForReading
{
    pthread_rwlock_rdlock(&_rwLock);
}

- (void)unlockForReading
{
    pthread_rwlock_unlock(&_rwLock);
}

- (void)redrawRenderer:(MKMapRect)mapRect {
    MKZoomScale currentZoomScale = (CGFloat)(_mapView.bounds.size.width / _mapView.visibleMapRect.size.width);
    TMRouteRenderer *pathRenderer = (TMRouteRenderer *)[_mapView rendererForOverlay:self];
    [pathRenderer updateDrawRect:mapRect zoomScale:currentZoomScale];
}

- (void)beginUpdateAnnotation:(NSObject <MKAnnotation> *)annotation {
    _oldRect = [self updatePathNode:annotation];
}

- (void)endUpdateAnnotation:(NSObject <MKAnnotation> *)annotation {
    MKMapRect newRect = [self updatePathNode:annotation];
    MKMapRect updateRect = MKMapRectUnion(newRect, _oldRect);
    [self redrawRenderer:updateRect];
    
    if(_delegate && [_delegate respondsToSelector:@selector(overlayUpdateRect:)]) {
        [_delegate overlayUpdateRect:self];
    }
    _oldRect = MKMapRectNull;
}

- (void)addWaypointAnnptation:(NSObject <MKAnnotation> *)annotation {
    MKMapRect updateRect = [self addPathNode:annotation];
    [self redrawRenderer:updateRect];
    if (_mapView) {
        [_mapView addAnnotation:annotation];
    }
}

- (void)removeAnnotation:(NSObject <MKAnnotation> *)annotation {
    if ([_waypointAnnotations containsObject:annotation]) {
        [annotation removeObserver:self forKeyPath:@"coordinate"];
        NSInteger index = [_waypointAnnotations indexOfObject:annotation];
        MKMapRect updateRect = [self removePathNodeAtIndex:index];
        [self redrawRenderer:updateRect];
        
        if (_mapView) {
            [_mapView removeAnnotation:annotation];
        }
    }
}

- (void)insertAnnotation:(NSObject<MKAnnotation> *)newAnnotation atIndex:(NSInteger)index {
    if ([_waypointAnnotations count] - 1 >= index) {
        [newAnnotation addObserver:self forKeyPath:@"coordinate" options:NSKeyValueObservingOptionPrior | NSKeyValueObservingOptionNew context:NULL];
        [self lockForReading];
        [_waypointAnnotations insertObject:newAnnotation atIndex:index];
        MKMapRect updateRect = [self mapRectAssociatedWithWaypointAtIndex:index];
        [self unlockForReading];
        [self redrawRenderer:updateRect];
        
        if (_mapView) {
            [_mapView addAnnotation:newAnnotation];
        }
    }
    else {
        [self addWaypointAnnptation:newAnnotation];
    }
}

- (MKMapRect)addPathNode:(NSObject<MKAnnotation> *)annotation {
    [annotation addObserver:self forKeyPath:@"coordinate" options:NSKeyValueObservingOptionPrior | NSKeyValueObservingOptionNew context:NULL];
    [self lockForReading];
    NSInteger index = [_waypointAnnotations count];
    
    [_waypointAnnotations addObject:annotation];
    MKMapRect updateRect = [self mapRectAssociatedWithWaypointAtIndex:index];
    [self unlockForReading];    
    return updateRect;
}

- (MKMapRect)removePathNodeAtIndex:(NSUInteger)index {
    if (index < [_waypointAnnotations count]) {
        [self lockForReading];
        MKMapRect updateRect = [self mapRectAssociatedWithWaypointAtIndex:index];
        [_waypointAnnotations removeObjectAtIndex:index];
        [self unlockForReading];
        
        return updateRect;
    }
    return MKMapRectNull;
}

- (MKMapRect)updatePathNode:(NSObject<MKAnnotation> *)annotation {
    if ([_waypointAnnotations containsObject:annotation]) {
        [self lockForReading];
        NSInteger index = [_waypointAnnotations indexOfObject:annotation];
        MKMapRect updateRect = [self mapRectAssociatedWithWaypointAtIndex:index];
        [self unlockForReading];
        return updateRect;
    }
    return MKMapRectNull;
}

- (NSDictionary *)associatedPointsWithIndex:(NSUInteger)index {
    if ([_waypointAnnotations count] > index && [_waypointAnnotations count] > 1) {
        NSObject<MKAnnotation> *prePoint, *nextPoint;
        if (index > 0) {
            prePoint = [_waypointAnnotations objectAtIndex:index - 1];
        }
        else if (self.closePath){
            prePoint = [_waypointAnnotations lastObject];
        }
        
        if (index < [_waypointAnnotations count] - 1) {
            nextPoint = [_waypointAnnotations objectAtIndex:index + 1];
        }
        else if (self.closePath) {
            nextPoint = [_waypointAnnotations objectAtIndex:0];
        }
        
        NSMutableDictionary *dict = [NSMutableDictionary new];
        if (prePoint) {
            [dict setObject:prePoint forKey:@"prepoint"];
        }
        if (nextPoint) {
            [dict setObject:nextPoint forKey:@"nextpoint"];
        }
        
        return dict;
    }
    return nil;
}

- (MKMapRect)mapRectAssociatedWithWaypointAtIndex:(NSUInteger)index {
    if ([_waypointAnnotations count] > index) {
        NSObject<MKAnnotation> *targetWaypoint = (NSObject<MKAnnotation> *)[_waypointAnnotations objectAtIndex:index];
        
        MKMapRect prevUpdateRect = MKMapRectNull;
        MKMapRect nextUpdateRect = MKMapRectNull;
        MKMapPoint targetPoint = MKMapPointForCoordinate(targetWaypoint.coordinate);
        
        NSDictionary *points = [self associatedPointsWithIndex:index];

        NSObject<MKAnnotation> *pre = [points objectForKey:@"prepoint"];
        NSObject<MKAnnotation> *next = [points objectForKey:@"nextpoint"];
        if (pre) {
            MKMapPoint prevPoint = MKMapPointForCoordinate(pre.coordinate);
            prevUpdateRect = [TMHelper makeRectWithPoint:prevPoint and:targetPoint];
        }
        
        if (next) {
            MKMapPoint nextPoint = MKMapPointForCoordinate(next.coordinate);
            nextUpdateRect = [TMHelper makeRectWithPoint:targetPoint and:nextPoint];
        }
        
        MKMapRect updateRect = MKMapRectUnion(prevUpdateRect, nextUpdateRect);
        return updateRect;
    }
    
    
    return MKMapRectNull;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([change objectForKey:@"notificationIsPrior"]) {
        [self beginUpdateAnnotation:object];
    }
    else if ([change objectForKey:@"new"]) {
        [self endUpdateAnnotation:object];
    }
}
@end
