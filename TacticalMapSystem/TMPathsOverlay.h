//
//  TMPathsOverlay.h
//  TacticalMapSystem
//
//  Created by Chen Zhejun on 7/25/14.
//  Copyright (c) 2014 chen zhejun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface TMPathsOverlay : NSObject<MKOverlay>
@property (readonly) NSMutableArray *paths;
@property (readonly) MKMapRect lastUpdateRect;
@property (nonatomic, weak)MKMapView *mapView;
@property (nonatomic, assign)BOOL closePath;
- (id)init;
- (void)lockForReading;
- (void)unlockForReading;
- (void)addPath:(NSArray *)path;
- (void)insertPath:(NSArray *)path atIndex:(NSUInteger)index;
- (void)removePathAtIndex:(NSUInteger)index;
@end
