//
//  TMRectOverlay.h
//  TacticalMapSystem
//
//  Created by Chen Zhejun on 2014/09/02.
//  Copyright (c) 2014年 chen zhejun. All rights reserved.
//

#import "TMRouteOverlay.h"
#import "TMAnnotationView.h"

@class TMAnnotation;
@interface TMRectOverlay : TMRouteOverlay <TMAnnotationViewDelegate>
@property (nonatomic, readonly)TMAnnotation *centerAnnotation;

- (id)initWithRegion:(MKCoordinateRegion) region mapView:(MKMapView *)mapView;
- (void)movedAnnotation:(NSObject<MKAnnotation> *)annotation;
- (void)setRegion:(MKCoordinateRegion)region;
//- (void)resetConersCoordinate;
@end
