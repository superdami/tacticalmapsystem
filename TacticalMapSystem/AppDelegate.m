//
//  AppDelegate.m
//  TacticalMapSystem
//
//  Created by chen zhejun on 9/20/12.
//  Copyright (c) 2012 chen zhejun. All rights reserved.
//

#import "AppDelegate.h"
#import "RootViewController.h"
#import "TMNavigationController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "FXKeychain.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    RootViewController *rootViewController = [[RootViewController alloc] init];
    _rootViewController = rootViewController;
    TMNavigationController *navigationC = [[TMNavigationController alloc] initWithRootViewController:rootViewController];
    self.window.rootViewController = navigationC;
    
    NSString *token = [TMUserManager sharedInstance].token;
    if ([FBSDKAccessToken currentAccessToken]) {
        
    } else if ([token length] > 0) {
        [[TMUserManager sharedInstance] authWithTokenCompletion:^{
            if([TMUserManager sharedInstance].isLogin) {
                [rootViewController checkRoom];
            } else {
                [rootViewController showLoginViewController];                
            }
        }];
    } else {
        [rootViewController showLoginViewController];
    }
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                    didFinishLaunchingWithOptions:launchOptions];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}
@end
