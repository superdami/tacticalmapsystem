//
//  PopMenuView.m
//  TacticalMapSystem
//
//  Created by chen zhejun on 10/1/12.
//  Copyright (c) 2012 chen zhejun. All rights reserved.
//

#import "PopMenuView.h"
#import "TMAnnotationManager.h"

@implementation PopMenuView {
    CGPoint _markPoint;
    NSMutableArray *_enabledPopViews;
    void (^handleBlock)(TMAnnotationType type, CGPoint markPoint);
    CGFloat _outerRadius;
    CGFloat _innerRadius;
    NSMutableArray *_circleViews;
    TMCircleViewType _highlightedType;
    NSArray *_annotationTypes;
    NSArray *_circleViewTypes;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {        
        _circleViews = [NSMutableArray array];
        CGPoint point[4];
        
        CGPoint center = CGPointMake(ceilf(self.bounds.size.width / 2.0), ceilf(self.bounds.size.height / 2.0));
        CGFloat centerOffset = 4.0;
        _outerRadius = MIN(frame.size.width, frame.size.height) / 2.0;
        _innerRadius = 20.0;
        
        TMCircleView *topView = [[TMCircleView alloc] initWithRadius:_outerRadius innerRadius:_innerRadius type:TMCircleViewTypeTop];
        point[0] = CGPointMake(center.x - kCeilfHalf(topView.frame.size.width), center.y - topView.frame.size.height - centerOffset);
        [self addSubview:topView];
        [_circleViews addObject:topView];
        
        TMCircleView *rightView = [[TMCircleView alloc] initWithRadius:_outerRadius innerRadius:_innerRadius type:TMCircleViewTypeRight];
        point[1] = CGPointMake(center.x + centerOffset, center.y - kCeilfHalf(rightView.frame.size.height));
        [self addSubview:rightView];
        [_circleViews addObject:rightView];
        
        TMCircleView *bottomView = [[TMCircleView alloc] initWithRadius:_outerRadius innerRadius:_innerRadius type:TMCircleViewTypeBottom];
        point[2] = CGPointMake(center.x - kCeilfHalf(bottomView.frame.size.width), center.y + centerOffset);
        [self addSubview:bottomView];
        [_circleViews addObject:bottomView];
        
        TMCircleView *leftView = [[TMCircleView alloc] initWithRadius:_outerRadius innerRadius:_innerRadius type:TMCircleViewTypeLeft];
        point[3] = CGPointMake(center.x - leftView.frame.size.width - centerOffset, center.y - kCeilfHalf(leftView.frame.size.height));
        [self addSubview:leftView];
        [_circleViews addObject:leftView];
        
        for (int i = 0; i < 4; i++) {
            TMCircleView *view = [_circleViews objectAtIndex:i];
            view.frame = CGRectMake(point[i].x, point[i].y, view.frame.size.width, view.frame.size.height);
            view.alpha = 0.0;
        }
        
        _highlightedType = TMCircleViewTypeNone;
    }
    
    return self;
}

- (void)setMenuWithTypes:(NSArray *)types annotationType:(NSArray *)annotationTypes handleBlock:(void (^)(TMAnnotationType selectedType, CGPoint markPoint))block {
    NSInteger count = [types count];
    if ((count != [annotationTypes count])) {
        return;
    }
    
    _annotationTypes = annotationTypes;
    _circleViewTypes = types;
    handleBlock = block;
    for (UIView *view in _circleViews) {
        NSArray *views = [view subviews];
        for (UIView *subView in views) {
            if ([subView isKindOfClass:[UIImageView class]]) {
                [subView removeFromSuperview];
            }
        }
    }
    
    _enabledPopViews = [[NSMutableArray alloc] initWithCapacity:count];
    
    for (int i = 0; i < count; i++) {
        TMCircleViewType type = (TMCircleViewType)[[types objectAtIndex:i] unsignedIntegerValue];
        [_enabledPopViews addObject:[_circleViews objectAtIndex:type]];
    }
    
    for(int i = 0; i < [_enabledPopViews count]; i++) {
        UIView *popView = [_enabledPopViews objectAtIndex:i];
        TMAnnotationType type = (TMAnnotationType)[[annotationTypes objectAtIndex:i] integerValue];
        UIImage *image = [TMAnnotationManager imageWithAnnotationType:type];
        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
        imageView.frame = CGRectMake(ceilf((popView.bounds.size.width - imageView.bounds.size.width) / 2.0),
                                     ceilf((popView.bounds.size.height - imageView.bounds.size.height) / 2.0), image.size.width, image.size.height);
        [popView addSubview:imageView];
    }
}

- (void)handleGestureChange:(UILongPressGestureRecognizer *)gesture {
    
    CGPoint p = [gesture locationInView:self];
    CGPoint center = CGPointMake(self.bounds.size.width / 2.0, self.bounds.size.height / 2.0);
    CGFloat distance = sqrtf(pow((p.x - center.x), 2) + pow((p.y - center.y), 2));
    TMCircleViewType type = 5;
    if (distance < _outerRadius && distance > _innerRadius) {
        CGPoint vector = CGPointMake(p.x - center.x, p.y - center.y);
        CGFloat n = atan(vector.y / vector.x);
        
        if (vector.x < 0) {
            if (vector.y > 0) {
                n = M_PI + n;
            } else {
                n = -M_PI + n;
            }
        }
        
        if (n > -M_PI + M_PI_4 && n < -M_PI_4) {
            type = TMCircleViewTypeTop;
        } else if (n > -M_PI_4 && n < M_PI_4){
            type = TMCircleViewTypeRight;
        } else if (n > M_PI_4 && n < M_PI - M_PI_4) {
            type = TMCircleViewTypeBottom;
        } else if (n > M_PI - M_PI_4 || n < -M_PI + M_PI_4) {
            type = TMCircleViewTypeLeft;
        }
    }
    [self highLightCircleViewWithType:type];
}

- (void)highLightCircleViewWithType:(TMCircleViewType)type {
    if(_highlightedType == type) return;
    
    for(TMCircleView *view in _circleViews) {
        [UIView animateWithDuration:0.1 animations:^{
            view.transform = CGAffineTransformIdentity;
        } completion:nil];
    }
    
    CGPoint offset;
    if (type == TMCircleViewTypeTop) {
        offset = CGPointMake(0.0, -10.0);
    } else if (type == TMCircleViewTypeRight) {
        offset = CGPointMake(10.0, 0.0);
    } else if (type == TMCircleViewTypeBottom) {
        offset = CGPointMake(0.0, 10.0);
    } else if (type == TMCircleViewTypeLeft) {
        offset = CGPointMake(-10.0, 0.0);
    }
    
    if (type < [_circleViews count]) {
        [UIView animateWithDuration:0.1 animations:^{
            TMCircleView *circle = [_circleViews objectAtIndex:type];
            circle.transform = CGAffineTransformMakeScale(1.1, 1.1);
            circle.transform = CGAffineTransformTranslate(circle.transform, offset.x, offset.y);
        } completion:nil];
    }
    _highlightedType = type;
}

- (void)handleGestureEnd:(UILongPressGestureRecognizer *)gesture {
    NSUInteger index = NSIntegerMax;
    for (int i = 0; i < [_circleViewTypes count]; i++) {
        if ([_circleViewTypes[i] integerValue] == _highlightedType) {
            index = i;
            break;
        }
    }
    
    if (index < [_enabledPopViews count]) {
        TMAnnotationType type = (TMAnnotationType)[_annotationTypes[index] integerValue];
        handleBlock(type, _markPoint);
    }
    if (_highlightedType < [_circleViews count]) {
        [UIView animateWithDuration:0.1 animations:^{
            TMCircleView *circle = [_circleViews objectAtIndex:_highlightedType];
            circle.transform = CGAffineTransformIdentity;
        } completion:nil];
    }
    _highlightedType = TMCircleViewTypeNone;
}

- (void)showAnimatedPosition:(CGPoint)point {
    _markPoint = point;
    self.hidden = NO;
    CGRect f = self.frame;
    f.origin = CGPointMake(point.x - ceilf(self.bounds.size.width / 2.0), point.y - ceilf(self.bounds.size.height / 2.0));
    self.frame = f;
    
    int i = 0;
    for (UIView *view in _enabledPopViews) {
        [UIView animateWithDuration:0.1 delay:i * 0.1 options:UIViewAnimationCurveEaseIn animations:^{view.alpha = 1.0;} completion:nil];
        i++;
    }
}

- (void)hideAnimated {
    int i = 0;
    for (UIView *view in _enabledPopViews) {
        __weak UIView *weakView = view;
        __weak PopMenuView *weakSelf = self;
        [UIView animateWithDuration:0.1 delay:i * 0.1 options:UIViewAnimationCurveEaseOut animations:^{view.alpha = 0.0;} completion:^(BOOL complete){
            NSInteger index = [_enabledPopViews indexOfObject:weakView];
            if (index == [_enabledPopViews count] - 1) {
                weakSelf.hidden = YES;
            }
        }];
    }
}
@end
