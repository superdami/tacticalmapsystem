//
//  CountDownView.m
//  TacticalMapSystem
//
//  Created by chen zhejun on 2015/05/24.
//  Copyright (c) 2015年 chen zhejun. All rights reserved.
//

#import "CountDownView.h"

@implementation CountDownView {
    UILabel *_label;
    UIView *_flashView;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.userInteractionEnabled = NO;
        
        _flashView = [[UIView alloc] initWithFrame:self.bounds];
        _flashView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _flashView.backgroundColor = [UIColor orangeColor];
        [self addSubview:_flashView];
        
        _label = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 200.0, 200.0)];
        _label.backgroundColor = [UIColor clearColor];
        _label.center = self.center;
        _label.textAlignment = NSTextAlignmentCenter;
        _label.font = [UIFont systemFontOfSize:self.bounds.size.height / 6.0];
        _label.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
        [self addSubview:_label];
    }
    return self;
}

- (void)updateCountDown:(NSInteger)number {
    if (number < 0) {
        _label.text = [NSString stringWithFormat:@"%ld", ABS(number)];
    } else {
        _label.text = @"Go";
    }
    _flashView.alpha = 1.0;
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        _flashView.alpha = 0.0;
    } completion:nil];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
