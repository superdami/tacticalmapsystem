//
//  TMPopupView.m
//  TacticalMapSystem
//
//  Created by chen zhejun on 2015/05/10.
//  Copyright (c) 2015年 chen zhejun. All rights reserved.
//

#import "TMPopupView.h"
@interface TMPopupView()
@end

@implementation TMPopupView {
    UIButton *_rotateLockButton;
    UISegmentedControl *_mapTypeControl;
    UISegmentedControl *_userStatusControl;
    UIButton *_gameStatusButton;
}

- (void)initControls {
    _rotateLockButton = [UIButton buttonWithType:UIButtonTypeSystem];
    _rotateLockButton.frame = CGRectMake(0.0, 10.0, self.frame.size.width, 30.0);
    [_rotateLockButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_rotateLockButton setBackgroundColor:[UIColor grayColor]];
    self.isRotateLocked = NO;
    [_rotateLockButton handleControlEvents:UIControlEventTouchUpInside withBlock:^(id weakSender) {
        self.isRotateLocked = !self.isRotateLocked;
        [_delegate lockRotateStatusChanged:self.isRotateLocked];
    }];
    [self addSubview:_rotateLockButton];
    
    _mapTypeControl = [[UISegmentedControl alloc] initWithItems:@[@"Standard", @"Satellite", @"Hybrid"]];
    _mapTypeControl.frame = CGRectMake(0.0, 50.0, self.frame.size.width, 30.0);
    [_mapTypeControl handleControlEvents:UIControlEventValueChanged withBlock:^(UISegmentedControl *weakSender) {
        _mapType = weakSender.selectedSegmentIndex;
        [_delegate mapTypeSelected:weakSender.selectedSegmentIndex];
    }];
    [self addSubview:_mapTypeControl];
    
    _userStatusControl = [[UISegmentedControl alloc] initWithItems:@[@"Idle", @"In action", @"KIA"]];
    _userStatusControl.frame = CGRectMake(0.0, 90.0, self.frame.size.width, 30.0);
    [_userStatusControl handleControlEvents:UIControlEventValueChanged withBlock:^(UISegmentedControl *weakSender) {
        _userStatus = weakSender.selectedSegmentIndex;
        [_delegate userStatusSelected:(TMAnnotationStatus)weakSender.selectedSegmentIndex];
    }];
    [self addSubview:_userStatusControl];
    
    _gameStatusButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _gameStatusButton.frame = CGRectMake(0.0, 130.0, self.frame.size.width, 30.0);
    [_gameStatusButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_gameStatusButton handleControlEvents:UIControlEventTouchUpInside withBlock:^(id weakSender) {
        [_delegate gameStatusChanged:!self.gameStarted];
    }];
    [self addSubview:_gameStatusButton];
    
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        UIVisualEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
        UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        visualEffectView.frame = self.bounds;
        visualEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self addSubview:visualEffectView];

        [self initControls];
    }
    return self;
}

- (void)setMapType:(MKMapType)mapType {
    _mapType = mapType;
    [_mapTypeControl setSelectedSegmentIndex:_mapType];
}

- (void)setIsRotateLocked:(BOOL)isRotateLocked {
    _isRotateLocked = isRotateLocked;
    if (_isRotateLocked) {
        [_rotateLockButton setTitle:@"Rotate Locked" forState:UIControlStateNormal];
    } else {
        [_rotateLockButton setTitle:@"Rotate Unlocked" forState:UIControlStateNormal];
    }
}

- (void)setUserStatus:(NSUInteger)userStatus {
    _userStatus = userStatus;
    [_userStatusControl setSelectedSegmentIndex:userStatus];
}

- (void)setGameStarted:(BOOL)gameStarted {
    _gameStarted = gameStarted;
    if (_gameStarted) {        
        [_gameStatusButton setBackgroundColor:[UIColor redColor]];
        [_gameStatusButton setTitle:@"End Game" forState:UIControlStateNormal];
    } else {
        [_gameStatusButton setBackgroundColor:[UIColor greenColor]];
        [_gameStatusButton setTitle:@"Start Game" forState:UIControlStateNormal];
    }
}
@end
