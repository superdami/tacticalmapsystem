//
//  TMPopupView.h
//  TacticalMapSystem
//
//  Created by chen zhejun on 2015/05/10.
//  Copyright (c) 2015年 chen zhejun. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMPopupViewDelegate <NSObject>
- (void)mapTypeSelected:(MKMapType)type;
- (void)userStatusSelected:(TMAnnotationStatus)index;
- (void)lockRotateStatusChanged:(BOOL)locked;
- (void)gameStatusChanged:(BOOL)started;
@end

@interface TMPopupView : UIView
@property(nonatomic, weak)id<TMPopupViewDelegate> delegate;
@property(nonatomic, assign)MKMapType mapType;
@property(nonatomic, assign)BOOL isRotateLocked;
@property(nonatomic, assign)NSUInteger userStatus;
@property(nonatomic, assign)BOOL gameStarted;
@end
