//
//  CountDownView.h
//  TacticalMapSystem
//
//  Created by chen zhejun on 2015/05/24.
//  Copyright (c) 2015年 chen zhejun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CountDownView : UIView

- (void)updateCountDown:(NSInteger)number;
@end
