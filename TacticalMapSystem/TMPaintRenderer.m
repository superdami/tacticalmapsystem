//
//  TMPaintRenderer.m
//  TacticalMapSystem
//
//  Created by Chen Zhejun on 2014/01/07.
//  Copyright (c) 2014年 chen zhejun. All rights reserved.
//

#import "TMPaintRenderer.h"
#import "TMPaintOverlay.h"

@implementation TMPaintRenderer

- (void)drawMapRect:(MKMapRect)mapRect
          zoomScale:(MKZoomScale)zoomScale
          inContext:(CGContextRef)context
{
    TMPaintOverlay *overlayer = self.overlay;
    CGFloat lineWidth = MKRoadWidthAtZoomScale(zoomScale);
    
    
    // outset the map rect by the line width so that points just outside
    // of the currently drawn rect are included in the generated path.
    MKMapRect clipRect = MKMapRectInset(mapRect, -lineWidth, -lineWidth);
    
    [overlayer lockForReading];
    for (int i = 0; i < [overlayer.paths count]; i++) {
        NSArray *array = [overlayer.paths objectAtIndex:i];
        CGPathRef path = [self newPathForLocations:array clipRect:clipRect zoomScale:zoomScale];
        
        if (path != nil)
        {
            
            CGContextAddPath(context, path);
            CGContextSetRGBStrokeColor(context, _red / 255.0, _green / 255.0, _blue / 255.0, _alpha);
            if (self.lineDash) {
                CGFloat dashes[] = {40.0 / zoomScale ,20.0 / zoomScale};
                CGContextSetLineDash(context, 0.0, dashes, 2);
            }
            else {
                CGContextSetLineJoin(context, kCGLineJoinRound);
                CGContextSetLineCap(context, kCGLineCapRound);
            }
            CGContextSetLineWidth(context, lineWidth);
            CGContextStrokePath(context);
            CGPathRelease(path);
        }
    }
    [overlayer unlockForReading];
}
@end
