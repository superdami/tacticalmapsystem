//
//  TMUserViewController.m
//  TacticalMapSystem
//
//  Created by chen zhejun on 2015/05/28.
//  Copyright (c) 2015年 chen zhejun. All rights reserved.
//

#import "TMUserViewController.h"
#import "TMUserManager.h"
#import "UIImage+Resize.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface TMUserViewController () <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate>

@end

@implementation TMUserViewController {
    UIImageView *_avatarImageView;
    UITextField *_nameTextField;
    UITextField *_callSignField;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _avatarImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 200.0, 200.0)];
    UIButton *avatarBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    avatarBtn.frame = _avatarImageView.bounds;
    avatarBtn.backgroundColor = [UIColor grayColor];
    [avatarBtn addSubview:_avatarImageView];
    [self.view addSubview:avatarBtn];
    [avatarBtn handleControlEvents:UIControlEventTouchUpInside withBlock:^(id weakSender) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Avatar" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera", @"Photo Library", nil];
        actionSheet.actionSheetStyle = UIBarStyleBlackTranslucent;
        [actionSheet showInView:self.view];
    }];

    CGRect f = avatarBtn.frame;
    f.origin.x = (self.view.frame.size.width - 200.0) / 2.0;
    f.origin.y = self.view.frame.size.height / 2.0 - 200.0;
    avatarBtn.frame = f;
    [self loadAvatarImage];
    
    _nameTextField = [[UITextField alloc] initWithFrame:CGRectMake(avatarBtn.frame.origin.x, CGRectGetMaxY(avatarBtn.frame) + 30.0, 200.0, 60.0)];
    _nameTextField.textColor = [UIColor darkTextColor];
    _nameTextField.textAlignment = NSTextAlignmentCenter;
    _nameTextField.text = [TMUserManager sharedInstance].userName;
    [self.view addSubview:_nameTextField];
    
    _callSignField = [[UITextField alloc] initWithFrame:CGRectMake(_nameTextField.frame.origin.x, CGRectGetMaxY(_nameTextField.frame) + 30.0, 200.0, 60.0)];
    _callSignField.textColor = [UIColor darkTextColor];
    _callSignField.textAlignment = NSTextAlignmentCenter;
    _callSignField.text = [TMUserManager sharedInstance].callsign;
    [self.view addSubview:_callSignField];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:^{
        UIImage *image = info[@"UIImagePickerControllerEditedImage"];
        NSData *data = UIImageJPEGRepresentation(image, 0.7);
        [TMAPIClient uploadAvatarImageData:data Completion:^(NSDictionary *result) {
            [[TMUserManager sharedInstance] setUserData:result];
            [self loadAvatarImage];
        } failure:nil];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)loadAvatarImage {
    NSString *path = [[NSURL URLWithString:[TMUserManager sharedInstance].avatar relativeToURL:[NSURL URLWithString:kBaseURL]] absoluteString];
    NSURL *url = [NSURL URLWithString:path];
    [_avatarImageView sd_setImageWithURL:url];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex < 2) {
        UIImagePickerController *imagePickerVc = [[UIImagePickerController alloc] init];
        imagePickerVc.allowsEditing = YES;
        imagePickerVc.delegate = self;
        imagePickerVc.sourceType = (buttonIndex > 0) ? UIImagePickerControllerSourceTypePhotoLibrary : UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:imagePickerVc animated:YES completion:nil];
    }
}
@end
