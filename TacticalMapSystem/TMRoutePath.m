//
//  TMRoutePath.m
//  TacticalMapSystem
//
//  Created by Chen Zhejun on 2014/01/01.
//  Copyright (c) 2014年 chen zhejun. All rights reserved.
//

#import "TMRoutePath.h"
#import "TMAnnotation.h"

@implementation TMRoutePath
- (id)init {
    self = [super init];
    _currentWaypointIndex = NSNotFound;
    return self;
}

//- (void)addWaypointAnnptation:(NSObject <MKAnnotation> *)annotation {
//    if (!_pathOverlayer) {
//        _pathOverlayer = [[TMRouteOverlayer alloc] initWithCenterCoordinate:annotation.coordinate];
//        _pathRenderer = [[TMRouteRenderer alloc] initWithOverlay:_pathOverlayer];
//        [_pathRenderer setPathColorWithRed:43.0 green:146.0 blue:247.0 alpha:0.8];
//        _pathRenderer.lineDash = YES;
//        [_delegate addOverlayToMapView:_pathOverlayer];
//    }
//    
//    MKMapRect updateRect = [_pathOverlayer addPathNode:annotation];
//    MKZoomScale currentZoomScale = [_delegate mapCurrentZoomScale];
//    [_pathRenderer updateDrawRect:updateRect zoomScale:currentZoomScale];
//}

//- (void)updateWaypointAtIndex:(NSUInteger) index {
//    MKMapRect updateRect = [_pathOverlayer updatePathNodeAtIndex:index];
//    MKZoomScale currentZoomScale = [_delegate mapCurrentZoomScale];
//    [_pathRenderer updateDrawRect:updateRect zoomScale:currentZoomScale];
//}

//- (void)updateWaypoint:(NSObject <MKAnnotation> *)annotation {
//    if ([_pathOverlayer.waypointAnnotations containsObject:annotation]) {
//        NSInteger index = [_pathOverlayer.waypointAnnotations indexOfObject:annotation];
//        [self updateWaypointAtIndex:index];
//    }
//}

//- (void)trackAnnotation:(NSObject<MKAnnotation> *)annotation withWaypointIndex:(NSUInteger)index {
//    if (!_trackingOverlayer) {
//        _trackingOverlayer = [[TMRouteOverlayer alloc] initWithCenterCoordinate:annotation.coordinate];
//        _trackingRenderer = [[TMRouteRenderer alloc] initWithOverlay:_trackingOverlayer];
//        [_trackingRenderer setPathColorWithRed:255.0 green:146.0 blue:247.0 alpha:0.8];
//        [_delegate addOverlayToMapView:_trackingOverlayer];
//    }
//    
//    [_trackingOverlayer.waypointAnnotations removeAllObjects];
//    if (index < [_pathOverlayer.waypointAnnotations count]) {
//        _currentWaypointIndex = index;
//        NSObject<MKAnnotation> *waypointAnnot = [_pathOverlayer.waypointAnnotations objectAtIndex:_currentWaypointIndex];
//        [_trackingOverlayer addPathNode:annotation];
//        [_trackingOverlayer addPathNode:waypointAnnot];
//        
//        [self updateTrackingAnnotation];
//    }
//}

//- (void)updateTrackingAnnotation {
//    
//    if ([_trackingOverlayer.waypointAnnotations count]) {
//        NSObject<MKAnnotation> *waypointAnnot1 = [_trackingOverlayer.waypointAnnotations objectAtIndex:0];
//        NSObject<MKAnnotation> *waypointAnnot2 = [_trackingOverlayer.waypointAnnotations objectAtIndex:1];
//        MKMapPoint point1 = MKMapPointForCoordinate(waypointAnnot1.coordinate);
//        MKMapPoint point2 = MKMapPointForCoordinate(waypointAnnot2.coordinate);
//        
//        CLLocationDistance dis = MKMetersBetweenMapPoints(point1, point2);
//        NSLog(@"distance %f", dis);
//        if (20 > dis){
//            if (_currentWaypointIndex < [_pathOverlayer.waypointAnnotations count] - 1) {
//                _currentWaypointIndex++;
//                NSObject<MKAnnotation> *waypointAnnot = [_pathOverlayer.waypointAnnotations objectAtIndex:_currentWaypointIndex];
//                [_trackingOverlayer.waypointAnnotations replaceObjectAtIndex:1 withObject:waypointAnnot];
//            }
//        }
//        
//        MKMapRect updateRect = [_trackingOverlayer updatePathNodeAtIndex:1];
//        MKZoomScale currentZoomScale = [_delegate mapCurrentZoomScale];
//        [_trackingRenderer updateDrawRect:updateRect zoomScale:currentZoomScale];
//    }
//}

- (void)untrackAnnotation {
    [_trackingOverlayer.waypointAnnotations removeAllObjects];
    
}

- (CLLocationCoordinate2D)waypointCoordinateAtIndex:(NSUInteger) index {
    if ([_pathOverlayer.waypointAnnotations count] > index) {
        NSObject<MKAnnotation> *annotation = [_pathOverlayer.waypointAnnotations objectAtIndex:index];
        return annotation.coordinate;
    }
    return CLLocationCoordinate2DMake(0, 0);
}

- (NSUInteger)indexOfAnnotation:(NSObject<MKAnnotation> *)annotation {
    if ([_pathOverlayer.waypointAnnotations containsObject:annotation]) {
        return [_pathOverlayer.waypointAnnotations indexOfObject:annotation];
    }
    return NSNotFound;
}
@end
