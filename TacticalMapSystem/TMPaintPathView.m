//
//  TMPaintPathView.m
//  TacticalMapSystem
//
//  Created by Chen Zhejun on 2014/01/07.
//  Copyright (c) 2014年 chen zhejun. All rights reserved.
//

#import "TMPaintPathView.h"
#import "TMAnnotation.h"

@implementation TMPaintPathView {
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.multipleTouchEnabled = false;
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self];
    
    CLLocationCoordinate2D coord = [_delegate convertPoint:touchLocation toCoordinateFromView:self];
    if (!_paintOverlay) {
        _paintOverlay = [[TMPaintOverlay alloc] initWithCenterCoordinate:coord];
        _paintRenderer = [[TMPaintRenderer alloc] initWithOverlay:_paintOverlay];
        [_paintRenderer setPathColorWithRed:60.0 green:60.0 blue:60.0 alpha:0.8];
        _paintRenderer.lineDash = NO;
    }
    
    CLLocation *newLocation = [[CLLocation alloc] initWithLatitude:coord.latitude longitude:coord.longitude];
    [_paintOverlay addPathWithLocation:newLocation];
    [_delegate addOverlayToMapView:_paintOverlay];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self];
    CLLocationCoordinate2D coord = [_delegate convertPoint:touchLocation toCoordinateFromView:self];

    CLLocation *newLocation = [[CLLocation alloc] initWithLatitude:coord.latitude longitude:coord.longitude];
    MKMapRect updateRect = [_paintOverlay addLineWithLocation:newLocation];
    MKZoomScale currentZoomScale = [_delegate mapCurrentZoomScale];
    [_paintRenderer updateDrawRect:updateRect zoomScale:currentZoomScale];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    
}
@end
