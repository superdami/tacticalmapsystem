//
//  RootViewController.m
//  TacticalMapSystem
//
//  Created by chen zhejun on 9/24/12.
//  Copyright (c) 2012 chen zhejun. All rights reserved.
//

#import "RootViewController.h"
#import "MapViewController.h"
#import "TMLoginViewController.h"
#import "TMHostViewController.h"
#import "TMRoom.h"
#import "MapViewController.h"
#import "KLCPopup.h"
#import "TMAnnotationManager.h"
#import "TMUserViewController.h"

static NSString *roomCellIdentifier = @"roomCellIdentifier";
@interface RootViewController () <UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate, UIAlertViewDelegate>

@end

@implementation RootViewController {
    MapViewController *_mapViewController;
    UIButton *_mapRestoreButton;
    UIImageView *_recordStatusView;
    UITableView *_tableView;
    
    CLLocationManager *_locationManager;
    NSMutableArray *_roomArray;
    
    TMRoom *_currentRoom;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _locationManager = [[CLLocationManager alloc] init];
    [_locationManager requestAlwaysAuthorization];
    _locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    _locationManager.delegate = self;
    
    _tableView = [[UITableView alloc] initWithFrame:self.view.frame];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:roomCellIdentifier];
    [self.view addSubview:_tableView];
    
    UIButton *userBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    userBtn.frame = CGRectMake(0.0, 0.0, 44.0, 44.0);
    [userBtn setTitle:@"User" forState:UIControlStateNormal];
    [userBtn setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
    [userBtn handleControlEvents:UIControlEventTouchUpInside withBlock:^(id weakSender) {
        if ([self checkUserAuth]) {
            TMUserViewController *userVC = [[TMUserViewController alloc] init];
            [self.navigationController pushViewController:userVC animated:YES];
        }
    }];
    UIBarButtonItem *buttonItem = [[UIBarButtonItem alloc] initWithCustomView:userBtn];
    self.navigationItem.rightBarButtonItem = buttonItem;
    
    UIButton *createBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    createBtn.frame = CGRectMake(0.0, self.view.frame.size.height - 60, self.view.frame.size.width / 2.0, 60);
    [createBtn setBackgroundColor:[UIColor redColor]];
    [createBtn setTitle:@"Add" forState:UIControlStateNormal];
    [createBtn setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
    [createBtn handleControlEvents:UIControlEventTouchUpInside withBlock:^(id weakSender) {
        if ([self checkUserAuth]) {
            [TMAnnotationManager sharedManager].offlineMode = NO;
            TMHostViewController *hostVC = [[TMHostViewController alloc] init];
            [self.navigationController pushViewController:hostVC animated:YES];
        }
    }];
    [self.view addSubview:createBtn];
    
    UIButton *offlineBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    offlineBtn.frame = CGRectMake(CGRectGetMaxX(createBtn.frame), self.view.frame.size.height - 60, self.view.frame.size.width / 2.0, 60);
    [offlineBtn setBackgroundColor:[UIColor orangeColor]];
    [offlineBtn setTitle:@"Offline" forState:UIControlStateNormal];
    [offlineBtn setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
    [offlineBtn handleControlEvents:UIControlEventTouchUpInside withBlock:^(id weakSender) {
        [TMAnnotationManager sharedManager].offlineMode = YES;
        MapViewController *mapVC = [[MapViewController alloc] init];
        [self.navigationController pushViewController:mapVC animated:YES];
    }];
    [self.view addSubview:offlineBtn];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    ((TMNavigationController *)self.navigationController).enableAutorotate = NO;
    [_locationManager startUpdatingLocation];
    
    [TMAPIClient requestRoomList:nil completion:^(id result) {
        _roomArray = [[NSMutableArray alloc] init];
        for (NSDictionary *data in result) {
            TMRoom *room = [[TMRoom alloc] init];
            room.originalData = data;
            [_roomArray addObject:room];
        }
        [_tableView reloadData];
    } failure:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)checkUserAuth {
    if ([TMUserManager sharedInstance].isLogin) {
        return YES;
    }
    
    [self showLoginViewController];
    return NO;
}

- (void)showLoginViewController {
    TMLoginViewController *loginVC = [[TMLoginViewController alloc] init];
    [self.navigationController presentViewController:loginVC animated:YES completion:nil];
}

- (void)checkRoom {
    [TMAPIClient checkRoomCompletion:^(id result) {
        _currentRoom = [[TMRoom alloc] init];
        _currentRoom.originalData = result;
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"Will you back to %@", _currentRoom.name] delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
        [alertView show];
        
    } failure:nil];
}

#pragma mark - UIAlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == alertView.cancelButtonIndex) {
        [TMAPIClient requestLeaveRoomCompletion:nil];
    } else {
        [[TMAPIClient sharedInstance] socketJoinRoomId:_currentRoom.uid completion:^(NSDictionary *result) {
            MapViewController *mapVC = [[MapViewController alloc] init];
            AppDelegate *appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            [appdelegate.rootViewController.navigationController pushViewController:mapVC animated:YES];
        }];
    }
}

#pragma mark - locationManager Delegate
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    [_locationManager stopUpdatingLocation];
    CLLocation *location = [locations lastObject];
    [TMUserManager sharedInstance].lastCoordinate = location.coordinate;
}

#pragma mark - UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_roomArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:roomCellIdentifier forIndexPath:indexPath];
    TMRoom *room = [_roomArray objectAtIndex:indexPath.row];
    cell.textLabel.text = room.name;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self checkUserAuth]) {
        TMHostViewController *hostVC = [[TMHostViewController alloc] init];
        TMRoom *room = [_roomArray objectAtIndex:indexPath.row];
        hostVC.joinRoomId = room.uid;
        [self.navigationController pushViewController:hostVC animated:YES];
    }
}
@end
