//
//  TMCircleView.m
//  TacticalMapSystem
//
//  Created by chen zhejun on 2015/04/26.
//  Copyright (c) 2015年 chen zhejun. All rights reserved.
//

#import "TMCircleView.h"

@implementation TMCircleView {
}

- (instancetype)initWithRadius:(CGFloat)redius innerRadius:(CGFloat)innerRadius type:(TMCircleViewType)type {
    CGFloat width = ceilf(redius / sqrt(2.0)) * 2.0;
    self = [super initWithFrame:CGRectMake(0.0, 0.0, width, width)];
    if (self) {
        UIVisualEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        visualEffectView.frame = self.bounds;
        visualEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self addSubview:visualEffectView];
        
        _circleType = type;
        CGPoint center;
        CGFloat startAngle, endAngle;
        CGPoint innerPos1, innerPos2;

        CGFloat innerWidth = innerRadius / ceilf(sqrt(2.0));
        if (_circleType == TMCircleViewTypeTop) {
            center = CGPointMake(ceilf(self.bounds.size.width / 2.0), self.bounds.size.height);
            innerPos1 = CGPointMake(center.x + innerWidth, center.y - innerWidth);
            innerPos2 = CGPointMake(center.x - innerWidth, center.y - innerWidth);
        } else if(_circleType == TMCircleViewTypeRight) {
            center = CGPointMake(0.0, ceilf(self.bounds.size.height / 2.0));
            innerPos1 = CGPointMake(center.x + innerWidth, center.y + innerWidth);
            innerPos2 = CGPointMake(center.x + innerWidth, center.y - innerWidth);
        } else if(_circleType == TMCircleViewTypeBottom) {
            center = CGPointMake(ceilf(self.bounds.size.width / 2.0), 0.0);
            innerPos1 = CGPointMake(center.x + innerWidth, center.y + innerWidth);
            innerPos2 = CGPointMake(center.x - innerWidth, center.y + innerWidth);
        } else {
            center = CGPointMake(self.bounds.size.width, ceilf(self.bounds.size.height / 2.0));
            innerPos1 = CGPointMake(center.x - innerWidth, center.y - innerWidth);
            innerPos2 = CGPointMake(center.x - innerWidth, center.y + innerWidth);
        }
        
        startAngle = -M_PI + M_PI_4 + _circleType * M_PI_2;
        endAngle = startAngle + M_PI_2;
        
        UIBezierPath *path = [[UIBezierPath alloc] init];
        [path addArcWithCenter:center radius:redius startAngle:startAngle endAngle:endAngle clockwise:YES];
        [path addLineToPoint:innerPos1];
        [path addArcWithCenter:center radius:innerRadius startAngle:endAngle endAngle:startAngle clockwise:NO];
        [path closePath];
        
        CAShapeLayer *pieShape = [CAShapeLayer layer];
        pieShape.path = [path CGPath];
        self.layer.mask = pieShape;
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
