//
//  TMWaypointView.m
//  TacticalMapSystem
//
//  Created by Chen Zhejun on 2014/02/11.
//  Copyright (c) 2014年 chen zhejun. All rights reserved.
//

#import "TMWaypointView.h"

@implementation TMWaypointView {
    UILabel *_numberLabel;
}

- (id)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self) {
        CGRect f = CGRectMake(ceilf((self.bounds.size.width - 50.0) / 2.0), ceilf((self.bounds.size.height - 50.0) / 2.0), 50.0, 50.0);
        _numberLabel = [[UILabel alloc] initWithFrame:f];
        _numberLabel.backgroundColor = [UIColor clearColor];
        _numberLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16.0];
        _numberLabel.textColor = [UIColor grayColor];
        _numberLabel.textAlignment = NSTextAlignmentCenter;
        _numberLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin |
        UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        [self addSubview:_numberLabel];
    }
    return self;
}

- (void)setLabelNumber:(NSInteger)number {
    _numberLabel.text = [NSString stringWithFormat:@"%d", number];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
