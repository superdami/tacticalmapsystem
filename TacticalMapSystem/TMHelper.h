//
//  TMHelper.h
//  TacticalMapSystem
//
//  Created by Chen Zhejun on 2014/01/08.
//  Copyright (c) 2014年 chen zhejun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
@interface TMHelper : NSObject
+ (MKMapRect)makeRectWithPoint:(MKMapPoint)point1 and:(MKMapPoint)point2;
+ (BOOL)linePoint:(MKMapPoint)p0 and:(MKMapPoint)p1 intersectsRect:(MKMapRect)r;
+ (CGFloat)distanceBetween:(CLLocationCoordinate2D)coordinate1 and:(CLLocationCoordinate2D)coordinate2;
+ (MKMapRect)MKMapRectForCoordinateRegion:(MKCoordinateRegion)region;
+ (MKCoordinateRegion) regionForAnnotations:(NSArray *)annotationArray;
+ (CLLocationDirection)directionFromCoordinate:(CLLocationCoordinate2D)coord1 to:(CLLocationCoordinate2D)coord2;
@end
