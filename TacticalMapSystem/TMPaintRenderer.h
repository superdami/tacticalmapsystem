//
//  TMPaintRenderer.h
//  TacticalMapSystem
//
//  Created by Chen Zhejun on 2014/01/07.
//  Copyright (c) 2014年 chen zhejun. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "TMRouteRenderer.h"

@interface TMPaintRenderer : TMRouteRenderer

@end
