//
//  RootViewController.h
//  TacticalMapSystem
//
//  Created by chen zhejun on 9/24/12.
//  Copyright (c) 2012 chen zhejun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RootViewController : UIViewController

- (void)showLoginViewController;
- (void)checkRoom;
@end
