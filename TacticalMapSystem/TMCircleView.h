//
//  TMCircleView.h
//  TacticalMapSystem
//
//  Created by chen zhejun on 2015/04/26.
//  Copyright (c) 2015年 chen zhejun. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    TMCircleViewTypeTop = 0,
    TMCircleViewTypeRight,
    TMCircleViewTypeBottom,
    TMCircleViewTypeLeft,
    TMCircleViewTypeNone
} TMCircleViewType;

@interface TMCircleView : UIView
@property (nonatomic, readonly)TMCircleViewType circleType;

- (instancetype)initWithRadius:(CGFloat)redius innerRadius:(CGFloat)innerRadius type:(TMCircleViewType)type;
@end
