//
//  TMPathsOverlay.m
//  TacticalMapSystem
//
//  Created by Chen Zhejun on 7/25/14.
//  Copyright (c) 2014 chen zhejun. All rights reserved.
//

#import "TMPathsOverlay.h"
#import "TMHelper.h"
#import "TMRouteRenderer.h"
#import <pthread.h>

#define WORLD_SCALE         4.0
@implementation TMPathsOverlay
{
    MKMapRect _boundingMapRect;
    pthread_rwlock_t _rwLock;
    MKMapRect _oldRect;
    MKMapPoint _centerPoint;
}

- (MKMapPoint)centerPointWithPath:(NSArray *)path {
    MKMapPoint p1 = MKMapPointForCoordinate([[path objectAtIndex:0] MKCoordinateValue]);
    MKMapPoint p2 = MKMapPointForCoordinate([[path objectAtIndex:1] MKCoordinateValue]);
    MKMapPoint center = MKMapPointMake(ceilf((p1.x + p2.x) / 2.0), ceilf((p1.y + p2.y) / 2.0));
    
    for (int i = 2; i < [path count]; i++) {
        MKMapPoint point1 = MKMapPointForCoordinate([[path objectAtIndex:i - 1] MKCoordinateValue]);
        MKMapPoint point2 = MKMapPointForCoordinate([[path objectAtIndex:i] MKCoordinateValue]);
        MKMapPoint newCenter = MKMapPointMake(ceilf((point1.x + point2.x) / 2.0), ceilf((point1.y + point2.y) / 2.0));
        
        center = MKMapPointMake(ceilf((center.x + newCenter.x) / 2.0), ceilf((center.y + newCenter.y) / 2.0));
    }
    
    return center;
}

- (void)updateBoundRectWithNewPath:(NSArray *)path {
    MKMapPoint pathCenter = [self centerPointWithPath:path];
    _centerPoint = MKMapPointMake(ceilf((_centerPoint.x + pathCenter.x) / 2.0), ceilf((_centerPoint.y + pathCenter.y) / 2.0));
    MKMapPoint origin = MKMapPointMake(_centerPoint.x - MKMapSizeWorld.width / (WORLD_SCALE * 2), _centerPoint.y - MKMapSizeWorld.height / (WORLD_SCALE * 2));
    MKMapSize size = MKMapSizeMake(MKMapSizeWorld.width / WORLD_SCALE, MKMapSizeWorld.height / WORLD_SCALE);
    MKMapRect rect = (MKMapRect){origin, size};
    MKMapRect worldRect = MKMapRectMake(0, 0, MKMapSizeWorld.width, MKMapSizeWorld.height);
    _boundingMapRect = MKMapRectIntersection(rect, worldRect);
}

- (void)updateBoundRectWithRemovePath:(NSArray *)path {
    MKMapPoint pathCenter = [self centerPointWithPath:path];
    _centerPoint = MKMapPointMake(2 * _centerPoint.x - pathCenter.x, 2 * _centerPoint.y - pathCenter.y);
    MKMapPoint origin = MKMapPointMake(_centerPoint.x - MKMapSizeWorld.width / (WORLD_SCALE * 2), _centerPoint.y - MKMapSizeWorld.height / (WORLD_SCALE * 2));
    MKMapSize size = MKMapSizeMake(MKMapSizeWorld.width / WORLD_SCALE, MKMapSizeWorld.height / WORLD_SCALE);
    MKMapRect rect = (MKMapRect){origin, size};
    MKMapRect worldRect = MKMapRectMake(0, 0, MKMapSizeWorld.width, MKMapSizeWorld.height);
    _boundingMapRect = MKMapRectIntersection(rect, worldRect);
}

- (id)init
{
    self = [super init];
    if (self) {
        _paths = [[NSMutableArray alloc] init];
        _lastUpdateRect = MKMapRectNull;
        _boundingMapRect = MKMapRectNull;
    }
    return self;
}

- (void)setMapView:(MKMapView *)mapView {
    if (_mapView) {
        [_mapView removeOverlay:self];
    }
    
    _mapView = mapView;
    if (_mapView) {
        [_mapView addOverlay:self];
    }
}

- (void)dealloc
{
    pthread_rwlock_destroy(&_rwLock);
    [self setMapView:nil];
}

- (MKMapRect)boundingMapRect
{
    return _boundingMapRect;
}

- (CLLocationCoordinate2D)coordinate
{
    return MKCoordinateForMapPoint(_centerPoint);
}

- (void)lockForReading
{
    pthread_rwlock_rdlock(&_rwLock);
}

- (void)unlockForReading
{
    pthread_rwlock_unlock(&_rwLock);
}

- (void)redrawRenderer:(MKMapRect)mapRect {
    MKZoomScale currentZoomScale = (CGFloat)(_mapView.bounds.size.width / _mapView.visibleMapRect.size.width);
    TMRouteRenderer *pathRenderer = (TMRouteRenderer *)[_mapView rendererForOverlay:self];
    [pathRenderer updateDrawRect:mapRect zoomScale:currentZoomScale];
}

- (void)addPath:(NSArray *)path {
    if (![_paths count]) {
        _centerPoint = [self centerPointWithPath:path];
    }
    [self updateBoundRectWithNewPath:path];
    [self lockForReading];
    [_paths addObject:path];
    MKMapRect updateRect = [self mapRectAssociatedWithPath:path];
    [self unlockForReading];
    
    [self redrawRenderer:updateRect];
}

- (void)insertPath:(NSArray *)path atIndex:(NSUInteger)index {
    if (![_paths count]) {
        _centerPoint = [self centerPointWithPath:path];
    }
    [self updateBoundRectWithNewPath:path];
    [self lockForReading];
    [_paths insertObject:path atIndex:index];
    MKMapRect updateRect = [self mapRectAssociatedWithPath:path];
    [self unlockForReading];
    
    [self redrawRenderer:updateRect];
}

- (void)removePathAtIndex:(NSUInteger)index {
    NSArray *path = [_paths objectAtIndex:index];
    [self updateBoundRectWithRemovePath:path];
    [self lockForReading];
    [_paths removeObject:path];
    MKMapRect updateRect = [self mapRectAssociatedWithPath:path];
    [self unlockForReading];
    
    [self redrawRenderer:updateRect];
}

- (MKMapRect)mapRectAssociatedWithPath:(NSArray *)path {
    MKMapRect lastUpdateRect = MKMapRectNull;
    for (int i = 1; i < [path count]; i++) {
        MKMapPoint p1 = MKMapPointForCoordinate([[path objectAtIndex:i - 1] MKCoordinateValue]);
        MKMapPoint p2 = MKMapPointForCoordinate([[path objectAtIndex:i] MKCoordinateValue]);
        MKMapRect rect = [TMHelper makeRectWithPoint:p1 and:p2];
        
        lastUpdateRect = MKMapRectUnion(lastUpdateRect, rect);
    }
    return lastUpdateRect;
}
@end
