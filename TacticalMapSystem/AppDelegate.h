//
//  AppDelegate.h
//  TacticalMapSystem
//
//  Created by chen zhejun on 9/20/12.
//  Copyright (c) 2012 chen zhejun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (readonly, nonatomic) UIViewController *rootViewController;
@end
