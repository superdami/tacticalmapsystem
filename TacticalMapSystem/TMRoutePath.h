//
//  TMRoutePath.h
//  TacticalMapSystem
//
//  Created by Chen Zhejun on 2014/01/01.
//  Copyright (c) 2014年 chen zhejun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "TMRouteOverlayer.h"
#import "TMRouteRenderer.h"

@class TMRoutePath;

@interface TMRoutePath : NSObject
@property (nonatomic, readonly) TMRouteOverlayer *pathOverlayer;
@property (nonatomic, readonly) TMRouteRenderer *pathRenderer;
@property (nonatomic, readonly) TMRouteOverlayer *trackingOverlayer;
@property (nonatomic, readonly) TMRouteRenderer *trackingRenderer;
@property (nonatomic, readonly) NSUInteger currentWaypointIndex;
@end
