//
//  TMSideMenuView.h
//  TacticalMapSystem
//
//  Created by chen zhejun on 2015/05/09.
//  Copyright (c) 2015年 chen zhejun. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol TMSlideMenuViewDelegate <NSObject>;
@optional
- (void)switchShowAnnotationTag:(BOOL)show;
- (void)switchUsingUnitMetric:(BOOL)isMetric;
- (void)switchUsingDegree:(BOOL)isDegree;
- (void)disconnect;
@end

@interface TMSideMenuView : UIView
@property (nonatomic, weak)id<TMSlideMenuViewDelegate> delegate;
@end
