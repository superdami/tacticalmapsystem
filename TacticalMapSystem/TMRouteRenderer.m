//
//  TMRouteRenderer.m
//  TacticalMapSystem
//
//  Created by Chen Zhejun on 2013/12/28.
//  Copyright (c) 2013年 chen zhejun. All rights reserved.
//

#import "TMRouteRenderer.h"
#import "TMRouteOverlay.h"
#import "TMPathsOverlay.h"
#import "TMHelper.h"
@implementation TMRouteRenderer
{
    CGFloat _red;
    CGFloat _green;
    CGFloat _blue;
    CGFloat _alpha;
    
}

- (void)setPathColorWithRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue alpha:(CGFloat)alpha {
    _red = red;
    _green = green;
    _blue = blue;
    _alpha = alpha;
}

- (void)updateDrawRect:(MKMapRect)updateRect zoomScale:(MKZoomScale)zoomScale {
    if (!MKMapRectIsNull(updateRect))
    {
        CGFloat lineWidth = MKRoadWidthAtZoomScale(zoomScale);
        updateRect = MKMapRectInset(updateRect, -lineWidth, -lineWidth);
        [self setNeedsDisplayInMapRect:updateRect];
    }
}

- (void)drawMapRect:(MKMapRect)mapRect
          zoomScale:(MKZoomScale)zoomScale
          inContext:(CGContextRef)context
{
    // outset the map rect by the line width so that points just outside
    // of the currently drawn rect are included in the generated path.
    CGFloat lineWidth = MKRoadWidthAtZoomScale(zoomScale);
    MKMapRect clipRect = MKMapRectInset(mapRect, -lineWidth, -lineWidth);

    if ([self.overlay isKindOfClass:[TMRouteOverlay class]]) {
        TMRouteOverlay *overLay = self.overlay;
        
        [overLay lockForReading];
        CGPathRef path = [self newPathForAnnotations:overLay.waypointAnnotations clipRect:clipRect zoomScale:zoomScale];
        [overLay unlockForReading];
        
        if (path != nil)
        {
            
            CGContextAddPath(context, path);
            CGContextSetRGBStrokeColor(context, _red / 255.0, _green / 255.0, _blue / 255.0, _alpha);
            if (_lineDash) {
                CGFloat dashes[] = {40.0 / zoomScale ,20.0 / zoomScale};
                CGContextSetLineDash(context, 0.0, dashes, 2);
            }
            else {
                CGContextSetLineJoin(context, kCGLineJoinRound);
                CGContextSetLineCap(context, kCGLineCapRound);
            }
            CGFloat width = (_lineWidth == 0 ? 1.0 : _lineWidth) / zoomScale;
            CGContextSetLineWidth(context, width);
            CGContextStrokePath(context);
            CGPathRelease(path);
        }
    }
    else if ([self.overlay isKindOfClass:[TMPathsOverlay class]]) {
        TMPathsOverlay *overlay = self.overlay;
        NSMutableArray *array = [[NSMutableArray alloc] init];
        for (NSArray *path in overlay.paths) {
            [overlay lockForReading];
            CGPathRef pathRef = [self newPath:path clipRect:clipRect zoomScale:zoomScale];
            [overlay unlockForReading];
            
            CGContextAddPath(context, pathRef);
            CGContextSetRGBStrokeColor(context, _red / 255.0, _green / 255.0, _blue / 255.0, _alpha);
            if (_lineDash) {
                CGFloat dashes[] = {40.0 / zoomScale ,20.0 / zoomScale};
                CGContextSetLineDash(context, 0.0, dashes, 2);
            }
            else {
                CGContextSetLineJoin(context, kCGLineJoinRound);
                CGContextSetLineCap(context, kCGLineCapRound);
            }
            CGFloat width = (_lineWidth == 0 ? 1.0 : _lineWidth) / zoomScale;
            CGContextSetLineWidth(context, width);
            CGContextStrokePath(context);
            CGPathRelease(pathRef);
        }
    }
}

#define MIN_POINT_DELTA 5.0

- (CGPathRef)newPathForAnnotations:(NSArray *)annotations
                          clipRect:(MKMapRect)mapRect
                         zoomScale:(MKZoomScale)zoomScale
{
    // The fastest way to draw a path in an MKOverlayView is to simplify the
    // geometry for the screen by eliding points that are too close together
    // and to omit any line segments that do not intersect the clipping rect.
    // While it is possible to just add all the points and let CoreGraphics
    // handle clipping and flatness, it is much faster to do it yourself:
    //
    if ([annotations count] < 2)
        return NULL;
    
    CGMutablePathRef path = NULL;
    
    BOOL needsMove = YES;
    
#define POW2(a) ((a) * (a))
    
    // Calculate the minimum distance between any two points by figuring out
    // how many map points correspond to MIN_POINT_DELTA of screen points
    // at the current zoomScale.
    double minPointDelta = MIN_POINT_DELTA / zoomScale;
    double c2 = POW2(minPointDelta);
    
    MKMapPoint point, lastPoint = MKMapPointForCoordinate(((NSObject<MKAnnotation> *)[annotations objectAtIndex:0]).coordinate);
    NSUInteger i;
    for (i = 1; i < [annotations count] - 1; i++)
    {
        point = MKMapPointForCoordinate(((NSObject<MKAnnotation> *)[annotations objectAtIndex:i]).coordinate);
        double a2b2 = POW2(point.x - lastPoint.x) + POW2(point.y - lastPoint.y);
        if (a2b2 >= c2) {
            if ([TMHelper linePoint:point and:lastPoint intersectsRect:mapRect])
            {
                if (!path)
                    path = CGPathCreateMutable();
                if (needsMove)
                {
                    CGPoint lastCGPoint = [self pointForMapPoint:lastPoint];
                    CGPathMoveToPoint(path, NULL, lastCGPoint.x, lastCGPoint.y);
                }
                CGPoint cgPoint = [self pointForMapPoint:point];
                CGPathAddLineToPoint(path, NULL, cgPoint.x, cgPoint.y);
            }
            else
            {
                // discontinuity, lift the pen
                needsMove = YES;
            }
            lastPoint = point;
        }
    }

#undef POW2
    
    // If the last line segment intersects the mapRect at all, add it unconditionally
    point = MKMapPointForCoordinate(((NSObject<MKAnnotation> *)[annotations lastObject]).coordinate);
    if ([TMHelper linePoint:lastPoint and:point intersectsRect:mapRect])
    {
        if (!path)
            path = CGPathCreateMutable();
        if (needsMove)
        {
            CGPoint lastCGPoint = [self pointForMapPoint:lastPoint];
            CGPathMoveToPoint(path, NULL, lastCGPoint.x, lastCGPoint.y);
        }
        CGPoint cgPoint = [self pointForMapPoint:point];
        CGPathAddLineToPoint(path, NULL, cgPoint.x, cgPoint.y);
    }
    
    if (((TMRouteOverlay *)self.overlay).closePath) {
        point = MKMapPointForCoordinate(((NSObject<MKAnnotation> *)[annotations objectAtIndex:0]).coordinate);
        lastPoint = MKMapPointForCoordinate(((NSObject<MKAnnotation> *)[annotations lastObject]).coordinate);
        if ([TMHelper linePoint:lastPoint and:point intersectsRect:mapRect])
        {
            if (!path)
                path = CGPathCreateMutable();
            if (needsMove)
            {
                CGPoint lastCGPoint = [self pointForMapPoint:lastPoint];
                CGPathMoveToPoint(path, NULL, lastCGPoint.x, lastCGPoint.y);
            }
            CGPoint cgPoint = [self pointForMapPoint:point];
            CGPathAddLineToPoint(path, NULL, cgPoint.x, cgPoint.y);
        }
    }
    
    return path;
}

- (CGPathRef)newPath:(NSArray *)pathLocations
            clipRect:(MKMapRect)mapRect
           zoomScale:(MKZoomScale)zoomScale
{
    // The fastest way to draw a path in an MKOverlayView is to simplify the
    // geometry for the screen by eliding points that are too close together
    // and to omit any line segments that do not intersect the clipping rect.
    // While it is possible to just add all the points and let CoreGraphics
    // handle clipping and flatness, it is much faster to do it yourself:
    //
    if ([pathLocations count] < 2)
        return NULL;
    
    CGMutablePathRef path = NULL;
    
    BOOL needsMove = YES;
    
#define POW2(a) ((a) * (a))
    
    // Calculate the minimum distance between any two points by figuring out
    // how many map points correspond to MIN_POINT_DELTA of screen points
    // at the current zoomScale.
    double minPointDelta = MIN_POINT_DELTA / zoomScale;
    double c2 = POW2(minPointDelta);
    
    MKMapPoint point, lastPoint = MKMapPointForCoordinate([[pathLocations objectAtIndex:0] MKCoordinateValue]);
    NSUInteger i;
    for (i = 1; i < [pathLocations count] - 1; i++)
    {
        point = MKMapPointForCoordinate([[pathLocations objectAtIndex:i] MKCoordinateValue]);
        double a2b2 = POW2(point.x - lastPoint.x) + POW2(point.y - lastPoint.y);
        if (a2b2 >= c2) {
            if ([TMHelper linePoint:point and:lastPoint intersectsRect:mapRect])
            {
                if (!path)
                    path = CGPathCreateMutable();
                if (needsMove)
                {
                    CGPoint lastCGPoint = [self pointForMapPoint:lastPoint];
                    CGPathMoveToPoint(path, NULL, lastCGPoint.x, lastCGPoint.y);
                }
                CGPoint cgPoint = [self pointForMapPoint:point];
                CGPathAddLineToPoint(path, NULL, cgPoint.x, cgPoint.y);
            }
            else
            {
                // discontinuity, lift the pen
                needsMove = YES;
            }
            lastPoint = point;
        }
    }
    
#undef POW2
    
    // If the last line segment intersects the mapRect at all, add it unconditionally
    point = MKMapPointForCoordinate([[pathLocations lastObject] MKCoordinateValue]);
    if ([TMHelper linePoint:lastPoint and:point intersectsRect:mapRect])
    {
        if (!path)
            path = CGPathCreateMutable();
        if (needsMove)
        {
            CGPoint lastCGPoint = [self pointForMapPoint:lastPoint];
            CGPathMoveToPoint(path, NULL, lastCGPoint.x, lastCGPoint.y);
        }
        CGPoint cgPoint = [self pointForMapPoint:point];
        CGPathAddLineToPoint(path, NULL, cgPoint.x, cgPoint.y);
    }
    
    return path;
}
@end